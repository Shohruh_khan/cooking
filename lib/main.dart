import 'package:cooking/core/splash/splash_screen.dart';
import 'package:cooking/features/favorite/data/repository/favorite_impl.dart';
import 'package:cooking/features/login/data/repository/auth_repository_impl.dart';
import 'package:cooking/features/login/presentation/blocs/auth_bloc.dart';
import 'package:cooking/features/search/data/data_source/local_data_source.dart';
import 'package:cooking/features/search/data/repository/search_repo_impl.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:overlay_support/overlay_support.dart';

import 'assets/colors/colors.dart';
import 'assets/icons/icons.dart';
import 'assets/theme/theme.dart';
import 'core/bloc/show_pop_up/show_pop_up_bloc.dart';
import 'core/bloc/translation/translation_bloc.dart';
import 'core/data/boxes.dart';
import 'core/data/service_locator.dart';
import 'core/models/popup_types.dart';
import 'core/widgets/popups.dart';
import 'features/favorite/presentation/blocs/favorite_bloc.dart';
import 'features/main/domain/entity/recipe.dart';
import 'features/search/domain/entity/search_history.dart';
import 'features/search/presentation/bloc/search_bloc.dart';
import 'firebase_options.dart';

Future<void> main() async {
  setupLocator();
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);

  await Hive.initFlutter();
  Hive.registerAdapter(RecipeEntityAdapter());
  recipeBox = await Hive.openBox<List<RecipeEntity>>(recipesKey);
  token = await Hive.openBox(tokenKey);
  Hive.registerAdapter(SearchHistoryEntityAdapter());
  searchHistoryBox =
      await Hive.openBox<List<SearchHistoryEntity>>(searchHistoryKey);

  // searchHistoryBox =
  //     await Hive.openBox<List<SearchHistoryEntity>>(searchHistoryKey);
  // searchBox = await Hive.openBox<List<SearchHistoryElement>>('searchRecipes');

  runApp(EasyLocalization(
      path: 'assets/translations',
      saveLocale: true,
      supportedLocales: const [Locale('uz'), Locale('en'), Locale('ru')],
      fallbackLocale: const Locale('uz'),
      child: MyApp(
        recipeBox: recipeBox,
      )));
  // runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.recipeBox}) : super(key: key);
  final Box recipeBox;

  @override
  Widget build(BuildContext context) {
    return OverlaySupport.global(
      child: MultiBlocProvider(
        providers: [
          BlocProvider<ShowPopUpBloc>(
            create: (BuildContext context) => ShowPopUpBloc(),
          ),
          BlocProvider<SearchBloc>(
            create: (BuildContext context) =>
                SearchBloc(SearchLocalDataSourceImpl(), SearchRepositoryImpl()),
          ),
          BlocProvider(create: (context) => AuthBloc(AuthRepositoryImpl())),
          // BlocProvider<MainBloc>(
          //     create: (BuildContext context) => MainBloc()),
          BlocProvider<TranslationBloc>(create: (context) => TranslationBloc()),
          BlocProvider<FavoriteBloc>(
              create: (context) =>
                  FavoriteBloc(repository: FavouritesRepositoryImpl())),
        ],
        child: BlocListener<ShowPopUpBloc, ShowPopUpState>(
          listener: (context, state) {
            if (state.showPopUp && state.popUpType == PopUpType.error) {
              showSimpleNotification(
                WPopUp(color: red, icon: AppIcons.error, text: state.errorText),
                elevation: 0,
                background: Colors.transparent,
                autoDismiss: true,
                slideDismissDirection: DismissDirection.horizontal,
              );
            } else if (state.showPopUp &&
                state.popUpType == PopUpType.warning) {
              showSimpleNotification(
                WPopUp(
                  color: Colors.orangeAccent,
                  icon: AppIcons.error,
                  text: state.warningText,
                ),
                elevation: 0,
                background: Colors.transparent,
                autoDismiss: true,
                slideDismissDirection: DismissDirection.horizontal,
              );
            } else if (state.showPopUp &&
                state.popUpType == PopUpType.success) {
              showSimpleNotification(
                WPopUp(
                  color: Colors.green,
                  icon: AppIcons.success,
                  text: state.successText,
                ),
                elevation: 0,
                background: Colors.transparent,
                autoDismiss: true,
                slideDismissDirection: DismissDirection.horizontal,
              );
            }
          },
          child: ScreenUtilInit(
            designSize: const Size(393, 852),
            minTextAdapt: true,
            splitScreenMode: true,
            child: Builder(builder: (context) {
              return MaterialApp(
                localizationsDelegates: context.localizationDelegates,
                supportedLocales: context.supportedLocales,
                locale: context.locale,
                debugShowCheckedModeBanner: false,
                theme: AppTheme.lightTheme(),
                home: const SplashScreen(),
              );
            }),
          ),
        ),
      ),
    );
  }
}
