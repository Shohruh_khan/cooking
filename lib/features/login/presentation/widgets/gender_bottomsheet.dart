import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../assets/icons/icons.dart';
import '../../../../core/widgets/w_bottomsheet.dart';
import '../../../../core/widgets/w_scale.dart';
import '../../../profile/presentation/widgets/item_bottomsheet.dart';
import '../blocs/auth_bloc.dart';

class GenderBottomsheet extends StatefulWidget {
  const GenderBottomsheet({
    Key? key,
  }) : super(key: key);

  @override
  State<GenderBottomsheet> createState() => _GenderBottomsheetState();
}

class _GenderBottomsheetState extends State<GenderBottomsheet> {
  @override
  Widget build(BuildContext context) {
    return WBottomSheet(
      children: [
        Row(
          children: [
            Text(
              'gender'.tr(),
              style: Theme.of(context).textTheme.headline1!.copyWith(
                    fontWeight: FontWeight.w600,
                    fontSize: 20,
                  ),
            ),
            const Spacer(),
            WScaleAnimation(
                child: SvgPicture.asset(AppIcons.cancel),
                onTap: () {
                  Navigator.pop(context);
                })
          ],
        ),
        const SizedBox(height: 24),
        ItemBottomsheet(
            text: 'male'.tr(),
            onTap: () {
              Navigator.of(context).pop();

              context.read<AuthBloc>().add(
                    const AuthEvent.chooseGender(isMale: true),
                  );
            },
            textStyle: Theme.of(context).textTheme.headline1!.copyWith(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                )),
        const SizedBox(height: 14),
        ItemBottomsheet(
          text: 'female'.tr(),
          onTap: () {
            Navigator.of(context).pop();

            context.read<AuthBloc>().add(
                  const AuthEvent.chooseGender(isMale: false),
                );
          },
          textStyle: Theme.of(context).textTheme.headline1!.copyWith(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
        ),
      ],
    );
  }
}
