import 'package:cooking/features/home/presentation/widgets/navigator.dart';
import 'package:cooking/features/login/presentation/blocs/auth_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../assets/colors/colors.dart';
import '../../../../assets/icons/icons.dart';
import '../../../../core/bloc/show_pop_up/show_pop_up_bloc.dart';
import '../../../../core/models/formz/formz_status.dart';
import '../../../../core/models/formz/formz_type.dart';
import '../../../../core/widgets/w_button.dart';
import '../../../../core/widgets/w_scale.dart';
import '../../../../core/widgets/w_text_field/bloc/validation_bloc.dart';
import '../../../../core/widgets/w_text_field/w_text_field.dart';
import '../login_screen.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);
  static const id = 'forgot_password_screen';

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _emailController = TextEditingController();
  final ValidationBloc validatorBlocEmail = ValidationBloc();

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: WScaleAnimation(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: SvgPicture.asset(AppIcons.back),
            ),
          ),
          backgroundColor: primary,
          title: Text(
            'forgot_password'.tr(),
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontSize: 18, fontWeight: FontWeight.w600),
          )),
      body: ListView(
        children: [
          const SizedBox(height: 56),
          WTextField(
            validationBloc: validatorBlocEmail,
            disabledColor: Colors.white38,
            title: 'email'.tr(),
            textStyle: Theme.of(context).textTheme.titleMedium,
            titleTextStyle: Theme.of(context).textTheme.titleMedium,
            margin: const EdgeInsets.symmetric(horizontal: 16),
            hintText: 'enter_email'.tr(),
            hintTextStyle: Theme.of(context).textTheme.titleMedium!.copyWith(
                fontSize: 14,
                color: black.withOpacity(0.8),
                fontWeight: FontWeight.w400),
            controller: _emailController,
            onChanged: (value) {},
            validationType: FormzType.email,
          ),
          WButton(
            color: primary,
            margin: const EdgeInsets.all(16),
            onTap: () {
              if (validatorBlocEmail.state.status ==
                  FormzStatus.submissionSuccess) {
                context.read<AuthBloc>().add(AuthEvent.resetPassword(
                    email: _emailController.text,
                    onFailure: (value) {
                      context
                          .read<ShowPopUpBloc>()
                          .add(ShowPopUpEvent.showFailure(text: value));
                    },
                    onSuccess: (value) {
                      debugPrint("succcess value $value");
                      Navigator.of(context).push(fade(page: LoginScreen()));
                    }));
              } else {
                context
                    .read<ShowPopUpBloc>()
                    .add(ShowPopUpEvent.showFailure(text: "fill_below".tr()));
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Icon(
                  Icons.mail_outline,
                  color: white,
                ),
                const SizedBox(width: 10),
                Text(
                  "reset_password".tr(),
                  style: Theme.of(context).textTheme.headlineMedium,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
