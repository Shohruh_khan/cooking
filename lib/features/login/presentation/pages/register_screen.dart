import 'package:cooking/core/models/formz/rules.dart';
import 'package:cooking/core/widgets/w_scale.dart';
import 'package:cooking/features/login/presentation/blocs/auth_bloc.dart';
import 'package:cooking/features/login/presentation/login_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../assets/colors/colors.dart';
import '../../../../assets/icons/icons.dart';
import '../../../../core/bloc/show_pop_up/show_pop_up_bloc.dart';
import '../../../../core/models/formz/formz_status.dart';
import '../../../../core/models/formz/formz_type.dart';
import '../../../../core/widgets/popus/popups.dart';
import '../../../../core/widgets/w_button.dart';
import '../../../../core/widgets/w_text_field/bloc/validation_bloc.dart';
import '../../../../core/widgets/w_text_field/w_text_field.dart';
import '../../../home/presentation/home.dart';
import '../../../home/presentation/widgets/navigator.dart';
import '../widgets/gender_bottomsheet.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({super.key});
  static const id = 'register_screen';

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _firstNameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _confirmPasswordController =
      TextEditingController();
  final TextEditingController _ageController = TextEditingController();

  final ValidationBloc validatorBlocEmail = ValidationBloc();
  final ValidationBloc validatorBlocFirstName = ValidationBloc();
  final ValidationBloc validatorBlocLastName = ValidationBloc();
  final ValidationBloc validatorBlocAge = ValidationBloc();
  final ValidationBloc validatorBlocPassword = ValidationBloc();
  final ValidationBloc validatorBlocConfirmPassword = ValidationBloc();
  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          leading: WScaleAnimation(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: SvgPicture.asset(AppIcons.back),
            ),
          ),
          backgroundColor: primary,
          title: Text(
            'register'.tr(),
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontSize: 18, fontWeight: FontWeight.w600),
          )),
      body: ListView(
        padding: const EdgeInsets.all(12),
        children: [
          Row(
            children: [
              Expanded(
                child: WTextField(
                  validationBloc: validatorBlocFirstName,
                  textInputAction: TextInputAction.next,
                  disabledColor: Colors.white38,
                  title: 'first_name'.tr(),
                  textStyle: Theme.of(context).textTheme.titleMedium,
                  titleTextStyle: Theme.of(context).textTheme.titleMedium,
                  hintText: 'enter_name'.tr(),
                  hintTextStyle: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(
                          fontSize: 14,
                          color: black.withOpacity(0.8),
                          fontWeight: FontWeight.w400),
                  controller: _firstNameController,
                  onChanged: (value) {},
                ),
              ),
              SizedBox(width: 6.w),
              Expanded(
                child: WTextField(
                  validationBloc: validatorBlocLastName,
                  textInputAction: TextInputAction.next,
                  disabledColor: Colors.white38,
                  title: 'surname'.tr(),
                  textStyle: Theme.of(context).textTheme.titleMedium,
                  titleTextStyle: Theme.of(context).textTheme.titleMedium,
                  hintText: 'enter_surname'.tr(),
                  hintTextStyle: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(
                          fontSize: 14,
                          color: black.withOpacity(0.8),
                          fontWeight: FontWeight.w400),
                  controller: _lastNameController,
                  onChanged: (value) {},
                ),
              ),
            ],
          ),
          SizedBox(height: 12.h),
          Row(
            children: [
              Expanded(
                child: WTextField(
                  validationBloc: validatorBlocAge,
                  textInputAction: TextInputAction.next,
                  disabledColor: Colors.white38,
                  title: 'age'.tr(),
                  textStyle: Theme.of(context).textTheme.titleMedium,
                  titleTextStyle: Theme.of(context).textTheme.titleMedium,
                  hintText: 'enter_age'.tr(),
                  keyBoardType: TextInputType.number,
                  textInputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  hintTextStyle: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(
                          fontSize: 14,
                          color: black.withOpacity(0.8),
                          fontWeight: FontWeight.w400),
                  controller: _ageController,
                  onChanged: (value) {},
                ),
              ),
              SizedBox(width: 6.w),
              Expanded(
                flex: 2,
                child: WTextField(
                  validationBloc: validatorBlocEmail,
                  textInputAction: TextInputAction.next,
                  disabledColor: Colors.white38,
                  title: 'email'.tr(),
                  textStyle: Theme.of(context).textTheme.titleMedium,
                  titleTextStyle: Theme.of(context).textTheme.titleMedium,
                  hintText: 'enter_email'.tr(),
                  hintTextStyle: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(
                          fontSize: 14,
                          color: black.withOpacity(0.8),
                          fontWeight: FontWeight.w400),
                  controller: _emailController,
                  onChanged: (value) {},
                  validationType: FormzType.email,
                ),
              ),
            ],
          ),
          SizedBox(height: 12.h),
          WScaleAnimation(
            onTap: () {
              showCustomBottomSheet(
                context,
                (bottomSheetContext) => BlocProvider.value(
                  value: context.read<AuthBloc>(),
                  child: const GenderBottomsheet(),
                ),
              );
            },
            child: Container(
              padding: const EdgeInsets.all(12),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: Colors.white38,
              ),
              child: Row(
                children: [
                  BlocBuilder<AuthBloc, AuthState>(
                    builder: (context, state) {
                      return Text(state.user!.gender ? "Male" : "Female");
                    },
                  ),
                  const Spacer(),
                  SvgPicture.asset(AppIcons.arrowBottom),
                  // SizedBox(child: icon),
                ],
              ),
            ),
          ),
          SizedBox(height: 12.h),
          WTextField(
            validationBloc: validatorBlocPassword,
            textInputAction: TextInputAction.next,
            disabledColor: Colors.white38,
            title: 'password'.tr(),
            isObscureText: true,
            textStyle: Theme.of(context).textTheme.titleMedium,
            titleTextStyle: Theme.of(context).textTheme.titleMedium,
            margin: const EdgeInsets.symmetric(horizontal: 16),
            hintText: 'enter_new_password'.tr(),
            hintTextStyle: Theme.of(context).textTheme.titleMedium!.copyWith(
                fontSize: 14,
                color: black.withOpacity(0.8),
                fontWeight: FontWeight.w400),
            controller: _passwordController,
            onChanged: (value) {},
            rules: const PasswordRules(
                minPasswordLength: 6,
                maxPasswordLength: 15,
                includeNumbers: true,
                includeLetters: true,
                includeChars: false),
            validationType: FormzType.password,
          ),
          WTextField(
            textInputAction: TextInputAction.send,
            disabledColor: Colors.white38,
            title: 'confirm_password'.tr(),
            isObscureText: true,
            textStyle: Theme.of(context).textTheme.titleMedium,
            titleTextStyle: Theme.of(context).textTheme.titleMedium,
            margin: const EdgeInsets.symmetric(horizontal: 16),
            hintText: 'confirm_password'.tr(),
            hintTextStyle: Theme.of(context).textTheme.titleMedium!.copyWith(
                fontSize: 14,
                color: black.withOpacity(0.8),
                fontWeight: FontWeight.w400),
            controller: _confirmPasswordController,
            onChanged: (value) {},
          ),
          SizedBox(height: 16.h),
          Align(
            child: BlocBuilder<AuthBloc, AuthState>(
              builder: (context, state) {
                return WButton(
                  text: 'register'.tr(),
                  textStyle: Theme.of(context).textTheme.headlineMedium,
                  color: primary,
                  margin: const EdgeInsets.all(16),
                  onTap: () {
                    if (validatorBlocEmail.state.status ==
                                FormzStatus.submissionSuccess &&
                            validatorBlocPassword.state.status ==
                                FormzStatus.submissionSuccess &&
                            _firstNameController.text.length > 3 &&
                            _lastNameController.text.length > 3
                        // _passwordController == _confirmPasswordController
                        ) {
                      context.read<AuthBloc>().add(AuthEvent.signUp(
                          firstName: _firstNameController.text,
                          surName: _lastNameController.text,
                          onFailure: (value) {
                            debugPrint("failure:  $value");
                            context
                                .read<ShowPopUpBloc>()
                                .add(ShowPopUpEvent.showFailure(text: value));
                          },
                          onSuccess: (value) {
                            debugPrint("success:  $value");
                            Navigator.of(context).pushReplacement(
                                fade(page: const HomeScreen()));
                          },
                          gender: state.user!.gender,
                          age: int.parse(_ageController.text),
                          email: _emailController.text,
                          password: _passwordController.text,
                          image: 'www.image.com'));
                    } else {
                      context.read<ShowPopUpBloc>().add(
                          ShowPopUpEvent.showFailure(text: "fill_below".tr()));
                    }
                  },
                );
              },
            ),
          ),
          SizedBox(height: 16.h),
          Align(
            child: WScaleAnimation(
              onTap: () {
                Navigator.of(context).push(
                  fade(
                    page: BlocProvider.value(
                      value: context.read<ShowPopUpBloc>(),
                      child: const LoginScreen(),
                    ),
                  ),
                );
              },
              child: Text('login'.tr(),
                  style: Theme.of(context)
                      .textTheme
                      .labelMedium!
                      .copyWith(fontSize: 18)),
            ),
          ),
        ],
      ),
    );
  }
}
