import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/login/domain/usecase/get_user.dart';
import 'package:cooking/features/login/domain/usecase/sign_in.dart';
import 'package:cooking/features/login/domain/usecase/sign_out.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../core/models/authentication_status.dart';
import '../../../../core/models/formz/formz_status.dart';
import '../../data/model/user_info.dart';
import '../../domain/repository/repository.dart';
import '../../domain/usecase/sign_up.dart';

part 'auth_bloc.freezed.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository _repository;
  // late StreamSubscription<User?> _streamSubscription;

  AuthBloc(AuthRepository repository)
      : _repository = repository,
        super(const _AuthState(
            user: UserModel(
                firstName: '',
                lastName: '',
                email: '',
                password: 'password',
                age: 12,
                gender: true,
                image: ''))) {
    // _streamSubscription = _repository.status.listen((user) {
    //   debugPrint("inside bloc $user");
    //
    //   if (user != null &&
    //       state.authStatus != AuthenticationStatus.authenticated) {
    //     add(const AuthEvent.changedStatus(
    //         status: AuthenticationStatus.authenticated));
    //   } else if (user == null &&
    //       state.authStatus != AuthenticationStatus.authenticated) {
    //     add(const AuthEvent.changedStatus(
    //         status: AuthenticationStatus.authenticated));
    //   }
    // });

    on<_SignIn>((event, emit) async {
      final usecase = SignInUseCase(_repository);
      final result = await usecase
          .call(SignInParams(email: event.email, password: event.password));
      result.either((value) {
        event.onFailure(value.errorMessage);
      }, (__) {
        emit(state.copyWith(authStatus: AuthenticationStatus.authenticated));
        event.onSuccess('');
      });
    });

    on<_SignUp>((event, emit) async {
      final usecase = SignUpUseCase(_repository);
      final result = await usecase.call(SignUpParams(
          firstName: event.firstName,
          lastName: event.surName,
          email: event.email,
          password: event.password,
          age: event.age,
          gender: event.gender,
          image: event.image));
      result.either((value) {
        event.onFailure(value.errorMessage);
        // emit(
        //   state.copyWith(
        //       status: FormzStatus.submissionFailure,
        //       errorMessage: value.errorMessage),
        // );
        emit(state.copyWith(authStatus: AuthenticationStatus.unauthenticated));
      }, (__) {
        event.onSuccess('');
        emit(state.copyWith(authStatus: AuthenticationStatus.authenticated));
      });
    });

    on<_SignOut>((event, emit) async {
      final usecase = SignOutUseCase(_repository);
      final result = await usecase.call(NoParams());
      result.either((value) {
        // emit(
        //   state.copyWith(
        //       status: FormzStatus.submissionFailure,
        //       errorMessage: value.errorMessage),
        // );
        event.onFailure(value.errorMessage);
      }, (value) {
        event.onSuccess('');
        emit(state.copyWith(authStatus: AuthenticationStatus.unauthenticated));
      });
    });

    on<_GetUserInfo>((event, emit) async {
      emit(state.copyWith(status: FormzStatus.submissionInProgress));
      final usecase = GetUserUseCase(_repository);
      final result = await usecase.call(NoParams());
      result.either((error) {
        emit(
          state.copyWith(
              status: FormzStatus.submissionFailure,
              errorMessage: error.errorMessage),
        );
      }, (data) {
        debugPrint('bloc successfull');
        emit(state.copyWith(status: FormzStatus.submissionSuccess, user: data));
      });
    });

    on<_ChooseGender>((event, emit) {
      emit(state.copyWith(user: state.user?.copyWith(gender: event.isMale)));
    });
  }
}
