// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'auth_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AuthEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signIn,
    required TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)
        signUp,
    required TResult Function() getUserInfo,
    required TResult Function(bool isMale) chooseGender,
    required TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)
        resetPassword,
    required TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signOut,
    required TResult Function(AuthenticationStatus status) changedStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult? Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult? Function()? getUserInfo,
    TResult? Function(bool isMale)? chooseGender,
    TResult? Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult? Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult? Function(AuthenticationStatus status)? changedStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult Function()? getUserInfo,
    TResult Function(bool isMale)? chooseGender,
    TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult Function(AuthenticationStatus status)? changedStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignIn value) signIn,
    required TResult Function(_SignUp value) signUp,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_ChooseGender value) chooseGender,
    required TResult Function(_ResetPassword value) resetPassword,
    required TResult Function(_SignOut value) signOut,
    required TResult Function(_ChangedStatus value) changedStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SignIn value)? signIn,
    TResult? Function(_SignUp value)? signUp,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_ChooseGender value)? chooseGender,
    TResult? Function(_ResetPassword value)? resetPassword,
    TResult? Function(_SignOut value)? signOut,
    TResult? Function(_ChangedStatus value)? changedStatus,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignIn value)? signIn,
    TResult Function(_SignUp value)? signUp,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_ChooseGender value)? chooseGender,
    TResult Function(_ResetPassword value)? resetPassword,
    TResult Function(_SignOut value)? signOut,
    TResult Function(_ChangedStatus value)? changedStatus,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthEventCopyWith<$Res> {
  factory $AuthEventCopyWith(AuthEvent value, $Res Function(AuthEvent) then) =
      _$AuthEventCopyWithImpl<$Res, AuthEvent>;
}

/// @nodoc
class _$AuthEventCopyWithImpl<$Res, $Val extends AuthEvent>
    implements $AuthEventCopyWith<$Res> {
  _$AuthEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$SignInImplCopyWith<$Res> {
  factory _$$SignInImplCopyWith(
          _$SignInImpl value, $Res Function(_$SignInImpl) then) =
      __$$SignInImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String email,
      String password,
      ValueChanged<String> onFailure,
      ValueChanged<String> onSuccess});
}

/// @nodoc
class __$$SignInImplCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res, _$SignInImpl>
    implements _$$SignInImplCopyWith<$Res> {
  __$$SignInImplCopyWithImpl(
      _$SignInImpl _value, $Res Function(_$SignInImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = null,
    Object? password = null,
    Object? onFailure = null,
    Object? onSuccess = null,
  }) {
    return _then(_$SignInImpl(
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
      onSuccess: null == onSuccess
          ? _value.onSuccess
          : onSuccess // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
    ));
  }
}

/// @nodoc

class _$SignInImpl implements _SignIn {
  const _$SignInImpl(
      {required this.email,
      required this.password,
      required this.onFailure,
      required this.onSuccess});

  @override
  final String email;
  @override
  final String password;
  @override
  final ValueChanged<String> onFailure;
  @override
  final ValueChanged<String> onSuccess;

  @override
  String toString() {
    return 'AuthEvent.signIn(email: $email, password: $password, onFailure: $onFailure, onSuccess: $onSuccess)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SignInImpl &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.password, password) ||
                other.password == password) &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure) &&
            (identical(other.onSuccess, onSuccess) ||
                other.onSuccess == onSuccess));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, email, password, onFailure, onSuccess);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SignInImplCopyWith<_$SignInImpl> get copyWith =>
      __$$SignInImplCopyWithImpl<_$SignInImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signIn,
    required TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)
        signUp,
    required TResult Function() getUserInfo,
    required TResult Function(bool isMale) chooseGender,
    required TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)
        resetPassword,
    required TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signOut,
    required TResult Function(AuthenticationStatus status) changedStatus,
  }) {
    return signIn(email, password, onFailure, onSuccess);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult? Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult? Function()? getUserInfo,
    TResult? Function(bool isMale)? chooseGender,
    TResult? Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult? Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult? Function(AuthenticationStatus status)? changedStatus,
  }) {
    return signIn?.call(email, password, onFailure, onSuccess);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult Function()? getUserInfo,
    TResult Function(bool isMale)? chooseGender,
    TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult Function(AuthenticationStatus status)? changedStatus,
    required TResult orElse(),
  }) {
    if (signIn != null) {
      return signIn(email, password, onFailure, onSuccess);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignIn value) signIn,
    required TResult Function(_SignUp value) signUp,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_ChooseGender value) chooseGender,
    required TResult Function(_ResetPassword value) resetPassword,
    required TResult Function(_SignOut value) signOut,
    required TResult Function(_ChangedStatus value) changedStatus,
  }) {
    return signIn(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SignIn value)? signIn,
    TResult? Function(_SignUp value)? signUp,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_ChooseGender value)? chooseGender,
    TResult? Function(_ResetPassword value)? resetPassword,
    TResult? Function(_SignOut value)? signOut,
    TResult? Function(_ChangedStatus value)? changedStatus,
  }) {
    return signIn?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignIn value)? signIn,
    TResult Function(_SignUp value)? signUp,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_ChooseGender value)? chooseGender,
    TResult Function(_ResetPassword value)? resetPassword,
    TResult Function(_SignOut value)? signOut,
    TResult Function(_ChangedStatus value)? changedStatus,
    required TResult orElse(),
  }) {
    if (signIn != null) {
      return signIn(this);
    }
    return orElse();
  }
}

abstract class _SignIn implements AuthEvent {
  const factory _SignIn(
      {required final String email,
      required final String password,
      required final ValueChanged<String> onFailure,
      required final ValueChanged<String> onSuccess}) = _$SignInImpl;

  String get email;
  String get password;
  ValueChanged<String> get onFailure;
  ValueChanged<String> get onSuccess;
  @JsonKey(ignore: true)
  _$$SignInImplCopyWith<_$SignInImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SignUpImplCopyWith<$Res> {
  factory _$$SignUpImplCopyWith(
          _$SignUpImpl value, $Res Function(_$SignUpImpl) then) =
      __$$SignUpImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String firstName,
      String surName,
      ValueChanged<String> onFailure,
      ValueChanged<String> onSuccess,
      bool gender,
      int age,
      String email,
      String image,
      String password});
}

/// @nodoc
class __$$SignUpImplCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res, _$SignUpImpl>
    implements _$$SignUpImplCopyWith<$Res> {
  __$$SignUpImplCopyWithImpl(
      _$SignUpImpl _value, $Res Function(_$SignUpImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? firstName = null,
    Object? surName = null,
    Object? onFailure = null,
    Object? onSuccess = null,
    Object? gender = null,
    Object? age = null,
    Object? email = null,
    Object? image = null,
    Object? password = null,
  }) {
    return _then(_$SignUpImpl(
      firstName: null == firstName
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String,
      surName: null == surName
          ? _value.surName
          : surName // ignore: cast_nullable_to_non_nullable
              as String,
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
      onSuccess: null == onSuccess
          ? _value.onSuccess
          : onSuccess // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as bool,
      age: null == age
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      password: null == password
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SignUpImpl implements _SignUp {
  const _$SignUpImpl(
      {required this.firstName,
      required this.surName,
      required this.onFailure,
      required this.onSuccess,
      required this.gender,
      required this.age,
      required this.email,
      required this.image,
      required this.password});

  @override
  final String firstName;
  @override
  final String surName;
  @override
  final ValueChanged<String> onFailure;
  @override
  final ValueChanged<String> onSuccess;
  @override
  final bool gender;
  @override
  final int age;
  @override
  final String email;
  @override
  final String image;
  @override
  final String password;

  @override
  String toString() {
    return 'AuthEvent.signUp(firstName: $firstName, surName: $surName, onFailure: $onFailure, onSuccess: $onSuccess, gender: $gender, age: $age, email: $email, image: $image, password: $password)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SignUpImpl &&
            (identical(other.firstName, firstName) ||
                other.firstName == firstName) &&
            (identical(other.surName, surName) || other.surName == surName) &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure) &&
            (identical(other.onSuccess, onSuccess) ||
                other.onSuccess == onSuccess) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.age, age) || other.age == age) &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.image, image) || other.image == image) &&
            (identical(other.password, password) ||
                other.password == password));
  }

  @override
  int get hashCode => Object.hash(runtimeType, firstName, surName, onFailure,
      onSuccess, gender, age, email, image, password);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SignUpImplCopyWith<_$SignUpImpl> get copyWith =>
      __$$SignUpImplCopyWithImpl<_$SignUpImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signIn,
    required TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)
        signUp,
    required TResult Function() getUserInfo,
    required TResult Function(bool isMale) chooseGender,
    required TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)
        resetPassword,
    required TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signOut,
    required TResult Function(AuthenticationStatus status) changedStatus,
  }) {
    return signUp(firstName, surName, onFailure, onSuccess, gender, age, email,
        image, password);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult? Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult? Function()? getUserInfo,
    TResult? Function(bool isMale)? chooseGender,
    TResult? Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult? Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult? Function(AuthenticationStatus status)? changedStatus,
  }) {
    return signUp?.call(firstName, surName, onFailure, onSuccess, gender, age,
        email, image, password);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult Function()? getUserInfo,
    TResult Function(bool isMale)? chooseGender,
    TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult Function(AuthenticationStatus status)? changedStatus,
    required TResult orElse(),
  }) {
    if (signUp != null) {
      return signUp(firstName, surName, onFailure, onSuccess, gender, age,
          email, image, password);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignIn value) signIn,
    required TResult Function(_SignUp value) signUp,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_ChooseGender value) chooseGender,
    required TResult Function(_ResetPassword value) resetPassword,
    required TResult Function(_SignOut value) signOut,
    required TResult Function(_ChangedStatus value) changedStatus,
  }) {
    return signUp(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SignIn value)? signIn,
    TResult? Function(_SignUp value)? signUp,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_ChooseGender value)? chooseGender,
    TResult? Function(_ResetPassword value)? resetPassword,
    TResult? Function(_SignOut value)? signOut,
    TResult? Function(_ChangedStatus value)? changedStatus,
  }) {
    return signUp?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignIn value)? signIn,
    TResult Function(_SignUp value)? signUp,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_ChooseGender value)? chooseGender,
    TResult Function(_ResetPassword value)? resetPassword,
    TResult Function(_SignOut value)? signOut,
    TResult Function(_ChangedStatus value)? changedStatus,
    required TResult orElse(),
  }) {
    if (signUp != null) {
      return signUp(this);
    }
    return orElse();
  }
}

abstract class _SignUp implements AuthEvent {
  const factory _SignUp(
      {required final String firstName,
      required final String surName,
      required final ValueChanged<String> onFailure,
      required final ValueChanged<String> onSuccess,
      required final bool gender,
      required final int age,
      required final String email,
      required final String image,
      required final String password}) = _$SignUpImpl;

  String get firstName;
  String get surName;
  ValueChanged<String> get onFailure;
  ValueChanged<String> get onSuccess;
  bool get gender;
  int get age;
  String get email;
  String get image;
  String get password;
  @JsonKey(ignore: true)
  _$$SignUpImplCopyWith<_$SignUpImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetUserInfoImplCopyWith<$Res> {
  factory _$$GetUserInfoImplCopyWith(
          _$GetUserInfoImpl value, $Res Function(_$GetUserInfoImpl) then) =
      __$$GetUserInfoImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetUserInfoImplCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res, _$GetUserInfoImpl>
    implements _$$GetUserInfoImplCopyWith<$Res> {
  __$$GetUserInfoImplCopyWithImpl(
      _$GetUserInfoImpl _value, $Res Function(_$GetUserInfoImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetUserInfoImpl implements _GetUserInfo {
  const _$GetUserInfoImpl();

  @override
  String toString() {
    return 'AuthEvent.getUserInfo()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetUserInfoImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signIn,
    required TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)
        signUp,
    required TResult Function() getUserInfo,
    required TResult Function(bool isMale) chooseGender,
    required TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)
        resetPassword,
    required TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signOut,
    required TResult Function(AuthenticationStatus status) changedStatus,
  }) {
    return getUserInfo();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult? Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult? Function()? getUserInfo,
    TResult? Function(bool isMale)? chooseGender,
    TResult? Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult? Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult? Function(AuthenticationStatus status)? changedStatus,
  }) {
    return getUserInfo?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult Function()? getUserInfo,
    TResult Function(bool isMale)? chooseGender,
    TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult Function(AuthenticationStatus status)? changedStatus,
    required TResult orElse(),
  }) {
    if (getUserInfo != null) {
      return getUserInfo();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignIn value) signIn,
    required TResult Function(_SignUp value) signUp,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_ChooseGender value) chooseGender,
    required TResult Function(_ResetPassword value) resetPassword,
    required TResult Function(_SignOut value) signOut,
    required TResult Function(_ChangedStatus value) changedStatus,
  }) {
    return getUserInfo(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SignIn value)? signIn,
    TResult? Function(_SignUp value)? signUp,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_ChooseGender value)? chooseGender,
    TResult? Function(_ResetPassword value)? resetPassword,
    TResult? Function(_SignOut value)? signOut,
    TResult? Function(_ChangedStatus value)? changedStatus,
  }) {
    return getUserInfo?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignIn value)? signIn,
    TResult Function(_SignUp value)? signUp,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_ChooseGender value)? chooseGender,
    TResult Function(_ResetPassword value)? resetPassword,
    TResult Function(_SignOut value)? signOut,
    TResult Function(_ChangedStatus value)? changedStatus,
    required TResult orElse(),
  }) {
    if (getUserInfo != null) {
      return getUserInfo(this);
    }
    return orElse();
  }
}

abstract class _GetUserInfo implements AuthEvent {
  const factory _GetUserInfo() = _$GetUserInfoImpl;
}

/// @nodoc
abstract class _$$ChooseGenderImplCopyWith<$Res> {
  factory _$$ChooseGenderImplCopyWith(
          _$ChooseGenderImpl value, $Res Function(_$ChooseGenderImpl) then) =
      __$$ChooseGenderImplCopyWithImpl<$Res>;
  @useResult
  $Res call({bool isMale});
}

/// @nodoc
class __$$ChooseGenderImplCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res, _$ChooseGenderImpl>
    implements _$$ChooseGenderImplCopyWith<$Res> {
  __$$ChooseGenderImplCopyWithImpl(
      _$ChooseGenderImpl _value, $Res Function(_$ChooseGenderImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isMale = null,
  }) {
    return _then(_$ChooseGenderImpl(
      isMale: null == isMale
          ? _value.isMale
          : isMale // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ChooseGenderImpl implements _ChooseGender {
  const _$ChooseGenderImpl({required this.isMale});

  @override
  final bool isMale;

  @override
  String toString() {
    return 'AuthEvent.chooseGender(isMale: $isMale)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChooseGenderImpl &&
            (identical(other.isMale, isMale) || other.isMale == isMale));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isMale);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChooseGenderImplCopyWith<_$ChooseGenderImpl> get copyWith =>
      __$$ChooseGenderImplCopyWithImpl<_$ChooseGenderImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signIn,
    required TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)
        signUp,
    required TResult Function() getUserInfo,
    required TResult Function(bool isMale) chooseGender,
    required TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)
        resetPassword,
    required TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signOut,
    required TResult Function(AuthenticationStatus status) changedStatus,
  }) {
    return chooseGender(isMale);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult? Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult? Function()? getUserInfo,
    TResult? Function(bool isMale)? chooseGender,
    TResult? Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult? Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult? Function(AuthenticationStatus status)? changedStatus,
  }) {
    return chooseGender?.call(isMale);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult Function()? getUserInfo,
    TResult Function(bool isMale)? chooseGender,
    TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult Function(AuthenticationStatus status)? changedStatus,
    required TResult orElse(),
  }) {
    if (chooseGender != null) {
      return chooseGender(isMale);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignIn value) signIn,
    required TResult Function(_SignUp value) signUp,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_ChooseGender value) chooseGender,
    required TResult Function(_ResetPassword value) resetPassword,
    required TResult Function(_SignOut value) signOut,
    required TResult Function(_ChangedStatus value) changedStatus,
  }) {
    return chooseGender(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SignIn value)? signIn,
    TResult? Function(_SignUp value)? signUp,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_ChooseGender value)? chooseGender,
    TResult? Function(_ResetPassword value)? resetPassword,
    TResult? Function(_SignOut value)? signOut,
    TResult? Function(_ChangedStatus value)? changedStatus,
  }) {
    return chooseGender?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignIn value)? signIn,
    TResult Function(_SignUp value)? signUp,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_ChooseGender value)? chooseGender,
    TResult Function(_ResetPassword value)? resetPassword,
    TResult Function(_SignOut value)? signOut,
    TResult Function(_ChangedStatus value)? changedStatus,
    required TResult orElse(),
  }) {
    if (chooseGender != null) {
      return chooseGender(this);
    }
    return orElse();
  }
}

abstract class _ChooseGender implements AuthEvent {
  const factory _ChooseGender({required final bool isMale}) =
      _$ChooseGenderImpl;

  bool get isMale;
  @JsonKey(ignore: true)
  _$$ChooseGenderImplCopyWith<_$ChooseGenderImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ResetPasswordImplCopyWith<$Res> {
  factory _$$ResetPasswordImplCopyWith(
          _$ResetPasswordImpl value, $Res Function(_$ResetPasswordImpl) then) =
      __$$ResetPasswordImplCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String email,
      ValueChanged<String> onFailure,
      ValueChanged<String> onSuccess});
}

/// @nodoc
class __$$ResetPasswordImplCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res, _$ResetPasswordImpl>
    implements _$$ResetPasswordImplCopyWith<$Res> {
  __$$ResetPasswordImplCopyWithImpl(
      _$ResetPasswordImpl _value, $Res Function(_$ResetPasswordImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? email = null,
    Object? onFailure = null,
    Object? onSuccess = null,
  }) {
    return _then(_$ResetPasswordImpl(
      email: null == email
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
      onSuccess: null == onSuccess
          ? _value.onSuccess
          : onSuccess // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
    ));
  }
}

/// @nodoc

class _$ResetPasswordImpl implements _ResetPassword {
  const _$ResetPasswordImpl(
      {required this.email, required this.onFailure, required this.onSuccess});

  @override
  final String email;
  @override
  final ValueChanged<String> onFailure;
  @override
  final ValueChanged<String> onSuccess;

  @override
  String toString() {
    return 'AuthEvent.resetPassword(email: $email, onFailure: $onFailure, onSuccess: $onSuccess)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ResetPasswordImpl &&
            (identical(other.email, email) || other.email == email) &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure) &&
            (identical(other.onSuccess, onSuccess) ||
                other.onSuccess == onSuccess));
  }

  @override
  int get hashCode => Object.hash(runtimeType, email, onFailure, onSuccess);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ResetPasswordImplCopyWith<_$ResetPasswordImpl> get copyWith =>
      __$$ResetPasswordImplCopyWithImpl<_$ResetPasswordImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signIn,
    required TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)
        signUp,
    required TResult Function() getUserInfo,
    required TResult Function(bool isMale) chooseGender,
    required TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)
        resetPassword,
    required TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signOut,
    required TResult Function(AuthenticationStatus status) changedStatus,
  }) {
    return resetPassword(email, onFailure, onSuccess);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult? Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult? Function()? getUserInfo,
    TResult? Function(bool isMale)? chooseGender,
    TResult? Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult? Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult? Function(AuthenticationStatus status)? changedStatus,
  }) {
    return resetPassword?.call(email, onFailure, onSuccess);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult Function()? getUserInfo,
    TResult Function(bool isMale)? chooseGender,
    TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult Function(AuthenticationStatus status)? changedStatus,
    required TResult orElse(),
  }) {
    if (resetPassword != null) {
      return resetPassword(email, onFailure, onSuccess);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignIn value) signIn,
    required TResult Function(_SignUp value) signUp,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_ChooseGender value) chooseGender,
    required TResult Function(_ResetPassword value) resetPassword,
    required TResult Function(_SignOut value) signOut,
    required TResult Function(_ChangedStatus value) changedStatus,
  }) {
    return resetPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SignIn value)? signIn,
    TResult? Function(_SignUp value)? signUp,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_ChooseGender value)? chooseGender,
    TResult? Function(_ResetPassword value)? resetPassword,
    TResult? Function(_SignOut value)? signOut,
    TResult? Function(_ChangedStatus value)? changedStatus,
  }) {
    return resetPassword?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignIn value)? signIn,
    TResult Function(_SignUp value)? signUp,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_ChooseGender value)? chooseGender,
    TResult Function(_ResetPassword value)? resetPassword,
    TResult Function(_SignOut value)? signOut,
    TResult Function(_ChangedStatus value)? changedStatus,
    required TResult orElse(),
  }) {
    if (resetPassword != null) {
      return resetPassword(this);
    }
    return orElse();
  }
}

abstract class _ResetPassword implements AuthEvent {
  const factory _ResetPassword(
      {required final String email,
      required final ValueChanged<String> onFailure,
      required final ValueChanged<String> onSuccess}) = _$ResetPasswordImpl;

  String get email;
  ValueChanged<String> get onFailure;
  ValueChanged<String> get onSuccess;
  @JsonKey(ignore: true)
  _$$ResetPasswordImplCopyWith<_$ResetPasswordImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SignOutImplCopyWith<$Res> {
  factory _$$SignOutImplCopyWith(
          _$SignOutImpl value, $Res Function(_$SignOutImpl) then) =
      __$$SignOutImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ValueChanged<String> onFailure, ValueChanged<String> onSuccess});
}

/// @nodoc
class __$$SignOutImplCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res, _$SignOutImpl>
    implements _$$SignOutImplCopyWith<$Res> {
  __$$SignOutImplCopyWithImpl(
      _$SignOutImpl _value, $Res Function(_$SignOutImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? onFailure = null,
    Object? onSuccess = null,
  }) {
    return _then(_$SignOutImpl(
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
      onSuccess: null == onSuccess
          ? _value.onSuccess
          : onSuccess // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
    ));
  }
}

/// @nodoc

class _$SignOutImpl implements _SignOut {
  const _$SignOutImpl({required this.onFailure, required this.onSuccess});

  @override
  final ValueChanged<String> onFailure;
  @override
  final ValueChanged<String> onSuccess;

  @override
  String toString() {
    return 'AuthEvent.signOut(onFailure: $onFailure, onSuccess: $onSuccess)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SignOutImpl &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure) &&
            (identical(other.onSuccess, onSuccess) ||
                other.onSuccess == onSuccess));
  }

  @override
  int get hashCode => Object.hash(runtimeType, onFailure, onSuccess);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SignOutImplCopyWith<_$SignOutImpl> get copyWith =>
      __$$SignOutImplCopyWithImpl<_$SignOutImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signIn,
    required TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)
        signUp,
    required TResult Function() getUserInfo,
    required TResult Function(bool isMale) chooseGender,
    required TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)
        resetPassword,
    required TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signOut,
    required TResult Function(AuthenticationStatus status) changedStatus,
  }) {
    return signOut(onFailure, onSuccess);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult? Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult? Function()? getUserInfo,
    TResult? Function(bool isMale)? chooseGender,
    TResult? Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult? Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult? Function(AuthenticationStatus status)? changedStatus,
  }) {
    return signOut?.call(onFailure, onSuccess);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult Function()? getUserInfo,
    TResult Function(bool isMale)? chooseGender,
    TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult Function(AuthenticationStatus status)? changedStatus,
    required TResult orElse(),
  }) {
    if (signOut != null) {
      return signOut(onFailure, onSuccess);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignIn value) signIn,
    required TResult Function(_SignUp value) signUp,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_ChooseGender value) chooseGender,
    required TResult Function(_ResetPassword value) resetPassword,
    required TResult Function(_SignOut value) signOut,
    required TResult Function(_ChangedStatus value) changedStatus,
  }) {
    return signOut(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SignIn value)? signIn,
    TResult? Function(_SignUp value)? signUp,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_ChooseGender value)? chooseGender,
    TResult? Function(_ResetPassword value)? resetPassword,
    TResult? Function(_SignOut value)? signOut,
    TResult? Function(_ChangedStatus value)? changedStatus,
  }) {
    return signOut?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignIn value)? signIn,
    TResult Function(_SignUp value)? signUp,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_ChooseGender value)? chooseGender,
    TResult Function(_ResetPassword value)? resetPassword,
    TResult Function(_SignOut value)? signOut,
    TResult Function(_ChangedStatus value)? changedStatus,
    required TResult orElse(),
  }) {
    if (signOut != null) {
      return signOut(this);
    }
    return orElse();
  }
}

abstract class _SignOut implements AuthEvent {
  const factory _SignOut(
      {required final ValueChanged<String> onFailure,
      required final ValueChanged<String> onSuccess}) = _$SignOutImpl;

  ValueChanged<String> get onFailure;
  ValueChanged<String> get onSuccess;
  @JsonKey(ignore: true)
  _$$SignOutImplCopyWith<_$SignOutImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ChangedStatusImplCopyWith<$Res> {
  factory _$$ChangedStatusImplCopyWith(
          _$ChangedStatusImpl value, $Res Function(_$ChangedStatusImpl) then) =
      __$$ChangedStatusImplCopyWithImpl<$Res>;
  @useResult
  $Res call({AuthenticationStatus status});
}

/// @nodoc
class __$$ChangedStatusImplCopyWithImpl<$Res>
    extends _$AuthEventCopyWithImpl<$Res, _$ChangedStatusImpl>
    implements _$$ChangedStatusImplCopyWith<$Res> {
  __$$ChangedStatusImplCopyWithImpl(
      _$ChangedStatusImpl _value, $Res Function(_$ChangedStatusImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
  }) {
    return _then(_$ChangedStatusImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as AuthenticationStatus,
    ));
  }
}

/// @nodoc

class _$ChangedStatusImpl implements _ChangedStatus {
  const _$ChangedStatusImpl({required this.status});

  @override
  final AuthenticationStatus status;

  @override
  String toString() {
    return 'AuthEvent.changedStatus(status: $status)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChangedStatusImpl &&
            (identical(other.status, status) || other.status == status));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChangedStatusImplCopyWith<_$ChangedStatusImpl> get copyWith =>
      __$$ChangedStatusImplCopyWithImpl<_$ChangedStatusImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signIn,
    required TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)
        signUp,
    required TResult Function() getUserInfo,
    required TResult Function(bool isMale) chooseGender,
    required TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)
        resetPassword,
    required TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)
        signOut,
    required TResult Function(AuthenticationStatus status) changedStatus,
  }) {
    return changedStatus(status);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult? Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult? Function()? getUserInfo,
    TResult? Function(bool isMale)? chooseGender,
    TResult? Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult? Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult? Function(AuthenticationStatus status)? changedStatus,
  }) {
    return changedStatus?.call(status);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String email, String password,
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signIn,
    TResult Function(
            String firstName,
            String surName,
            ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess,
            bool gender,
            int age,
            String email,
            String image,
            String password)?
        signUp,
    TResult Function()? getUserInfo,
    TResult Function(bool isMale)? chooseGender,
    TResult Function(String email, ValueChanged<String> onFailure,
            ValueChanged<String> onSuccess)?
        resetPassword,
    TResult Function(
            ValueChanged<String> onFailure, ValueChanged<String> onSuccess)?
        signOut,
    TResult Function(AuthenticationStatus status)? changedStatus,
    required TResult orElse(),
  }) {
    if (changedStatus != null) {
      return changedStatus(status);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SignIn value) signIn,
    required TResult Function(_SignUp value) signUp,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_ChooseGender value) chooseGender,
    required TResult Function(_ResetPassword value) resetPassword,
    required TResult Function(_SignOut value) signOut,
    required TResult Function(_ChangedStatus value) changedStatus,
  }) {
    return changedStatus(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SignIn value)? signIn,
    TResult? Function(_SignUp value)? signUp,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_ChooseGender value)? chooseGender,
    TResult? Function(_ResetPassword value)? resetPassword,
    TResult? Function(_SignOut value)? signOut,
    TResult? Function(_ChangedStatus value)? changedStatus,
  }) {
    return changedStatus?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SignIn value)? signIn,
    TResult Function(_SignUp value)? signUp,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_ChooseGender value)? chooseGender,
    TResult Function(_ResetPassword value)? resetPassword,
    TResult Function(_SignOut value)? signOut,
    TResult Function(_ChangedStatus value)? changedStatus,
    required TResult orElse(),
  }) {
    if (changedStatus != null) {
      return changedStatus(this);
    }
    return orElse();
  }
}

abstract class _ChangedStatus implements AuthEvent {
  const factory _ChangedStatus({required final AuthenticationStatus status}) =
      _$ChangedStatusImpl;

  AuthenticationStatus get status;
  @JsonKey(ignore: true)
  _$$ChangedStatusImplCopyWith<_$ChangedStatusImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AuthState {
  AuthenticationStatus get authStatus => throw _privateConstructorUsedError;
  UserModel? get user => throw _privateConstructorUsedError;
  FormzStatus get status => throw _privateConstructorUsedError;
  String get errorMessage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AuthStateCopyWith<AuthState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AuthStateCopyWith<$Res> {
  factory $AuthStateCopyWith(AuthState value, $Res Function(AuthState) then) =
      _$AuthStateCopyWithImpl<$Res, AuthState>;
  @useResult
  $Res call(
      {AuthenticationStatus authStatus,
      UserModel? user,
      FormzStatus status,
      String errorMessage});
}

/// @nodoc
class _$AuthStateCopyWithImpl<$Res, $Val extends AuthState>
    implements $AuthStateCopyWith<$Res> {
  _$AuthStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? authStatus = null,
    Object? user = freezed,
    Object? status = null,
    Object? errorMessage = null,
  }) {
    return _then(_value.copyWith(
      authStatus: null == authStatus
          ? _value.authStatus
          : authStatus // ignore: cast_nullable_to_non_nullable
              as AuthenticationStatus,
      user: freezed == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as UserModel?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AuthStateImplCopyWith<$Res>
    implements $AuthStateCopyWith<$Res> {
  factory _$$AuthStateImplCopyWith(
          _$AuthStateImpl value, $Res Function(_$AuthStateImpl) then) =
      __$$AuthStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {AuthenticationStatus authStatus,
      UserModel? user,
      FormzStatus status,
      String errorMessage});
}

/// @nodoc
class __$$AuthStateImplCopyWithImpl<$Res>
    extends _$AuthStateCopyWithImpl<$Res, _$AuthStateImpl>
    implements _$$AuthStateImplCopyWith<$Res> {
  __$$AuthStateImplCopyWithImpl(
      _$AuthStateImpl _value, $Res Function(_$AuthStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? authStatus = null,
    Object? user = freezed,
    Object? status = null,
    Object? errorMessage = null,
  }) {
    return _then(_$AuthStateImpl(
      authStatus: null == authStatus
          ? _value.authStatus
          : authStatus // ignore: cast_nullable_to_non_nullable
              as AuthenticationStatus,
      user: freezed == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as UserModel?,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AuthStateImpl implements _AuthState {
  const _$AuthStateImpl(
      {this.authStatus = AuthenticationStatus.unknown,
      this.user = null,
      this.status = FormzStatus.pure,
      this.errorMessage = ''});

  @override
  @JsonKey()
  final AuthenticationStatus authStatus;
  @override
  @JsonKey()
  final UserModel? user;
  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final String errorMessage;

  @override
  String toString() {
    return 'AuthState(authStatus: $authStatus, user: $user, status: $status, errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AuthStateImpl &&
            (identical(other.authStatus, authStatus) ||
                other.authStatus == authStatus) &&
            (identical(other.user, user) || other.user == user) &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, authStatus, user, status, errorMessage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AuthStateImplCopyWith<_$AuthStateImpl> get copyWith =>
      __$$AuthStateImplCopyWithImpl<_$AuthStateImpl>(this, _$identity);
}

abstract class _AuthState implements AuthState {
  const factory _AuthState(
      {final AuthenticationStatus authStatus,
      final UserModel? user,
      final FormzStatus status,
      final String errorMessage}) = _$AuthStateImpl;

  @override
  AuthenticationStatus get authStatus;
  @override
  UserModel? get user;
  @override
  FormzStatus get status;
  @override
  String get errorMessage;
  @override
  @JsonKey(ignore: true)
  _$$AuthStateImplCopyWith<_$AuthStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
