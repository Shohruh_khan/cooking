part of 'auth_bloc.dart';

@freezed
class AuthState with _$AuthState {
  const factory AuthState({
    @Default(AuthenticationStatus.unknown) AuthenticationStatus authStatus,
    @Default(null) UserModel? user,
    @Default(FormzStatus.pure) FormzStatus status,
    @Default('') String errorMessage,
  }) = _AuthState;
// const factory MainState({
//   @Default(FormzStatus.pure) FormzStatus status,
//   @Default([]) List<RecipeEntity> entities,
//   @Default('') String errorMessage,
// }) = _MainState;
}
