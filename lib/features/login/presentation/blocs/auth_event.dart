part of 'auth_bloc.dart';

@freezed
class AuthEvent with _$AuthEvent {
  const factory AuthEvent.signIn(
      {required String email,
      required String password,
      required ValueChanged<String> onFailure,
      required ValueChanged<String> onSuccess}) = _SignIn;

  const factory AuthEvent.signUp({
    required String firstName,
    required String surName,
    required ValueChanged<String> onFailure,
    required ValueChanged<String> onSuccess,
    required bool gender,
    required int age,
    required String email,
    required String image,
    required String password,
  }) = _SignUp;
  const factory AuthEvent.getUserInfo() = _GetUserInfo;

  const factory AuthEvent.chooseGender({required bool isMale}) = _ChooseGender;
  const factory AuthEvent.resetPassword(
      {required String email,
      required ValueChanged<String> onFailure,
      required ValueChanged<String> onSuccess}) = _ResetPassword;
  const factory AuthEvent.signOut(
      {required ValueChanged<String> onFailure,
      required ValueChanged<String> onSuccess}) = _SignOut;
  const factory AuthEvent.changedStatus(
      {required AuthenticationStatus status}) = _ChangedStatus;
}
