import 'package:cooking/core/models/formz/formz_type.dart';
import 'package:cooking/core/models/formz/rules.dart';
import 'package:cooking/core/widgets/w_scale.dart';
import 'package:cooking/features/login/presentation/blocs/auth_bloc.dart';
import 'package:cooking/features/login/presentation/pages/forgot_password_screen.dart';
import 'package:cooking/features/login/presentation/pages/register_screen.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../../../assets/colors/colors.dart';
import '../../../assets/icons/icons.dart';
import '../../../core/bloc/show_pop_up/show_pop_up_bloc.dart';
import '../../../core/bloc/translation/translation_bloc.dart';
import '../../../core/models/formz/formz_status.dart';
import '../../../core/widgets/popus/popups.dart';
import '../../../core/widgets/w_button.dart';
import '../../../core/widgets/w_text_field/bloc/validation_bloc.dart';
import '../../../core/widgets/w_text_field/w_text_field.dart';
import '../../home/presentation/home.dart';
import '../../home/presentation/widgets/navigator.dart';
import '../../profile/presentation/widgets/translate_bottomsheet.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final ValidationBloc validatorBlocEmail = ValidationBloc();
  final ValidationBloc validatorBlocPassword = ValidationBloc();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return Scaffold(
        appBar: AppBar(
          leading: WScaleAnimation(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: SvgPicture.asset(AppIcons.back),
            ),
          ),
          backgroundColor: primary,
          title: Text(
            'login'.tr(),
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(fontSize: 18, fontWeight: FontWeight.w600),
          ),
        ),
        body: ListView(
          children: [
            const SizedBox(height: 56),
            WTextField(
              validationBloc: validatorBlocEmail,
              textInputAction: TextInputAction.next,
              disabledColor: Colors.white38,
              title: 'email'.tr(),
              textStyle: Theme.of(context).textTheme.titleMedium,
              titleTextStyle: Theme.of(context).textTheme.titleMedium,
              margin: const EdgeInsets.symmetric(horizontal: 16),
              hintText: 'enter_email'.tr(),
              hintTextStyle: Theme.of(context).textTheme.titleMedium!.copyWith(
                  fontSize: 14,
                  color: black.withOpacity(0.8),
                  fontWeight: FontWeight.w400),
              controller: _emailController,
              onChanged: (value) {},
              validationType: FormzType.email,
            ),
            const SizedBox(height: 12),
            WTextField(
              validationBloc: validatorBlocPassword,
              textInputAction: TextInputAction.send,
              hasBorderColor: true,
              disabledColor: Colors.white38,
              title: 'password'.tr(),
              isObscureText: true,
              textStyle: Theme.of(context).textTheme.titleMedium,
              titleTextStyle: Theme.of(context).textTheme.titleMedium,
              margin: const EdgeInsets.symmetric(horizontal: 16),
              hintText: 'enter_password'.tr(),
              hintTextStyle: Theme.of(context).textTheme.titleMedium!.copyWith(
                  fontSize: 14,
                  color: black.withOpacity(0.8),
                  fontWeight: FontWeight.w400),
              controller: _passwordController,
              onChanged: (value) {},
              validationType: FormzType.password,
              rules: const PasswordRules(
                  minPasswordLength: 5,
                  maxPasswordLength: 25,
                  includeNumbers: true,
                  includeChars: false,
                  includeLetters: true),
              // onSubmitted: (value) => signIn(),
            ),
            WButton(
              color: primary,
              margin: const EdgeInsets.all(16),
              onTap: () {
                if (validatorBlocEmail.state.status ==
                        FormzStatus.submissionSuccess &&
                    validatorBlocPassword.state.status ==
                        FormzStatus.submissionSuccess) {
                  context.read<AuthBloc>().add(AuthEvent.signIn(
                      email: _emailController.text,
                      password: _passwordController.text,
                      onFailure: (value) {
                        debugPrint("UI:   $value");
                        context
                            .read<ShowPopUpBloc>()
                            .add(ShowPopUpEvent.showFailure(text: value));
                      },
                      onSuccess: (value) {
                        debugPrint("login value:  $value");
                        Navigator.of(context).push(fade(page: HomeScreen()));
                      }));
                } else {
                  context.read<ShowPopUpBloc>().add(
                      ShowPopUpEvent.showFailure(text: "something_wrong".tr()));
                }
              },
              text: 'login'.tr(),
              textStyle: Theme.of(context).textTheme.headlineMedium,
            ),
            Align(
              child: WScaleAnimation(
                onTap: () {
                  Navigator.of(context)
                      .push(fade(page: const RegisterScreen()));
                },
                child: Text(
                  'dont_have_account'.tr(),
                  style: Theme.of(context).textTheme.labelMedium,
                ),
              ),
            ),
            SizedBox(height: 16.h),
            Align(
              child: WScaleAnimation(
                onTap: () {
                  Navigator.of(context).push(
                    fade(
                      page: const ForgotPasswordScreen(),
                    ),
                  );
                },
                child: Text('forgot_password'.tr(),
                    style: Theme.of(context).textTheme.labelMedium),
              ),
            ),
            SizedBox(height: 16.h),
            Container(
              padding: EdgeInsets.symmetric(vertical: 24.h, horizontal: 20.w),
              decoration: BoxDecoration(
                color: white,
                borderRadius: BorderRadius.circular(14),
              ),
              child: WScaleAnimation(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'language'.tr(),
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                        BlocBuilder<TranslationBloc, TranslationState>(
                          builder: (context, state) {
                            return Text(
                              state.language,
                              style: Theme.of(context).textTheme.labelMedium,
                            );
                          },
                        ),
                      ],
                    ),
                    SvgPicture.asset(AppIcons.arrowRight),
                  ],
                ),
                onTap: () {
                  showCustomBottomSheet(
                      context,
                      (_) => BlocProvider.value(
                          value: context.read<TranslationBloc>(),
                          child: TranslateBottomsheet()));
                },
              ),
            ),
          ],
        ),
      );
    });
  }
}
