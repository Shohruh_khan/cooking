import 'package:firebase_auth/firebase_auth.dart';

import '../../../../core/data/either.dart';
import '../../../../core/error/failure.dart';
import '../../data/model/user_info.dart';

abstract class AuthRepository {
  Stream<User?> get status;

  Future<Either<ServerFailure, void>> signIn(
      {required String email, required String password});

  Future<Either<ServerFailure, void>> signUp(
      {required String email,
      required String password,
      required String firstName,
      required String lastName,
      required String image,
      required bool gender,
      required int age});

  Future<Either<ServerFailure, void>> resetPassword({required String email});
  Future<Either<Failure, void>> logOut();
  Future<Either<Failure, UserModel>> getUserInfo();
}
