import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

part 'user_info_entity.g.dart';

@HiveType(typeId: 2)
class UserInfoEntity extends Equatable {
  @HiveField(0)
  final String firstName;
  @HiveField(1)
  final String lastName;
  @HiveField(2)
  final String email;
  @HiveField(3)
  final String password;
  @HiveField(4)
  final String image;
  @HiveField(5)
  final int age;
  @HiveField(6)
  final String gender;

  const UserInfoEntity(
      {required this.firstName,
      required this.lastName,
      required this.email,
      required this.password,
      required this.image,
      required this.age,
      required this.gender});
  @override
  List<Object?> get props =>
      [firstName, lastName, email, image, password, gender, age];
}
