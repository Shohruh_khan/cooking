// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_info_entity.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserInfoEntityAdapter extends TypeAdapter<UserInfoEntity> {
  @override
  final int typeId = 2;

  @override
  UserInfoEntity read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return UserInfoEntity(
      firstName: fields[0] as String,
      lastName: fields[1] as String,
      email: fields[2] as String,
      password: fields[3] as String,
      image: fields[4] as String,
      age: fields[5] as int,
      gender: fields[6] as String,
    );
  }

  @override
  void write(BinaryWriter writer, UserInfoEntity obj) {
    writer
      ..writeByte(7)
      ..writeByte(0)
      ..write(obj.firstName)
      ..writeByte(1)
      ..write(obj.lastName)
      ..writeByte(2)
      ..write(obj.email)
      ..writeByte(3)
      ..write(obj.password)
      ..writeByte(4)
      ..write(obj.image)
      ..writeByte(5)
      ..write(obj.age)
      ..writeByte(6)
      ..write(obj.gender);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserInfoEntityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
