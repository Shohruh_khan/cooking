import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/login/domain/repository/repository.dart';

class SignInUseCase implements UseCase<void, SignInParams> {
  final AuthRepository _repository;
  SignInUseCase(this._repository);

  @override
  Future<Either<Failure, void>> call(SignInParams params) {
    return _repository.signIn(email: params.email, password: params.password);
  }
}
