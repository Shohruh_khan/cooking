import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/login/data/repository/auth_repository_impl.dart';
import 'package:cooking/features/login/domain/repository/repository.dart';

class SignOutUseCase implements UseCase<void, NoParams> {
  final AuthRepository _repository;
  SignOutUseCase(AuthRepository authRepository, {AuthRepository? repository})
      : _repository = repository ?? AuthRepositoryImpl();
  @override
  Future<Either<Failure, void>> call(NoParams params) {
    return _repository.logOut();
  }
}
