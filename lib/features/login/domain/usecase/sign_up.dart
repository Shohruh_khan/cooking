import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/login/domain/repository/repository.dart';

class SignUpUseCase implements UseCase<void, SignUpParams> {
  final AuthRepository _repository;
  SignUpUseCase(this._repository);

  @override
  Future<Either<Failure, void>> call(SignUpParams params) {
    return _repository.signUp(
        email: params.email,
        password: params.password,
        firstName: params.firstName,
        lastName: params.lastName,
        image: params.image,
        gender: params.gender,
        age: params.age);
  }
}
