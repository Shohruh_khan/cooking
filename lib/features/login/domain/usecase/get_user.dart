import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/login/domain/repository/repository.dart';

import '../../data/model/user_info.dart';

class GetUserUseCase implements UseCase<void, NoParams> {
  final AuthRepository _repository;
  GetUserUseCase(this._repository);

  @override
  Future<Either<Failure, UserModel>> call(NoParams params) {
    return _repository.getUserInfo();
  }
}
