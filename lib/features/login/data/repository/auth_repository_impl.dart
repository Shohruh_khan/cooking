import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cooking/core/data/boxes.dart';
import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/features/login/data/model/user_info.dart';
import 'package:cooking/features/login/domain/repository/repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';

import '../../../../core/data/network_info.dart';

class AuthRepositoryImpl extends AuthRepository {
  final NetworkInfo _networkInfo = const NetworkInfoImpl();
  var token = Hive.box(tokenKey);
  @override
  Stream<User?> get status => FirebaseAuth.instance.authStateChanges();

  @override
  Future<Either<Failure, void>> logOut() async {
    try {
      await FirebaseAuth.instance.signOut();
      // box.clear();
      token.delete(tokenKey);
      return Right(null);
    } on FirebaseException catch (e) {
      return Left(ServerFailure(errorMessage: e.message!));
    }
  }

  @override
  Future<Either<ServerFailure, void>> resetPassword(
      {required String email}) async {
    {
      if (await _networkInfo.connected) {
        await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
        return Right(null);
      } else {
        return Left(const ServerFailure(errorMessage: 'no internet'));
      }
    }
  }

  @override
  Future<Either<ServerFailure, void>> signIn(
      {required String email, required String password}) async {
    if (await _networkInfo.connected) {
      // final userCred =
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password)
          .then((value) {
        var uid = FirebaseAuth.instance.currentUser!.uid;
        token.put(tokenKey, uid);
        var name = token.get(tokenKey);
        debugPrint("signIn:  $name");
      });
      return Right(null);
    } else {
      return Left(const ServerFailure(errorMessage: 'no internet'));
    }
  }

  @override
  Future<Either<ServerFailure, void>> signUp(
      {required String email,
      required String password,
      required String firstName,
      required String lastName,
      required String image,
      required bool gender,
      required int age}) async {
    if (await _networkInfo.connected) {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((__) {
        var uid = FirebaseAuth.instance.currentUser!.uid;
        token.put(tokenKey, uid);
        var name = token.get(tokenKey);
        debugPrint("signUp:  $name");
        // GetStorage().write('token', uid);
      });
      await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .set({
        'firstName': firstName,
        'lastName': lastName,
        'age': age,
        'gender': gender,
        'image': image,
        'email': email,
        'password': password,
      });
      return Right(null);
    } else {
      return Left(const ServerFailure(errorMessage: 'no internet'));
    }
  }

  @override
  Future<Either<Failure, UserModel>> getUserInfo() async {
    if (await _networkInfo.connected) {
      // try {
      final data = await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser!.uid)
          .get();

      debugPrint("data from fires: ${data.data}");

      return Right(UserModel.fromMap(data.data() ?? <String, dynamic>{}));
    } else {
      return Left(const ServerFailure(errorMessage: "No Internet"));
    }
  }
}
