// import 'package:freezed_annotation/freezed_annotation.dart';
//
// part 'user_info.freezed.dart';
// part 'user_info.g.dart';
//
// @freezed
// class UserInfoModel with _$UserInfoModel {
//   const factory UserInfoModel({
//     required String firstName,
//     required String lastName,
//     required String email,
//     required String password,
//     required int age,
//     required String gender,
//     required String image,
//   }) = _UserInfoModel;
//   factory UserInfoModel.fromJson(Map<String, dynamic> json) =>
//       _$UserInfoModelFromJson(json);
// }

// ignore: must_be_immutable
import 'package:equatable/equatable.dart';

class UserModel extends Equatable {
  final String firstName;
  final String lastName;
  final String email;
  final String password;
  final int age;
  final bool gender;
  final String image;

  const UserModel(
      {required this.firstName,
      required this.lastName,
      required this.email,
      required this.password,
      required this.age,
      required this.gender,
      required this.image});

  UserModel copyWith({
    String? firstName,
    String? lastName,
    String? email,
    String? password,
    int? age,
    bool? gender,
    String? image,
  }) {
    return UserModel(
      firstName: firstName ?? this.firstName,
      lastName: lastName ?? this.lastName,
      email: email ?? this.email,
      password: password ?? this.password,
      age: age ?? this.age,
      gender: gender ?? this.gender,
      image: image ?? this.image,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'firstName': firstName,
      'lastName': lastName,
      'email': email,
      'password': password,
      'age': age,
      'gender': gender,
      'image': image,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      firstName: map['firstName'] ?? '',
      lastName: map['lastName'] ?? '',
      email: map['email'] ?? '',
      password: map['password'] ?? '',
      age: map['age'] ?? 25,
      gender: map['gender'],
      image: map['image'] ?? '',
    );
  }

  @override
  List<Object?> get props =>
      [firstName, lastName, email, password, age, gender, image];
}
