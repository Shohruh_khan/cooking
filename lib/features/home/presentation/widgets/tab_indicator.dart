import 'package:cooking/assets/colors/colors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../navbar/navbar.dart';

class TabItemWidget extends StatelessWidget {
  final bool isActive;
  final NavBar item;
  const TabItemWidget({
    this.isActive = false,
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        width: double.maxFinite,
        decoration: const BoxDecoration(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(
              isActive ? item.iconActive : item.icon,
              fit: BoxFit.contain,
            ),
            Text(item.title.tr(),
                style: isActive
                    ? Theme.of(context).textTheme.headlineSmall!.copyWith(
                          color: primary,
                          fontSize: 10,
                          fontWeight: FontWeight.w700,
                        )
                    : Theme.of(context).textTheme.headlineSmall!.copyWith(
                          color: hintText,
                          fontSize: 10,
                          fontWeight: FontWeight.w500,
                        )),
          ],
        ),
      );
}
