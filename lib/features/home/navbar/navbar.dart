class NavBar {
  final int id;
  final String icon;
  final String title;
  final String iconActive;

  const NavBar(
      {required this.id,
      required this.icon,
      required this.title,
      required this.iconActive});
}
