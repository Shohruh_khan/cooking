import 'package:cooking/core/widgets/w_scale.dart';
import 'package:cooking/core/widgets/w_text_field/w_text_field.dart';
import 'package:cooking/features/main/presentation/pages/recipe_instruction.dart';
import 'package:cooking/features/main/presentation/widgets/recipe_menu.dart';
import 'package:cooking/features/search/domain/entity/search_history.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';

import '../../../assets/animations/animations.dart';
import '../../../assets/colors/colors.dart';
import '../../../assets/icons/icons.dart';
import '../../../core/models/formz/formz_status.dart';
import 'bloc/search_bloc.dart';

class SearchPage extends StatefulWidget {
  SearchPage({super.key});

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController textEditingController = TextEditingController();
  @override
  void initState() {
    context.read<SearchBloc>().add(const SearchEvent.getSearchHistory());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return Scaffold(
        appBar: AppBar(
          elevation: 2,
          title: Text('search'.tr(),
              style: Theme.of(context).textTheme.headlineMedium),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(50),
            child: BlocBuilder<SearchBloc, SearchState>(
              builder: (context, state) {
                return WTextField(
                  textStyle: Theme.of(context).textTheme.titleMedium,
                  disabledColor: Colors.white38,
                  borderRadius: 12,
                  hintText: 'write_food_name'.tr(),
                  margin: const EdgeInsets.all(8.0),
                  hintTextStyle: Theme.of(context)
                      .textTheme
                      .titleMedium!
                      .copyWith(
                          fontSize: 14,
                          color: black.withOpacity(0.8),
                          fontWeight: FontWeight.w400),
                  controller: textEditingController,
                  onChanged: (value) {},
                  onSubmitted: (text) {
                    context.read<SearchBloc>().add(SearchEvent.putSearchHistory(
                        SearchHistoryEntity(
                            name: textEditingController.text,
                            id: DateFormat('dd.MM.yyyy HH:mm')
                                .format(DateTime.now()))));
                    context
                        .read<SearchBloc>()
                        .add(SearchEvent.getResults(text));
                  },
                  textInputAction: TextInputAction.search,
                  textCapitalization: TextCapitalization.sentences,
                  hasBorderColor: true,
                  suffixIcon: AppIcons.cancel,
                  onTapSuffix: () {
                    textEditingController.clear();
                    context.read<SearchBloc>().add(const SearchEvent.clear());
                  },
                );
              },
            ),
          ),
          backgroundColor: primary,
        ),
        body: BlocBuilder<SearchBloc, SearchState>(builder: (_, state) {
          if (state.status == FormzStatus.pure) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Wrap(
                spacing: 5,
                children: List.generate(
                  state.history.length,
                  (index) {
                    final searchHistory =
                        state.history[state.history.length - index - 1];

                    return GestureDetector(
                      onTap: () {
                        context.read<SearchBloc>().add(SearchEvent.getResults(
                              state.history[state.history.length - index - 1]
                                  .name,
                            ));
                        textEditingController.text = state
                            .history[state.history.length - index - 1].name;
                      },
                      child: Container(
                        padding: const EdgeInsets.all(14),
                        margin: const EdgeInsets.symmetric(vertical: 6),
                        decoration: BoxDecoration(
                          color: dividerColor,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              state.history[state.history.length - index - 1]
                                  .name,
                              style: Theme.of(context).textTheme.titleMedium,
                            ),
                            Text(
                                state.history[state.history.length - index - 1]
                                    .id,
                                style: Theme.of(context).textTheme.titleMedium),
                            WScaleAnimation(
                                child: SvgPicture.asset(AppIcons.trash),
                                onTap: () {
                                  context.read<SearchBloc>().add(
                                      SearchEvent.removeHistoryElement(
                                          searchHistory));
                                })
                          ],
                        ),
                      ),
                    );

                    Chip(
                      onDeleted: () {
                        context.read<SearchBloc>().add(
                            SearchEvent.removeHistoryElement(searchHistory));
                      },
                      elevation: 2,
                      deleteIcon: const Icon(Icons.remove_circle_outline),
                      backgroundColor: primary,
                      label: Text(
                        state.history[state.history.length - index - 1].name,
                        style: const TextStyle(fontSize: 14),
                      ),
                    );
                  },
                ),
              ),
            );
          } else if (state.status == FormzStatus.submissionInProgress) {
            return Center(
              child: Lottie.asset(AppAnimations.search, height: 150),
            );
          } else if (state.status == FormzStatus.submissionSuccess) {
            if (state.results.isNotEmpty) {
              return ListView.separated(
                padding: const EdgeInsets.all(16),
                itemBuilder: (context, index) => GestureDetector(
                    onTap: () {
                      Navigator.of(
                        context,
                        rootNavigator: true,
                      ).push(MaterialPageRoute(
                        builder: (
                          context,
                        ) =>
                            RecipeInstruction(
                          data: state.results[index],
                        ),
                      ));
                    },
                    child: RecipesMenu(data: state.results[index])),
                separatorBuilder: (_, __) => const SizedBox(
                  height: 10,
                ),
                itemCount: state.results.length,
              );
            } else {
              return Center(
                child: Text(
                  'nothing_found'.tr(),
                ),
              );
            }
          } else {
            return Text(
              state.errorMessage,
              style: Theme.of(context).textTheme.titleMedium,
            );
          }
        }),
      );
    });
  }
}
