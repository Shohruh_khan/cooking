import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/search/data/data_source/local_data_source.dart';
import 'package:cooking/features/search/domain/repository/search_repo.dart';
import 'package:cooking/features/search/domain/usecase/get_history.dart';
import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive/hive.dart';

import '../../../../core/data/boxes.dart';
import '../../../../core/models/formz/formz_status.dart';
import '../../../main/domain/entity/recipe.dart';
import '../../domain/entity/search_history.dart';
import '../../domain/usecase/get_results.dart';

part 'search_bloc.freezed.dart';
part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  late StreamSubscription streamSearchHistory;
  final SearchLocalDataSource localDataSource;
  final SearchRepository repository;
  Box searchBox = Hive.box<List<SearchHistoryEntity>>(searchHistoryKey);
  SearchBloc(this.localDataSource, this.repository)
      : super(const _SearchState()) {
    // streamSearchHistory = searchBox.getSearchHistoryStream().listen((history) {
    //   add(SearchEvent.changeHistory(history));
    // });

    on<_ChangeHistory>(
        (event, emit) => emit(state.copyWith(history: event.history)));

    on<_Clear>((event, emit) => emit(
          state.copyWith(status: FormzStatus.pure),
        ));

    on<_GetResults>((event, emit) async {
      emit(state.copyWith(status: FormzStatus.submissionInProgress));
      final usecase = GetResultsUseCase();
      final result = await usecase.call(event.query);
      result.either((fail) {
        emit(state.copyWith(status: FormzStatus.submissionFailure));
        debugPrint("Inside bloc failure:   ${fail.toString()}");
      }, (data) {
        // emit(state.copyWith(history: ))
        // localDataSource
        //     .putSearchHistoryElement(SearchHistoryElement(name: data[0].title));
        // objectbox
        //     .putSearchHistoryElement(SearchHistoryElement(name: data[0].title));
        emit(state.copyWith(
            status: FormzStatus.submissionSuccess, results: data));
        debugPrint("Successfully find");
      });
    });

    on<_GetSearchHistory>((event, emit) async {
      // emit(state.copyWith(status: FormzStatus.submissionInProgress));
      final usecase = GetSearchHistoryUseCase();
      final result = await usecase.call(NoParams());
      result.either((fail) {}, (history) {
        emit(state.copyWith(history: history));
      });
    });

    on<_PutSearchHistory>((event, emit) async {
      // final put = repository.putSearchHistory(event.query);
      // final results = await repository.putSearchHistory(event.query);
      // final usecase = PutSearchHistoryUseCase();
      //  final result = await usecase.call(event.historyEntity);
      // result.either((fail) {
      //   debugPrint("put error inside bloc:  $fail");
      // }, (data) {
      //   debugPrint("inside blocc put: success");
      //   // emit(state.copyWith(history: data));
      // });

      final newElement = state.history + [event.historyEntity];
      final result = await repository.putSearchHistory(newElement);
      result.either((value) {
        debugPrint("Put event bloc added fail");
      }, (_) {
        debugPrint("put event bloc added success");
      });
      emit(state.copyWith(history: newElement));
    });

    on<_RemoveHistoryElement>((event, emit) async {
      List<SearchHistoryEntity> newEntities = [];
      for (int i = 0; i < state.history.length; i++) {
        if (state.history[i] != event.historyEntity) {
          newEntities.add(state.history[i]);
        }
      }
      final result =
          await repository.removeSearchHistory(event.historyEntity.id);
      result.either((value) {}, (_) {
        print("successfully removed inside bloc");
      });
      emit(state.copyWith(history: newEntities));
    });
  }
}
