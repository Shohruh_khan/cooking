// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'search_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SearchEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) getResults,
    required TResult Function() clear,
    required TResult Function() getSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        putSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        removeHistoryElement,
    required TResult Function(List<SearchHistoryEntity> history) changeHistory,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? getResults,
    TResult? Function()? clear,
    TResult? Function()? getSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult? Function(List<SearchHistoryEntity> history)? changeHistory,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? getResults,
    TResult Function()? clear,
    TResult Function()? getSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult Function(List<SearchHistoryEntity> history)? changeHistory,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetResults value) getResults,
    required TResult Function(_Clear value) clear,
    required TResult Function(_GetSearchHistory value) getSearchHistory,
    required TResult Function(_PutSearchHistory value) putSearchHistory,
    required TResult Function(_RemoveHistoryElement value) removeHistoryElement,
    required TResult Function(_ChangeHistory value) changeHistory,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetResults value)? getResults,
    TResult? Function(_Clear value)? clear,
    TResult? Function(_GetSearchHistory value)? getSearchHistory,
    TResult? Function(_PutSearchHistory value)? putSearchHistory,
    TResult? Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult? Function(_ChangeHistory value)? changeHistory,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetResults value)? getResults,
    TResult Function(_Clear value)? clear,
    TResult Function(_GetSearchHistory value)? getSearchHistory,
    TResult Function(_PutSearchHistory value)? putSearchHistory,
    TResult Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult Function(_ChangeHistory value)? changeHistory,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SearchEventCopyWith<$Res> {
  factory $SearchEventCopyWith(
          SearchEvent value, $Res Function(SearchEvent) then) =
      _$SearchEventCopyWithImpl<$Res, SearchEvent>;
}

/// @nodoc
class _$SearchEventCopyWithImpl<$Res, $Val extends SearchEvent>
    implements $SearchEventCopyWith<$Res> {
  _$SearchEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GetResultsImplCopyWith<$Res> {
  factory _$$GetResultsImplCopyWith(
          _$GetResultsImpl value, $Res Function(_$GetResultsImpl) then) =
      __$$GetResultsImplCopyWithImpl<$Res>;
  @useResult
  $Res call({String query});
}

/// @nodoc
class __$$GetResultsImplCopyWithImpl<$Res>
    extends _$SearchEventCopyWithImpl<$Res, _$GetResultsImpl>
    implements _$$GetResultsImplCopyWith<$Res> {
  __$$GetResultsImplCopyWithImpl(
      _$GetResultsImpl _value, $Res Function(_$GetResultsImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? query = null,
  }) {
    return _then(_$GetResultsImpl(
      null == query
          ? _value.query
          : query // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$GetResultsImpl implements _GetResults {
  const _$GetResultsImpl(this.query);

  @override
  final String query;

  @override
  String toString() {
    return 'SearchEvent.getResults(query: $query)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetResultsImpl &&
            (identical(other.query, query) || other.query == query));
  }

  @override
  int get hashCode => Object.hash(runtimeType, query);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GetResultsImplCopyWith<_$GetResultsImpl> get copyWith =>
      __$$GetResultsImplCopyWithImpl<_$GetResultsImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) getResults,
    required TResult Function() clear,
    required TResult Function() getSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        putSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        removeHistoryElement,
    required TResult Function(List<SearchHistoryEntity> history) changeHistory,
  }) {
    return getResults(query);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? getResults,
    TResult? Function()? clear,
    TResult? Function()? getSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult? Function(List<SearchHistoryEntity> history)? changeHistory,
  }) {
    return getResults?.call(query);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? getResults,
    TResult Function()? clear,
    TResult Function()? getSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult Function(List<SearchHistoryEntity> history)? changeHistory,
    required TResult orElse(),
  }) {
    if (getResults != null) {
      return getResults(query);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetResults value) getResults,
    required TResult Function(_Clear value) clear,
    required TResult Function(_GetSearchHistory value) getSearchHistory,
    required TResult Function(_PutSearchHistory value) putSearchHistory,
    required TResult Function(_RemoveHistoryElement value) removeHistoryElement,
    required TResult Function(_ChangeHistory value) changeHistory,
  }) {
    return getResults(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetResults value)? getResults,
    TResult? Function(_Clear value)? clear,
    TResult? Function(_GetSearchHistory value)? getSearchHistory,
    TResult? Function(_PutSearchHistory value)? putSearchHistory,
    TResult? Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult? Function(_ChangeHistory value)? changeHistory,
  }) {
    return getResults?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetResults value)? getResults,
    TResult Function(_Clear value)? clear,
    TResult Function(_GetSearchHistory value)? getSearchHistory,
    TResult Function(_PutSearchHistory value)? putSearchHistory,
    TResult Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult Function(_ChangeHistory value)? changeHistory,
    required TResult orElse(),
  }) {
    if (getResults != null) {
      return getResults(this);
    }
    return orElse();
  }
}

abstract class _GetResults implements SearchEvent {
  const factory _GetResults(final String query) = _$GetResultsImpl;

  String get query;
  @JsonKey(ignore: true)
  _$$GetResultsImplCopyWith<_$GetResultsImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ClearImplCopyWith<$Res> {
  factory _$$ClearImplCopyWith(
          _$ClearImpl value, $Res Function(_$ClearImpl) then) =
      __$$ClearImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ClearImplCopyWithImpl<$Res>
    extends _$SearchEventCopyWithImpl<$Res, _$ClearImpl>
    implements _$$ClearImplCopyWith<$Res> {
  __$$ClearImplCopyWithImpl(
      _$ClearImpl _value, $Res Function(_$ClearImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$ClearImpl implements _Clear {
  const _$ClearImpl();

  @override
  String toString() {
    return 'SearchEvent.clear()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$ClearImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) getResults,
    required TResult Function() clear,
    required TResult Function() getSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        putSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        removeHistoryElement,
    required TResult Function(List<SearchHistoryEntity> history) changeHistory,
  }) {
    return clear();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? getResults,
    TResult? Function()? clear,
    TResult? Function()? getSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult? Function(List<SearchHistoryEntity> history)? changeHistory,
  }) {
    return clear?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? getResults,
    TResult Function()? clear,
    TResult Function()? getSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult Function(List<SearchHistoryEntity> history)? changeHistory,
    required TResult orElse(),
  }) {
    if (clear != null) {
      return clear();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetResults value) getResults,
    required TResult Function(_Clear value) clear,
    required TResult Function(_GetSearchHistory value) getSearchHistory,
    required TResult Function(_PutSearchHistory value) putSearchHistory,
    required TResult Function(_RemoveHistoryElement value) removeHistoryElement,
    required TResult Function(_ChangeHistory value) changeHistory,
  }) {
    return clear(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetResults value)? getResults,
    TResult? Function(_Clear value)? clear,
    TResult? Function(_GetSearchHistory value)? getSearchHistory,
    TResult? Function(_PutSearchHistory value)? putSearchHistory,
    TResult? Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult? Function(_ChangeHistory value)? changeHistory,
  }) {
    return clear?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetResults value)? getResults,
    TResult Function(_Clear value)? clear,
    TResult Function(_GetSearchHistory value)? getSearchHistory,
    TResult Function(_PutSearchHistory value)? putSearchHistory,
    TResult Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult Function(_ChangeHistory value)? changeHistory,
    required TResult orElse(),
  }) {
    if (clear != null) {
      return clear(this);
    }
    return orElse();
  }
}

abstract class _Clear implements SearchEvent {
  const factory _Clear() = _$ClearImpl;
}

/// @nodoc
abstract class _$$GetSearchHistoryImplCopyWith<$Res> {
  factory _$$GetSearchHistoryImplCopyWith(_$GetSearchHistoryImpl value,
          $Res Function(_$GetSearchHistoryImpl) then) =
      __$$GetSearchHistoryImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetSearchHistoryImplCopyWithImpl<$Res>
    extends _$SearchEventCopyWithImpl<$Res, _$GetSearchHistoryImpl>
    implements _$$GetSearchHistoryImplCopyWith<$Res> {
  __$$GetSearchHistoryImplCopyWithImpl(_$GetSearchHistoryImpl _value,
      $Res Function(_$GetSearchHistoryImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetSearchHistoryImpl implements _GetSearchHistory {
  const _$GetSearchHistoryImpl();

  @override
  String toString() {
    return 'SearchEvent.getSearchHistory()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetSearchHistoryImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) getResults,
    required TResult Function() clear,
    required TResult Function() getSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        putSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        removeHistoryElement,
    required TResult Function(List<SearchHistoryEntity> history) changeHistory,
  }) {
    return getSearchHistory();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? getResults,
    TResult? Function()? clear,
    TResult? Function()? getSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult? Function(List<SearchHistoryEntity> history)? changeHistory,
  }) {
    return getSearchHistory?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? getResults,
    TResult Function()? clear,
    TResult Function()? getSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult Function(List<SearchHistoryEntity> history)? changeHistory,
    required TResult orElse(),
  }) {
    if (getSearchHistory != null) {
      return getSearchHistory();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetResults value) getResults,
    required TResult Function(_Clear value) clear,
    required TResult Function(_GetSearchHistory value) getSearchHistory,
    required TResult Function(_PutSearchHistory value) putSearchHistory,
    required TResult Function(_RemoveHistoryElement value) removeHistoryElement,
    required TResult Function(_ChangeHistory value) changeHistory,
  }) {
    return getSearchHistory(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetResults value)? getResults,
    TResult? Function(_Clear value)? clear,
    TResult? Function(_GetSearchHistory value)? getSearchHistory,
    TResult? Function(_PutSearchHistory value)? putSearchHistory,
    TResult? Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult? Function(_ChangeHistory value)? changeHistory,
  }) {
    return getSearchHistory?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetResults value)? getResults,
    TResult Function(_Clear value)? clear,
    TResult Function(_GetSearchHistory value)? getSearchHistory,
    TResult Function(_PutSearchHistory value)? putSearchHistory,
    TResult Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult Function(_ChangeHistory value)? changeHistory,
    required TResult orElse(),
  }) {
    if (getSearchHistory != null) {
      return getSearchHistory(this);
    }
    return orElse();
  }
}

abstract class _GetSearchHistory implements SearchEvent {
  const factory _GetSearchHistory() = _$GetSearchHistoryImpl;
}

/// @nodoc
abstract class _$$PutSearchHistoryImplCopyWith<$Res> {
  factory _$$PutSearchHistoryImplCopyWith(_$PutSearchHistoryImpl value,
          $Res Function(_$PutSearchHistoryImpl) then) =
      __$$PutSearchHistoryImplCopyWithImpl<$Res>;
  @useResult
  $Res call({SearchHistoryEntity historyEntity});
}

/// @nodoc
class __$$PutSearchHistoryImplCopyWithImpl<$Res>
    extends _$SearchEventCopyWithImpl<$Res, _$PutSearchHistoryImpl>
    implements _$$PutSearchHistoryImplCopyWith<$Res> {
  __$$PutSearchHistoryImplCopyWithImpl(_$PutSearchHistoryImpl _value,
      $Res Function(_$PutSearchHistoryImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? historyEntity = null,
  }) {
    return _then(_$PutSearchHistoryImpl(
      null == historyEntity
          ? _value.historyEntity
          : historyEntity // ignore: cast_nullable_to_non_nullable
              as SearchHistoryEntity,
    ));
  }
}

/// @nodoc

class _$PutSearchHistoryImpl implements _PutSearchHistory {
  const _$PutSearchHistoryImpl(this.historyEntity);

  @override
  final SearchHistoryEntity historyEntity;

  @override
  String toString() {
    return 'SearchEvent.putSearchHistory(historyEntity: $historyEntity)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PutSearchHistoryImpl &&
            (identical(other.historyEntity, historyEntity) ||
                other.historyEntity == historyEntity));
  }

  @override
  int get hashCode => Object.hash(runtimeType, historyEntity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$PutSearchHistoryImplCopyWith<_$PutSearchHistoryImpl> get copyWith =>
      __$$PutSearchHistoryImplCopyWithImpl<_$PutSearchHistoryImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) getResults,
    required TResult Function() clear,
    required TResult Function() getSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        putSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        removeHistoryElement,
    required TResult Function(List<SearchHistoryEntity> history) changeHistory,
  }) {
    return putSearchHistory(historyEntity);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? getResults,
    TResult? Function()? clear,
    TResult? Function()? getSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult? Function(List<SearchHistoryEntity> history)? changeHistory,
  }) {
    return putSearchHistory?.call(historyEntity);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? getResults,
    TResult Function()? clear,
    TResult Function()? getSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult Function(List<SearchHistoryEntity> history)? changeHistory,
    required TResult orElse(),
  }) {
    if (putSearchHistory != null) {
      return putSearchHistory(historyEntity);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetResults value) getResults,
    required TResult Function(_Clear value) clear,
    required TResult Function(_GetSearchHistory value) getSearchHistory,
    required TResult Function(_PutSearchHistory value) putSearchHistory,
    required TResult Function(_RemoveHistoryElement value) removeHistoryElement,
    required TResult Function(_ChangeHistory value) changeHistory,
  }) {
    return putSearchHistory(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetResults value)? getResults,
    TResult? Function(_Clear value)? clear,
    TResult? Function(_GetSearchHistory value)? getSearchHistory,
    TResult? Function(_PutSearchHistory value)? putSearchHistory,
    TResult? Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult? Function(_ChangeHistory value)? changeHistory,
  }) {
    return putSearchHistory?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetResults value)? getResults,
    TResult Function(_Clear value)? clear,
    TResult Function(_GetSearchHistory value)? getSearchHistory,
    TResult Function(_PutSearchHistory value)? putSearchHistory,
    TResult Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult Function(_ChangeHistory value)? changeHistory,
    required TResult orElse(),
  }) {
    if (putSearchHistory != null) {
      return putSearchHistory(this);
    }
    return orElse();
  }
}

abstract class _PutSearchHistory implements SearchEvent {
  const factory _PutSearchHistory(final SearchHistoryEntity historyEntity) =
      _$PutSearchHistoryImpl;

  SearchHistoryEntity get historyEntity;
  @JsonKey(ignore: true)
  _$$PutSearchHistoryImplCopyWith<_$PutSearchHistoryImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RemoveHistoryElementImplCopyWith<$Res> {
  factory _$$RemoveHistoryElementImplCopyWith(_$RemoveHistoryElementImpl value,
          $Res Function(_$RemoveHistoryElementImpl) then) =
      __$$RemoveHistoryElementImplCopyWithImpl<$Res>;
  @useResult
  $Res call({SearchHistoryEntity historyEntity});
}

/// @nodoc
class __$$RemoveHistoryElementImplCopyWithImpl<$Res>
    extends _$SearchEventCopyWithImpl<$Res, _$RemoveHistoryElementImpl>
    implements _$$RemoveHistoryElementImplCopyWith<$Res> {
  __$$RemoveHistoryElementImplCopyWithImpl(_$RemoveHistoryElementImpl _value,
      $Res Function(_$RemoveHistoryElementImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? historyEntity = null,
  }) {
    return _then(_$RemoveHistoryElementImpl(
      null == historyEntity
          ? _value.historyEntity
          : historyEntity // ignore: cast_nullable_to_non_nullable
              as SearchHistoryEntity,
    ));
  }
}

/// @nodoc

class _$RemoveHistoryElementImpl implements _RemoveHistoryElement {
  const _$RemoveHistoryElementImpl(this.historyEntity);

  @override
  final SearchHistoryEntity historyEntity;

  @override
  String toString() {
    return 'SearchEvent.removeHistoryElement(historyEntity: $historyEntity)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RemoveHistoryElementImpl &&
            (identical(other.historyEntity, historyEntity) ||
                other.historyEntity == historyEntity));
  }

  @override
  int get hashCode => Object.hash(runtimeType, historyEntity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RemoveHistoryElementImplCopyWith<_$RemoveHistoryElementImpl>
      get copyWith =>
          __$$RemoveHistoryElementImplCopyWithImpl<_$RemoveHistoryElementImpl>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) getResults,
    required TResult Function() clear,
    required TResult Function() getSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        putSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        removeHistoryElement,
    required TResult Function(List<SearchHistoryEntity> history) changeHistory,
  }) {
    return removeHistoryElement(historyEntity);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? getResults,
    TResult? Function()? clear,
    TResult? Function()? getSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult? Function(List<SearchHistoryEntity> history)? changeHistory,
  }) {
    return removeHistoryElement?.call(historyEntity);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? getResults,
    TResult Function()? clear,
    TResult Function()? getSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult Function(List<SearchHistoryEntity> history)? changeHistory,
    required TResult orElse(),
  }) {
    if (removeHistoryElement != null) {
      return removeHistoryElement(historyEntity);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetResults value) getResults,
    required TResult Function(_Clear value) clear,
    required TResult Function(_GetSearchHistory value) getSearchHistory,
    required TResult Function(_PutSearchHistory value) putSearchHistory,
    required TResult Function(_RemoveHistoryElement value) removeHistoryElement,
    required TResult Function(_ChangeHistory value) changeHistory,
  }) {
    return removeHistoryElement(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetResults value)? getResults,
    TResult? Function(_Clear value)? clear,
    TResult? Function(_GetSearchHistory value)? getSearchHistory,
    TResult? Function(_PutSearchHistory value)? putSearchHistory,
    TResult? Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult? Function(_ChangeHistory value)? changeHistory,
  }) {
    return removeHistoryElement?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetResults value)? getResults,
    TResult Function(_Clear value)? clear,
    TResult Function(_GetSearchHistory value)? getSearchHistory,
    TResult Function(_PutSearchHistory value)? putSearchHistory,
    TResult Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult Function(_ChangeHistory value)? changeHistory,
    required TResult orElse(),
  }) {
    if (removeHistoryElement != null) {
      return removeHistoryElement(this);
    }
    return orElse();
  }
}

abstract class _RemoveHistoryElement implements SearchEvent {
  const factory _RemoveHistoryElement(final SearchHistoryEntity historyEntity) =
      _$RemoveHistoryElementImpl;

  SearchHistoryEntity get historyEntity;
  @JsonKey(ignore: true)
  _$$RemoveHistoryElementImplCopyWith<_$RemoveHistoryElementImpl>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ChangeHistoryImplCopyWith<$Res> {
  factory _$$ChangeHistoryImplCopyWith(
          _$ChangeHistoryImpl value, $Res Function(_$ChangeHistoryImpl) then) =
      __$$ChangeHistoryImplCopyWithImpl<$Res>;
  @useResult
  $Res call({List<SearchHistoryEntity> history});
}

/// @nodoc
class __$$ChangeHistoryImplCopyWithImpl<$Res>
    extends _$SearchEventCopyWithImpl<$Res, _$ChangeHistoryImpl>
    implements _$$ChangeHistoryImplCopyWith<$Res> {
  __$$ChangeHistoryImplCopyWithImpl(
      _$ChangeHistoryImpl _value, $Res Function(_$ChangeHistoryImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? history = null,
  }) {
    return _then(_$ChangeHistoryImpl(
      null == history
          ? _value._history
          : history // ignore: cast_nullable_to_non_nullable
              as List<SearchHistoryEntity>,
    ));
  }
}

/// @nodoc

class _$ChangeHistoryImpl implements _ChangeHistory {
  const _$ChangeHistoryImpl(final List<SearchHistoryEntity> history)
      : _history = history;

  final List<SearchHistoryEntity> _history;
  @override
  List<SearchHistoryEntity> get history {
    if (_history is EqualUnmodifiableListView) return _history;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_history);
  }

  @override
  String toString() {
    return 'SearchEvent.changeHistory(history: $history)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChangeHistoryImpl &&
            const DeepCollectionEquality().equals(other._history, _history));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_history));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChangeHistoryImplCopyWith<_$ChangeHistoryImpl> get copyWith =>
      __$$ChangeHistoryImplCopyWithImpl<_$ChangeHistoryImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String query) getResults,
    required TResult Function() clear,
    required TResult Function() getSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        putSearchHistory,
    required TResult Function(SearchHistoryEntity historyEntity)
        removeHistoryElement,
    required TResult Function(List<SearchHistoryEntity> history) changeHistory,
  }) {
    return changeHistory(history);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String query)? getResults,
    TResult? Function()? clear,
    TResult? Function()? getSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult? Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult? Function(List<SearchHistoryEntity> history)? changeHistory,
  }) {
    return changeHistory?.call(history);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String query)? getResults,
    TResult Function()? clear,
    TResult Function()? getSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? putSearchHistory,
    TResult Function(SearchHistoryEntity historyEntity)? removeHistoryElement,
    TResult Function(List<SearchHistoryEntity> history)? changeHistory,
    required TResult orElse(),
  }) {
    if (changeHistory != null) {
      return changeHistory(history);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetResults value) getResults,
    required TResult Function(_Clear value) clear,
    required TResult Function(_GetSearchHistory value) getSearchHistory,
    required TResult Function(_PutSearchHistory value) putSearchHistory,
    required TResult Function(_RemoveHistoryElement value) removeHistoryElement,
    required TResult Function(_ChangeHistory value) changeHistory,
  }) {
    return changeHistory(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetResults value)? getResults,
    TResult? Function(_Clear value)? clear,
    TResult? Function(_GetSearchHistory value)? getSearchHistory,
    TResult? Function(_PutSearchHistory value)? putSearchHistory,
    TResult? Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult? Function(_ChangeHistory value)? changeHistory,
  }) {
    return changeHistory?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetResults value)? getResults,
    TResult Function(_Clear value)? clear,
    TResult Function(_GetSearchHistory value)? getSearchHistory,
    TResult Function(_PutSearchHistory value)? putSearchHistory,
    TResult Function(_RemoveHistoryElement value)? removeHistoryElement,
    TResult Function(_ChangeHistory value)? changeHistory,
    required TResult orElse(),
  }) {
    if (changeHistory != null) {
      return changeHistory(this);
    }
    return orElse();
  }
}

abstract class _ChangeHistory implements SearchEvent {
  const factory _ChangeHistory(final List<SearchHistoryEntity> history) =
      _$ChangeHistoryImpl;

  List<SearchHistoryEntity> get history;
  @JsonKey(ignore: true)
  _$$ChangeHistoryImplCopyWith<_$ChangeHistoryImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SearchState {
  FormzStatus get status =>
      throw _privateConstructorUsedError; // @Default([]) List<SuggestionEntity> suggestions,
  List<RecipeEntity> get results => throw _privateConstructorUsedError;
  List<SearchHistoryEntity> get history => throw _privateConstructorUsedError;
  String get errorMessage => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SearchStateCopyWith<SearchState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SearchStateCopyWith<$Res> {
  factory $SearchStateCopyWith(
          SearchState value, $Res Function(SearchState) then) =
      _$SearchStateCopyWithImpl<$Res, SearchState>;
  @useResult
  $Res call(
      {FormzStatus status,
      List<RecipeEntity> results,
      List<SearchHistoryEntity> history,
      String errorMessage});
}

/// @nodoc
class _$SearchStateCopyWithImpl<$Res, $Val extends SearchState>
    implements $SearchStateCopyWith<$Res> {
  _$SearchStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? results = null,
    Object? history = null,
    Object? errorMessage = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<RecipeEntity>,
      history: null == history
          ? _value.history
          : history // ignore: cast_nullable_to_non_nullable
              as List<SearchHistoryEntity>,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SearchStateImplCopyWith<$Res>
    implements $SearchStateCopyWith<$Res> {
  factory _$$SearchStateImplCopyWith(
          _$SearchStateImpl value, $Res Function(_$SearchStateImpl) then) =
      __$$SearchStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      List<RecipeEntity> results,
      List<SearchHistoryEntity> history,
      String errorMessage});
}

/// @nodoc
class __$$SearchStateImplCopyWithImpl<$Res>
    extends _$SearchStateCopyWithImpl<$Res, _$SearchStateImpl>
    implements _$$SearchStateImplCopyWith<$Res> {
  __$$SearchStateImplCopyWithImpl(
      _$SearchStateImpl _value, $Res Function(_$SearchStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? results = null,
    Object? history = null,
    Object? errorMessage = null,
  }) {
    return _then(_$SearchStateImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<RecipeEntity>,
      history: null == history
          ? _value._history
          : history // ignore: cast_nullable_to_non_nullable
              as List<SearchHistoryEntity>,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SearchStateImpl implements _SearchState {
  const _$SearchStateImpl(
      {this.status = FormzStatus.pure,
      final List<RecipeEntity> results = const [],
      final List<SearchHistoryEntity> history = const [],
      this.errorMessage = ''})
      : _results = results,
        _history = history;

  @override
  @JsonKey()
  final FormzStatus status;
// @Default([]) List<SuggestionEntity> suggestions,
  final List<RecipeEntity> _results;
// @Default([]) List<SuggestionEntity> suggestions,
  @override
  @JsonKey()
  List<RecipeEntity> get results {
    if (_results is EqualUnmodifiableListView) return _results;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  final List<SearchHistoryEntity> _history;
  @override
  @JsonKey()
  List<SearchHistoryEntity> get history {
    if (_history is EqualUnmodifiableListView) return _history;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_history);
  }

  @override
  @JsonKey()
  final String errorMessage;

  @override
  String toString() {
    return 'SearchState(status: $status, results: $results, history: $history, errorMessage: $errorMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SearchStateImpl &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality().equals(other._results, _results) &&
            const DeepCollectionEquality().equals(other._history, _history) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      status,
      const DeepCollectionEquality().hash(_results),
      const DeepCollectionEquality().hash(_history),
      errorMessage);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SearchStateImplCopyWith<_$SearchStateImpl> get copyWith =>
      __$$SearchStateImplCopyWithImpl<_$SearchStateImpl>(this, _$identity);
}

abstract class _SearchState implements SearchState {
  const factory _SearchState(
      {final FormzStatus status,
      final List<RecipeEntity> results,
      final List<SearchHistoryEntity> history,
      final String errorMessage}) = _$SearchStateImpl;

  @override
  FormzStatus get status;
  @override // @Default([]) List<SuggestionEntity> suggestions,
  List<RecipeEntity> get results;
  @override
  List<SearchHistoryEntity> get history;
  @override
  String get errorMessage;
  @override
  @JsonKey(ignore: true)
  _$$SearchStateImplCopyWith<_$SearchStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
