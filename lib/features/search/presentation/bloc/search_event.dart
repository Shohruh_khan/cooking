part of 'search_bloc.dart';

@freezed
class SearchEvent with _$SearchEvent {
  // const factory SearchEvent.getSuggestions(String query) = _GetSuggestions;
  const factory SearchEvent.getResults(String query) = _GetResults;
  // const factory SearchEvent.getSuggestionResult(int id) = _GetSuggestionResult;
  const factory SearchEvent.clear() = _Clear;
  const factory SearchEvent.getSearchHistory() = _GetSearchHistory;
  const factory SearchEvent.putSearchHistory(
      SearchHistoryEntity historyEntity) = _PutSearchHistory;
  const factory SearchEvent.removeHistoryElement(
      SearchHistoryEntity historyEntity) = _RemoveHistoryElement;
  const factory SearchEvent.changeHistory(List<SearchHistoryEntity> history) =
      _ChangeHistory;
}
