// import 'package:remote_recipe/features/home/domain/entity/recipe.dart';
// import 'package:remote_recipe/features/search/domain/entity/suggestion.dart';

import 'package:cooking/features/search/domain/entity/search_history.dart';

import '../../../../core/data/either.dart';
import '../../../../core/error/failure.dart';
import '../../../main/domain/entity/recipe.dart';

abstract class SearchRepository {
  Future<Either<Failure, List<RecipeEntity>>> getResults(String query);
  Either<CacheFailure, List<SearchHistoryEntity>> getSearchHistory();
  Future<Either<CacheFailure, void>> putSearchHistory(
      List<SearchHistoryEntity> entity);
  Future<Either<CacheFailure, void>> removeSearchHistory(String id);
}
