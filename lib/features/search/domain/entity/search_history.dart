// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

part 'search_history.g.dart';

@HiveType(typeId: 1)
class SearchHistoryEntity extends Equatable {
  @HiveField(0)
  String id;
  @HiveField(1)
  late final String name;
  SearchHistoryEntity({
    this.id = '00',
    required this.name,
  });
  @override
  List<Object?> get props => [id, name];
}
