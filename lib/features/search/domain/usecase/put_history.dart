import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/features/search/domain/entity/search_history.dart';

import '../../../../core/usecase/usecase.dart';
import '../../data/repository/search_repo_impl.dart';
import '../repository/search_repo.dart';

class PutSearchHistoryUseCase
    implements UseCase<void, List<SearchHistoryEntity>> {
  final SearchRepository _repository = SearchRepositoryImpl();

  @override
  Future<Either<Failure, void>> call(List<SearchHistoryEntity> params) async {
    return _repository.putSearchHistory(params);
  }
}
