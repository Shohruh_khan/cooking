import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/search/data/repository/search_repo_impl.dart';
import 'package:cooking/features/search/domain/repository/search_repo.dart';

class RemoveSearchHistoryUseCase implements UseCase<void, String> {
  final SearchRepository _repository = SearchRepositoryImpl();

  @override
  Future<Either<Failure, void>> call(String id) {
    return _repository.removeSearchHistory(id);
  }
}
