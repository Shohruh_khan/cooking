import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/search/data/repository/search_repo_impl.dart';
import 'package:cooking/features/search/domain/entity/search_history.dart';
import 'package:cooking/features/search/domain/repository/search_repo.dart';

class GetSearchHistoryUseCase
    implements UseCase<List<SearchHistoryEntity>, NoParams> {
  final SearchRepository _repository = SearchRepositoryImpl();
  @override
  Future<Either<Failure, List<SearchHistoryEntity>>> call(
      NoParams params) async {
    return _repository.getSearchHistory();
  }
}
