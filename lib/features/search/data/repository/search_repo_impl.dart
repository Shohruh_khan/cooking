import 'package:cooking/core/data/boxes.dart';
import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';

import '../../../../core/data/either.dart';
import '../../../../core/data/network_info.dart';
import '../../../../core/error/exeptions.dart';
import '../../../../core/error/failure.dart';
import '../../../main/data/models/converter.dart';
import '../../../main/domain/entity/recipe.dart';
import '../../domain/entity/search_history.dart';
import '../../domain/repository/search_repo.dart';
import '../data_source/local_data_source.dart';
import '../data_source/remote_data_source.dart';

class SearchRepositoryImpl extends SearchRepository {
  final SearchLocalDataSource _localDataSource = SearchLocalDataSourceImpl();

  final NetworkInfo _networkInfo = const NetworkInfoImpl();
  final SearchRemoteDataSourceImpl _remoteDataSource =
      SearchRemoteDataSourceImpl();
  final Box _searchHistoryBox =
      Hive.box<List<SearchHistoryEntity>>(searchHistoryKey);

  @override
  Future<Either<Failure, List<RecipeEntity>>> getResults(String query) async {
    if (await _networkInfo.connected) {
      try {
        final data = await _remoteDataSource.getResults(query);
        _localDataSource
            .putSearchHistoryElement(SearchHistoryEntity(name: query));
        return Right(MainConverter.recipeModelsToEntities(data));
      } on ServerException {
        rethrow;
      } catch (e) {
        return Left(ServerFailure(
          errorMessage: e.toString(),
        ));
      }
    } else {
      return Left(const ServerFailure(errorMessage: 'No Internet'));
    }
  }

  @override
  Either<CacheFailure, List<SearchHistoryEntity>> getSearchHistory() {
    try {
      final result = _searchHistoryBox.get(searchHistoryKey)
          as List<SearchHistoryEntity>?; //TODO add null
      debugPrint("searchHistory entity Successfully run get");
      return Right(result ?? []);
    } on CacheException {
      rethrow;
    } catch (e) {
      return Left(const CacheFailure(errorMessage: 'error'));
    }
  }

  @override
  Future<Either<CacheFailure, void>> putSearchHistory(
      List<SearchHistoryEntity> entity) async {
    try {
      await _searchHistoryBox.add(entity);
      debugPrint("searchHistory entity successfully added");
      return Right(null);
    } on CacheException {
      rethrow;
    } catch (e) {
      return Left(const CacheFailure(errorMessage: 'error'));
    }
  }

  @override
  Future<Either<CacheFailure, void>> removeSearchHistory(String id) async {
    try {
      final List<SearchHistoryEntity>? searchHistory =
          _searchHistoryBox.get(searchHistoryKey) as List<SearchHistoryEntity>?;

      if (searchHistory != null) {
        final removeHistoryEntity = searchHistory.firstWhere(
          (element) => element.id == id,
        );
        await _searchHistoryBox.delete(removeHistoryEntity.id);
        debugPrint("remove run from repository");
        // if (removeHistoryEntity != null) {
        //
        // } else {
        //   print("No element with the specified id found.");
        // }
      }
      return Right(null);
    } on CacheException {
      rethrow;
    } catch (e) {
      return Left(const CacheFailure(errorMessage: 'error'));
    }
  }
}
