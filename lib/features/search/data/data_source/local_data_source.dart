import 'package:cooking/core/data/boxes.dart';
import 'package:hive/hive.dart';

import '../../domain/entity/search_history.dart';

abstract class SearchLocalDataSource {
  void putSearchHistoryElement(SearchHistoryEntity entity);
  List<SearchHistoryEntity> getSearchHistory();
  void removeSearchHistoryElement(String id);
}

class SearchLocalDataSourceImpl implements SearchLocalDataSource {
  // SearchLocalDataSourceImpl(Box? searchBox) {
  //   _searchBox = searchBox;
  // }
  // SearchLocalDataSourceImpl({required this.searchBox});

  Box searchBox = Hive.box<List<SearchHistoryEntity>>(searchHistoryKey);
  @override
  List<SearchHistoryEntity> getSearchHistory() {
    final getSearch = searchBox.get(searchHistoryKey);
    return getSearch ?? [];
  }

  @override
  void putSearchHistoryElement(SearchHistoryEntity entity) {
    // objectbox.putSearchHistoryElement(entity);
    searchBox.put(searchHistoryKey, [entity]) ?? [];
    // return putSearchBox??[];
    // await searchBox.put(key, value)
  }

  // @override
  // Future<void> removeSearchHistoryElement(int id) async {
  //   final searchToDelete =
  //       await searchBox.values.firstWhere((element) => element.id == id);
  //   await searchToDelete.delete();
  //   // objectbox.removeSearchHistoryElement(id);
  // }
  @override
  Future<void> removeSearchHistoryElement(String id) async {
    await searchBox.delete(id);
  }
}
