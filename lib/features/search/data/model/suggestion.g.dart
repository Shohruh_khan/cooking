// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'suggestion.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$SuggestionModelImpl _$$SuggestionModelImplFromJson(
        Map<String, dynamic> json) =>
    _$SuggestionModelImpl(
      id: json['id'] as int,
      title: json['title'] as String,
    );

Map<String, dynamic> _$$SuggestionModelImplToJson(
        _$SuggestionModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
    };
