import 'package:cooking/core/data/boxes.dart';
import 'package:flutter/cupertino.dart';
import 'package:hive/hive.dart';

import '../../../../core/data/either.dart';
import '../../../../core/error/exeptions.dart';
import '../../../../core/error/failure.dart';
import '../../../main/domain/entity/recipe.dart';
import '../../domain/repository/favourites_repo.dart';

class FavouritesRepositoryImpl extends FavouritesRepository {
  // FavouritesRepositoryImpl(Box recipeBox) {
  //   _recipeBox = recipeBox;
  // }

  final Box _recipeBox = Hive.box<List<RecipeEntity>>(recipesKey);

  @override
  Either<CacheFailure, List<RecipeEntity>> getEntities() {
    try {
      final result = _recipeBox.get(recipesKey) as List<RecipeEntity>?;
      debugPrint("get entity: Successfull");
      return Right(result ?? []);
    } on CacheException {
      rethrow;
    } catch (e) {
      return Left(const CacheFailure(
          errorMessage: 'Error Occured on local storage to get'));
    }
  }

  @override
  Future<Either<CacheFailure, void>> putEntities(
      List<RecipeEntity> recipes) async {
    try {
      await _recipeBox.add(recipes);
      print("put entity: Successfull");
      return Right(null);
    } on CacheException {
      rethrow;
    } catch (e) {
      return Left(const CacheFailure(
          errorMessage: 'Error Occured on local storage to put'));
    }
  }

  @override
  Future<Either<CacheFailure, void>> removeEntity(int id) async {
    try {
      // objectbox?.removeEntity(id);
      final recipeToDelete =
          await _recipeBox.values.firstWhere((element) => element.id == id);
      await recipeToDelete.delete();
      return Right(null);
    } on CacheException {
      rethrow;
    } catch (e) {
      return Left(CacheFailure(errorMessage: 'error'));
    }
  }
}
