import 'package:cooking/features/main/presentation/pages/recipe_instruction.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lottie/lottie.dart';

import '../../../assets/animations/animations.dart';
import '../../../assets/colors/colors.dart';
import '../../../core/models/formz/formz_status.dart';
import '../../main/presentation/widgets/recipe_menu.dart';
import 'blocs/favorite_bloc.dart';

class FavouritesPage extends StatelessWidget {
  const FavouritesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      return Scaffold(
        appBar: AppBar(
          elevation: 1,
          title: Text('wishlist'.tr(),
              style: Theme.of(context).textTheme.headlineMedium),
          backgroundColor: primary,
        ),
        body: BlocBuilder<FavoriteBloc, FavoriteState>(
          builder: (context, state) {
            if (state.status == FormzStatus.pure) {
              context.read<FavoriteBloc>().add(FavoriteEvent.getRecipes(
                onFailure: (value) {
                  debugPrint("here error: $value");
                  // AppFunctions.showSnackbar(context, value);
                },
              ));
              return const SizedBox();
            } else if (state.status == FormzStatus.submissionInProgress) {
              return const CircularProgressIndicator();
            } else if (state.status == FormzStatus.submissionSuccess) {
              if (state.entities.isNotEmpty) {
                return ListView.separated(
                  padding: const EdgeInsets.all(16),
                  itemBuilder: (context, index) => GestureDetector(
                      onTap: () => Navigator.of(
                            context,
                            rootNavigator: true,
                          ).push(MaterialPageRoute(
                            builder: (
                              context,
                            ) =>
                                RecipeInstruction(data: state.entities[index]),
                          )),
                      child: RecipesMenu(data: state.entities[index])),
                  separatorBuilder: (_, __) => const SizedBox(
                    height: 10,
                  ),
                  itemCount: state.entities.length,
                );
              } else {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      LottieBuilder.asset(
                        AppAnimations.empty,
                        height: 200.h,
                      ),
                      Text(
                        'empty'.tr(),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                );
              }
            } else {
              return Text(state.errorMessage);
            }
          },
        ),
      );
    });
  }
}
