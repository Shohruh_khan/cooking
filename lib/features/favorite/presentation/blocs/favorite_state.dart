part of 'favorite_bloc.dart';

@freezed
class FavoriteState with _$FavoriteState {
  factory FavoriteState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default([]) List<RecipeEntity> entities,
    @Default('') String errorMessage,
    @Default(false) bool isLiked,
  }) = _FavoriteState;
}
