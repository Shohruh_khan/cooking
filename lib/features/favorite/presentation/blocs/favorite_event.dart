part of 'favorite_bloc.dart';

@freezed
class FavoriteEvent with _$FavoriteEvent {
  const factory FavoriteEvent.getRecipes(
      {required ValueChanged<String> onFailure}) = _GetRecipes;
  const factory FavoriteEvent.addRecipe(RecipeEntity entity) = _AddRecipe;
  const factory FavoriteEvent.removeRecipe(RecipeEntity entity) = _RemoveRecipe;
  const factory FavoriteEvent.isLiked({required bool like}) = _IsLiked;
}
