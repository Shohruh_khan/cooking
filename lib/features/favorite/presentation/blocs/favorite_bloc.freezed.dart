// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'favorite_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$FavoriteEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) getRecipes,
    required TResult Function(RecipeEntity entity) addRecipe,
    required TResult Function(RecipeEntity entity) removeRecipe,
    required TResult Function(bool like) isLiked,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? getRecipes,
    TResult? Function(RecipeEntity entity)? addRecipe,
    TResult? Function(RecipeEntity entity)? removeRecipe,
    TResult? Function(bool like)? isLiked,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? getRecipes,
    TResult Function(RecipeEntity entity)? addRecipe,
    TResult Function(RecipeEntity entity)? removeRecipe,
    TResult Function(bool like)? isLiked,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetRecipes value) getRecipes,
    required TResult Function(_AddRecipe value) addRecipe,
    required TResult Function(_RemoveRecipe value) removeRecipe,
    required TResult Function(_IsLiked value) isLiked,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetRecipes value)? getRecipes,
    TResult? Function(_AddRecipe value)? addRecipe,
    TResult? Function(_RemoveRecipe value)? removeRecipe,
    TResult? Function(_IsLiked value)? isLiked,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetRecipes value)? getRecipes,
    TResult Function(_AddRecipe value)? addRecipe,
    TResult Function(_RemoveRecipe value)? removeRecipe,
    TResult Function(_IsLiked value)? isLiked,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FavoriteEventCopyWith<$Res> {
  factory $FavoriteEventCopyWith(
          FavoriteEvent value, $Res Function(FavoriteEvent) then) =
      _$FavoriteEventCopyWithImpl<$Res, FavoriteEvent>;
}

/// @nodoc
class _$FavoriteEventCopyWithImpl<$Res, $Val extends FavoriteEvent>
    implements $FavoriteEventCopyWith<$Res> {
  _$FavoriteEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GetRecipesImplCopyWith<$Res> {
  factory _$$GetRecipesImplCopyWith(
          _$GetRecipesImpl value, $Res Function(_$GetRecipesImpl) then) =
      __$$GetRecipesImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ValueChanged<String> onFailure});
}

/// @nodoc
class __$$GetRecipesImplCopyWithImpl<$Res>
    extends _$FavoriteEventCopyWithImpl<$Res, _$GetRecipesImpl>
    implements _$$GetRecipesImplCopyWith<$Res> {
  __$$GetRecipesImplCopyWithImpl(
      _$GetRecipesImpl _value, $Res Function(_$GetRecipesImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? onFailure = null,
  }) {
    return _then(_$GetRecipesImpl(
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
    ));
  }
}

/// @nodoc

class _$GetRecipesImpl implements _GetRecipes {
  const _$GetRecipesImpl({required this.onFailure});

  @override
  final ValueChanged<String> onFailure;

  @override
  String toString() {
    return 'FavoriteEvent.getRecipes(onFailure: $onFailure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetRecipesImpl &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, onFailure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GetRecipesImplCopyWith<_$GetRecipesImpl> get copyWith =>
      __$$GetRecipesImplCopyWithImpl<_$GetRecipesImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) getRecipes,
    required TResult Function(RecipeEntity entity) addRecipe,
    required TResult Function(RecipeEntity entity) removeRecipe,
    required TResult Function(bool like) isLiked,
  }) {
    return getRecipes(onFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? getRecipes,
    TResult? Function(RecipeEntity entity)? addRecipe,
    TResult? Function(RecipeEntity entity)? removeRecipe,
    TResult? Function(bool like)? isLiked,
  }) {
    return getRecipes?.call(onFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? getRecipes,
    TResult Function(RecipeEntity entity)? addRecipe,
    TResult Function(RecipeEntity entity)? removeRecipe,
    TResult Function(bool like)? isLiked,
    required TResult orElse(),
  }) {
    if (getRecipes != null) {
      return getRecipes(onFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetRecipes value) getRecipes,
    required TResult Function(_AddRecipe value) addRecipe,
    required TResult Function(_RemoveRecipe value) removeRecipe,
    required TResult Function(_IsLiked value) isLiked,
  }) {
    return getRecipes(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetRecipes value)? getRecipes,
    TResult? Function(_AddRecipe value)? addRecipe,
    TResult? Function(_RemoveRecipe value)? removeRecipe,
    TResult? Function(_IsLiked value)? isLiked,
  }) {
    return getRecipes?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetRecipes value)? getRecipes,
    TResult Function(_AddRecipe value)? addRecipe,
    TResult Function(_RemoveRecipe value)? removeRecipe,
    TResult Function(_IsLiked value)? isLiked,
    required TResult orElse(),
  }) {
    if (getRecipes != null) {
      return getRecipes(this);
    }
    return orElse();
  }
}

abstract class _GetRecipes implements FavoriteEvent {
  const factory _GetRecipes({required final ValueChanged<String> onFailure}) =
      _$GetRecipesImpl;

  ValueChanged<String> get onFailure;
  @JsonKey(ignore: true)
  _$$GetRecipesImplCopyWith<_$GetRecipesImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$AddRecipeImplCopyWith<$Res> {
  factory _$$AddRecipeImplCopyWith(
          _$AddRecipeImpl value, $Res Function(_$AddRecipeImpl) then) =
      __$$AddRecipeImplCopyWithImpl<$Res>;
  @useResult
  $Res call({RecipeEntity entity});
}

/// @nodoc
class __$$AddRecipeImplCopyWithImpl<$Res>
    extends _$FavoriteEventCopyWithImpl<$Res, _$AddRecipeImpl>
    implements _$$AddRecipeImplCopyWith<$Res> {
  __$$AddRecipeImplCopyWithImpl(
      _$AddRecipeImpl _value, $Res Function(_$AddRecipeImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? entity = null,
  }) {
    return _then(_$AddRecipeImpl(
      null == entity
          ? _value.entity
          : entity // ignore: cast_nullable_to_non_nullable
              as RecipeEntity,
    ));
  }
}

/// @nodoc

class _$AddRecipeImpl implements _AddRecipe {
  const _$AddRecipeImpl(this.entity);

  @override
  final RecipeEntity entity;

  @override
  String toString() {
    return 'FavoriteEvent.addRecipe(entity: $entity)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AddRecipeImpl &&
            (identical(other.entity, entity) || other.entity == entity));
  }

  @override
  int get hashCode => Object.hash(runtimeType, entity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AddRecipeImplCopyWith<_$AddRecipeImpl> get copyWith =>
      __$$AddRecipeImplCopyWithImpl<_$AddRecipeImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) getRecipes,
    required TResult Function(RecipeEntity entity) addRecipe,
    required TResult Function(RecipeEntity entity) removeRecipe,
    required TResult Function(bool like) isLiked,
  }) {
    return addRecipe(entity);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? getRecipes,
    TResult? Function(RecipeEntity entity)? addRecipe,
    TResult? Function(RecipeEntity entity)? removeRecipe,
    TResult? Function(bool like)? isLiked,
  }) {
    return addRecipe?.call(entity);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? getRecipes,
    TResult Function(RecipeEntity entity)? addRecipe,
    TResult Function(RecipeEntity entity)? removeRecipe,
    TResult Function(bool like)? isLiked,
    required TResult orElse(),
  }) {
    if (addRecipe != null) {
      return addRecipe(entity);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetRecipes value) getRecipes,
    required TResult Function(_AddRecipe value) addRecipe,
    required TResult Function(_RemoveRecipe value) removeRecipe,
    required TResult Function(_IsLiked value) isLiked,
  }) {
    return addRecipe(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetRecipes value)? getRecipes,
    TResult? Function(_AddRecipe value)? addRecipe,
    TResult? Function(_RemoveRecipe value)? removeRecipe,
    TResult? Function(_IsLiked value)? isLiked,
  }) {
    return addRecipe?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetRecipes value)? getRecipes,
    TResult Function(_AddRecipe value)? addRecipe,
    TResult Function(_RemoveRecipe value)? removeRecipe,
    TResult Function(_IsLiked value)? isLiked,
    required TResult orElse(),
  }) {
    if (addRecipe != null) {
      return addRecipe(this);
    }
    return orElse();
  }
}

abstract class _AddRecipe implements FavoriteEvent {
  const factory _AddRecipe(final RecipeEntity entity) = _$AddRecipeImpl;

  RecipeEntity get entity;
  @JsonKey(ignore: true)
  _$$AddRecipeImplCopyWith<_$AddRecipeImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RemoveRecipeImplCopyWith<$Res> {
  factory _$$RemoveRecipeImplCopyWith(
          _$RemoveRecipeImpl value, $Res Function(_$RemoveRecipeImpl) then) =
      __$$RemoveRecipeImplCopyWithImpl<$Res>;
  @useResult
  $Res call({RecipeEntity entity});
}

/// @nodoc
class __$$RemoveRecipeImplCopyWithImpl<$Res>
    extends _$FavoriteEventCopyWithImpl<$Res, _$RemoveRecipeImpl>
    implements _$$RemoveRecipeImplCopyWith<$Res> {
  __$$RemoveRecipeImplCopyWithImpl(
      _$RemoveRecipeImpl _value, $Res Function(_$RemoveRecipeImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? entity = null,
  }) {
    return _then(_$RemoveRecipeImpl(
      null == entity
          ? _value.entity
          : entity // ignore: cast_nullable_to_non_nullable
              as RecipeEntity,
    ));
  }
}

/// @nodoc

class _$RemoveRecipeImpl implements _RemoveRecipe {
  const _$RemoveRecipeImpl(this.entity);

  @override
  final RecipeEntity entity;

  @override
  String toString() {
    return 'FavoriteEvent.removeRecipe(entity: $entity)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RemoveRecipeImpl &&
            (identical(other.entity, entity) || other.entity == entity));
  }

  @override
  int get hashCode => Object.hash(runtimeType, entity);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RemoveRecipeImplCopyWith<_$RemoveRecipeImpl> get copyWith =>
      __$$RemoveRecipeImplCopyWithImpl<_$RemoveRecipeImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) getRecipes,
    required TResult Function(RecipeEntity entity) addRecipe,
    required TResult Function(RecipeEntity entity) removeRecipe,
    required TResult Function(bool like) isLiked,
  }) {
    return removeRecipe(entity);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? getRecipes,
    TResult? Function(RecipeEntity entity)? addRecipe,
    TResult? Function(RecipeEntity entity)? removeRecipe,
    TResult? Function(bool like)? isLiked,
  }) {
    return removeRecipe?.call(entity);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? getRecipes,
    TResult Function(RecipeEntity entity)? addRecipe,
    TResult Function(RecipeEntity entity)? removeRecipe,
    TResult Function(bool like)? isLiked,
    required TResult orElse(),
  }) {
    if (removeRecipe != null) {
      return removeRecipe(entity);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetRecipes value) getRecipes,
    required TResult Function(_AddRecipe value) addRecipe,
    required TResult Function(_RemoveRecipe value) removeRecipe,
    required TResult Function(_IsLiked value) isLiked,
  }) {
    return removeRecipe(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetRecipes value)? getRecipes,
    TResult? Function(_AddRecipe value)? addRecipe,
    TResult? Function(_RemoveRecipe value)? removeRecipe,
    TResult? Function(_IsLiked value)? isLiked,
  }) {
    return removeRecipe?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetRecipes value)? getRecipes,
    TResult Function(_AddRecipe value)? addRecipe,
    TResult Function(_RemoveRecipe value)? removeRecipe,
    TResult Function(_IsLiked value)? isLiked,
    required TResult orElse(),
  }) {
    if (removeRecipe != null) {
      return removeRecipe(this);
    }
    return orElse();
  }
}

abstract class _RemoveRecipe implements FavoriteEvent {
  const factory _RemoveRecipe(final RecipeEntity entity) = _$RemoveRecipeImpl;

  RecipeEntity get entity;
  @JsonKey(ignore: true)
  _$$RemoveRecipeImplCopyWith<_$RemoveRecipeImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$IsLikedImplCopyWith<$Res> {
  factory _$$IsLikedImplCopyWith(
          _$IsLikedImpl value, $Res Function(_$IsLikedImpl) then) =
      __$$IsLikedImplCopyWithImpl<$Res>;
  @useResult
  $Res call({bool like});
}

/// @nodoc
class __$$IsLikedImplCopyWithImpl<$Res>
    extends _$FavoriteEventCopyWithImpl<$Res, _$IsLikedImpl>
    implements _$$IsLikedImplCopyWith<$Res> {
  __$$IsLikedImplCopyWithImpl(
      _$IsLikedImpl _value, $Res Function(_$IsLikedImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? like = null,
  }) {
    return _then(_$IsLikedImpl(
      like: null == like
          ? _value.like
          : like // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$IsLikedImpl implements _IsLiked {
  const _$IsLikedImpl({required this.like});

  @override
  final bool like;

  @override
  String toString() {
    return 'FavoriteEvent.isLiked(like: $like)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$IsLikedImpl &&
            (identical(other.like, like) || other.like == like));
  }

  @override
  int get hashCode => Object.hash(runtimeType, like);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$IsLikedImplCopyWith<_$IsLikedImpl> get copyWith =>
      __$$IsLikedImplCopyWithImpl<_$IsLikedImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) getRecipes,
    required TResult Function(RecipeEntity entity) addRecipe,
    required TResult Function(RecipeEntity entity) removeRecipe,
    required TResult Function(bool like) isLiked,
  }) {
    return isLiked(like);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? getRecipes,
    TResult? Function(RecipeEntity entity)? addRecipe,
    TResult? Function(RecipeEntity entity)? removeRecipe,
    TResult? Function(bool like)? isLiked,
  }) {
    return isLiked?.call(like);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? getRecipes,
    TResult Function(RecipeEntity entity)? addRecipe,
    TResult Function(RecipeEntity entity)? removeRecipe,
    TResult Function(bool like)? isLiked,
    required TResult orElse(),
  }) {
    if (isLiked != null) {
      return isLiked(like);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetRecipes value) getRecipes,
    required TResult Function(_AddRecipe value) addRecipe,
    required TResult Function(_RemoveRecipe value) removeRecipe,
    required TResult Function(_IsLiked value) isLiked,
  }) {
    return isLiked(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetRecipes value)? getRecipes,
    TResult? Function(_AddRecipe value)? addRecipe,
    TResult? Function(_RemoveRecipe value)? removeRecipe,
    TResult? Function(_IsLiked value)? isLiked,
  }) {
    return isLiked?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetRecipes value)? getRecipes,
    TResult Function(_AddRecipe value)? addRecipe,
    TResult Function(_RemoveRecipe value)? removeRecipe,
    TResult Function(_IsLiked value)? isLiked,
    required TResult orElse(),
  }) {
    if (isLiked != null) {
      return isLiked(this);
    }
    return orElse();
  }
}

abstract class _IsLiked implements FavoriteEvent {
  const factory _IsLiked({required final bool like}) = _$IsLikedImpl;

  bool get like;
  @JsonKey(ignore: true)
  _$$IsLikedImplCopyWith<_$IsLikedImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$FavoriteState {
  FormzStatus get status => throw _privateConstructorUsedError;
  List<RecipeEntity> get entities => throw _privateConstructorUsedError;
  String get errorMessage => throw _privateConstructorUsedError;
  bool get isLiked => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FavoriteStateCopyWith<FavoriteState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FavoriteStateCopyWith<$Res> {
  factory $FavoriteStateCopyWith(
          FavoriteState value, $Res Function(FavoriteState) then) =
      _$FavoriteStateCopyWithImpl<$Res, FavoriteState>;
  @useResult
  $Res call(
      {FormzStatus status,
      List<RecipeEntity> entities,
      String errorMessage,
      bool isLiked});
}

/// @nodoc
class _$FavoriteStateCopyWithImpl<$Res, $Val extends FavoriteState>
    implements $FavoriteStateCopyWith<$Res> {
  _$FavoriteStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? entities = null,
    Object? errorMessage = null,
    Object? isLiked = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      entities: null == entities
          ? _value.entities
          : entities // ignore: cast_nullable_to_non_nullable
              as List<RecipeEntity>,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
      isLiked: null == isLiked
          ? _value.isLiked
          : isLiked // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FavoriteStateImplCopyWith<$Res>
    implements $FavoriteStateCopyWith<$Res> {
  factory _$$FavoriteStateImplCopyWith(
          _$FavoriteStateImpl value, $Res Function(_$FavoriteStateImpl) then) =
      __$$FavoriteStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      List<RecipeEntity> entities,
      String errorMessage,
      bool isLiked});
}

/// @nodoc
class __$$FavoriteStateImplCopyWithImpl<$Res>
    extends _$FavoriteStateCopyWithImpl<$Res, _$FavoriteStateImpl>
    implements _$$FavoriteStateImplCopyWith<$Res> {
  __$$FavoriteStateImplCopyWithImpl(
      _$FavoriteStateImpl _value, $Res Function(_$FavoriteStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? entities = null,
    Object? errorMessage = null,
    Object? isLiked = null,
  }) {
    return _then(_$FavoriteStateImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      entities: null == entities
          ? _value._entities
          : entities // ignore: cast_nullable_to_non_nullable
              as List<RecipeEntity>,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
      isLiked: null == isLiked
          ? _value.isLiked
          : isLiked // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$FavoriteStateImpl implements _FavoriteState {
  _$FavoriteStateImpl(
      {this.status = FormzStatus.pure,
      final List<RecipeEntity> entities = const [],
      this.errorMessage = '',
      this.isLiked = false})
      : _entities = entities;

  @override
  @JsonKey()
  final FormzStatus status;
  final List<RecipeEntity> _entities;
  @override
  @JsonKey()
  List<RecipeEntity> get entities {
    if (_entities is EqualUnmodifiableListView) return _entities;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_entities);
  }

  @override
  @JsonKey()
  final String errorMessage;
  @override
  @JsonKey()
  final bool isLiked;

  @override
  String toString() {
    return 'FavoriteState(status: $status, entities: $entities, errorMessage: $errorMessage, isLiked: $isLiked)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FavoriteStateImpl &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality().equals(other._entities, _entities) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage) &&
            (identical(other.isLiked, isLiked) || other.isLiked == isLiked));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status,
      const DeepCollectionEquality().hash(_entities), errorMessage, isLiked);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FavoriteStateImplCopyWith<_$FavoriteStateImpl> get copyWith =>
      __$$FavoriteStateImplCopyWithImpl<_$FavoriteStateImpl>(this, _$identity);
}

abstract class _FavoriteState implements FavoriteState {
  factory _FavoriteState(
      {final FormzStatus status,
      final List<RecipeEntity> entities,
      final String errorMessage,
      final bool isLiked}) = _$FavoriteStateImpl;

  @override
  FormzStatus get status;
  @override
  List<RecipeEntity> get entities;
  @override
  String get errorMessage;
  @override
  bool get isLiked;
  @override
  @JsonKey(ignore: true)
  _$$FavoriteStateImplCopyWith<_$FavoriteStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
