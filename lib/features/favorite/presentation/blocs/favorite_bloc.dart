import 'package:bloc/bloc.dart';
import 'package:cooking/features/favorite/domain/repository/favourites_repo.dart';
import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../core/models/formz/formz_status.dart';
import '../../../main/domain/entity/recipe.dart';

part 'favorite_bloc.freezed.dart';
part 'favorite_event.dart';
part 'favorite_state.dart';

class FavoriteBloc extends Bloc<FavoriteEvent, FavoriteState> {
  final FavouritesRepository _repository;

  FavoriteBloc({required FavouritesRepository repository})
      : _repository = repository,
        super(_FavoriteState()) {
    on<_GetRecipes>((event, emit) {
      final recipes = _repository.getEntities();
      recipes.either((fail) {
        event.onFailure(fail.errorMessage);
      }, (data) {
        emit(state.copyWith(
            status: FormzStatus.submissionSuccess, entities: data));
      });
    });
    on<_AddRecipe>((event, emit) async {
      final newEntities = state.entities + [event.entity];
      final result = await _repository.putEntities(newEntities);
      result.either((value) {
        print("bloc added fail");
      }, (_) {
        print("bloc added success");
      });
      print("bloc added");
      emit(state.copyWith(entities: newEntities));
    });

    on<_RemoveRecipe>((event, emit) async {
      List<RecipeEntity> newEntities = [];
      for (int i = 0; i < state.entities.length; i++) {
        if (state.entities[i] != event.entity) {
          newEntities.add(state.entities[i]);
        }
      }
      final result = await _repository.removeEntity(event.entity.id);
      result.either((value) {}, (_) {});
      emit(state.copyWith(entities: newEntities));
    });
    on<_IsLiked>((event, emit) {
      emit(state.copyWith(isLiked: event.like));
    });
  }
}
