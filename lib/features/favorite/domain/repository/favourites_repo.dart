import '../../../../core/data/either.dart';
import '../../../../core/error/failure.dart';
import '../../../main/domain/entity/recipe.dart';

abstract class FavouritesRepository {
  Either<CacheFailure, List<RecipeEntity>> getEntities();
  Future<Either<CacheFailure, void>> putEntities(List<RecipeEntity> recipes);
  Future<Either<CacheFailure, void>> removeEntity(int id);
}
