import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

class ChangeLanguageAlert extends StatelessWidget {
  const ChangeLanguageAlert({super.key, required this.onTap});
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text('available_english'.tr(),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyMedium),
      content: Text('want_continue'.tr(),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.titleMedium),
      actionsAlignment: MainAxisAlignment.spaceBetween,
      actions: [
        TextButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Text('cancel'.tr(),
              style: Theme.of(context).textTheme.labelMedium),
        ),
        TextButton(
          onPressed: onTap,
          child: Text('continue'.tr(),
              style: Theme.of(context).textTheme.labelMedium),
        ),
      ],
    );
  }
}
