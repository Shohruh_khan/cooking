import 'package:flutter/material.dart';

import '../../../../assets/colors/colors.dart';
import '../../../../core/widgets/w_scale.dart';

class ItemBottomsheet extends StatelessWidget {
  const ItemBottomsheet({
    Key? key,
    this.icon,
    required this.text,
    required this.onTap,
    required this.textStyle,
  }) : super(key: key);

  final Widget? icon;
  final String text;
  final VoidCallback onTap;
  final TextStyle textStyle;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
              color: borderColor, style: BorderStyle.solid, width: 1.5),
        ),
        padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 14),
        child: Row(
          children: [
            Container(child: icon),
            const SizedBox(width: 8),
            Text(text, style: textStyle),
          ],
        ),
      ),
    );
  }
}
