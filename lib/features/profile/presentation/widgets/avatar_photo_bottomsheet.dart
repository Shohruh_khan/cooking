import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../assets/icons/icons.dart';
import '../../../../core/bloc/show_pop_up/show_pop_up_bloc.dart';
import '../../../../core/widgets/w_bottomsheet.dart';
import '../../../../core/widgets/w_scale.dart';
import '../blocs/profile_bloc.dart';
import 'item_bottomsheet.dart';

class AvatarPhotoBottomsheet extends StatefulWidget {
  const AvatarPhotoBottomsheet({Key? key}) : super(key: key);

  @override
  State<AvatarPhotoBottomsheet> createState() => _AvatarPhotoBottomsheetState();
}

class _AvatarPhotoBottomsheetState extends State<AvatarPhotoBottomsheet> {
  @override
  Widget build(BuildContext context) {
    return WBottomSheet(
      children: [
        Row(
          children: [
            Text('profile_photo'.tr(),
                style: Theme.of(context).textTheme.titleLarge),
            const Spacer(),
            WScaleAnimation(
                child: SvgPicture.asset(AppIcons.cancel),
                onTap: () {
                  Navigator.pop(context);
                })
          ],
        ),
        const SizedBox(height: 24),
        ItemBottomsheet(
            icon: SvgPicture.asset(AppIcons.gallery),
            text: 'gallery'.tr(),
            onTap: () async {
              Navigator.of(context).pop();
              context.read<ProfileBloc>().add(ProfileEvent.uploadPhoto(
                onFailure: (errorMessage) {
                  context.read<ShowPopUpBloc>().add(
                        ShowPopUpEvent.showFailure(
                          text: errorMessage,
                        ),
                      );
                },
              ));
            },
            textStyle: Theme.of(context).textTheme.titleMedium!),
        const SizedBox(height: 14),
        ItemBottomsheet(
            icon: SvgPicture.asset(AppIcons.camera),
            text: 'camera'.tr(),
            onTap: () async {
              Navigator.of(context).pop();
              context.read<ProfileBloc>().add(
                ProfileEvent.uploadFromCamera(onFailure: (errorMessage) {
                  context.read<ShowPopUpBloc>().add(
                        ShowPopUpEvent.showFailure(
                          text: errorMessage,
                        ),
                      );
                }),
              );
            },
            textStyle: Theme.of(context).textTheme.titleMedium!),
        const SizedBox(height: 14),
        BlocBuilder<ProfileBloc, ProfileState>(
          builder: (context, state) {
            if (state.image.isNotEmpty) {
              return ItemBottomsheet(
                  icon: SvgPicture.asset(AppIcons.trash),
                  text: 'delete'.tr(),
                  onTap: () async {
                    Navigator.of(context).pop();
                    context.read<ProfileBloc>().add(
                          const ProfileEvent.deletePhoto(),
                        );
                  },
                  textStyle: Theme.of(context).textTheme.titleMedium!);
            } else {
              return const SizedBox();
            }
          },
        )
      ],
    );
  }
}
