import 'package:cooking/core/bloc/show_pop_up/show_pop_up_bloc.dart';
import 'package:cooking/core/widgets/w_scale.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../assets/colors/colors.dart';
import '../../../../assets/icons/icons.dart';
import '../../../../core/bloc/translation/translation_bloc.dart';
import '../../../../core/widgets/w_bottomsheet.dart';
import '../../../../core/widgets/w_checkbox.dart';

class TranslateBottomsheet extends StatelessWidget {
  TranslateBottomsheet({Key? key}) : super(key: key);
  bool isChecked = true;

  @override
  Widget build(BuildContext context) {
    return BlocListener<TranslationBloc, TranslationState>(
      listener: (context, state) {
        context.setLocale(Locale(state.language));
      },
      child: WBottomSheet(
        bottomNavigationPadding:
            EdgeInsets.symmetric(vertical: 15.w, horizontal: 16.h),
        children: [
          Center(
            child: Text(
              'choose_lan'.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .displayLarge!
                  .copyWith(fontSize: 18, fontWeight: FontWeight.w700),
            ),
          ),
          const SizedBox(height: 40),
          WScaleAnimation(
            onTap: () {
              context
                  .read<TranslationBloc>()
                  .add(TranslationEvent.changeLanguage(languageName: 'ru'));

              // context
              //     .read<ShowPopUpBloc>()
              //     .add(ShowPopUpEvent.showSuccess(text: 'success_lan'.tr()));
              Navigator.of(context).pop();
              // showDialog(
              //   context: context,
              //   builder: (BuildContext context) {
              //     return ChangeLanguageAlert(
              //       onTap: () {
              //         context.read<TranslationBloc>().add(
              //             TranslationEvent.changeLanguage(languageName: 'ru'));
              //
              //         context.read<ShowPopUpBloc>().add(
              //             ShowPopUpEvent.showSuccess(text: 'success_lan'.tr()));
              //         Navigator.pop(context);
              //       },
              //     );
              //   },
              // );
            },
            child: Row(
              children: [
                SvgPicture.asset(AppIcons.flagRu),
                SizedBox(width: 16.w),
                Text(
                  'Русский',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const Spacer(),
                BlocBuilder<TranslationBloc, TranslationState>(
                  builder: (context, state) {
                    return WCheckBox(
                        borderRadius: 100,
                        activeColor: onSecondary,
                        value: (state.language == 'ru') ? true : false,
                        onPressed: (newValue) {
                          Navigator.of(context).pop();
                          context.read<TranslationBloc>().add(
                              TranslationEvent.changeLanguage(
                                  languageName: 'ru'));
                        });
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          WScaleAnimation(
            onTap: () {
              // context
              //     .read<ShowPopUpBloc>()
              //     .add(ShowPopUpEvent.showSuccess(text: 'success_lan'.tr()));
              context
                  .read<TranslationBloc>()
                  .add(TranslationEvent.changeLanguage(languageName: 'uz'));
              Navigator.of(context).pop();
              // showDialog(
              //   context: context,
              //   builder: (BuildContext context) {
              //     return ChangeLanguageAlert(
              //       onTap: () {
              //         context.read<ShowPopUpBloc>().add(
              //             ShowPopUpEvent.showSuccess(text: 'success_lan'.tr()));
              //         context.read<TranslationBloc>().add(
              //             TranslationEvent.changeLanguage(languageName: 'uz'));
              //         Navigator.of(context).pop();
              //       },
              //     );
              //   },
              // );
            },
            child: Row(
              children: [
                // Image.asset(AppIcons.flagUzb),
                SvgPicture.asset(AppIcons.flagUzb),
                const SizedBox(width: 16),
                Text(
                  "O'zbek ",
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const Spacer(),
                BlocBuilder<TranslationBloc, TranslationState>(
                  builder: (context, state) {
                    return WCheckBox(
                        borderRadius: 100,
                        value: (state.language == 'uz') ? true : false,
                        onPressed: (newValue) {
                          Navigator.of(context).pop();
                          context.read<TranslationBloc>().add(
                              TranslationEvent.changeLanguage(
                                  languageName: 'uz'));
                        });
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 20),
          WScaleAnimation(
            onTap: () {
              context
                  .read<ShowPopUpBloc>()
                  .add(ShowPopUpEvent.showSuccess(text: 'success_lan'.tr()));
              Navigator.of(context).pop();
              context
                  .read<TranslationBloc>()
                  .add(TranslationEvent.changeLanguage(languageName: 'en'));
            },
            child: Row(
              children: [
                SvgPicture.asset(AppIcons.flagEn),
                const SizedBox(width: 16),
                Text(
                  "English",
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                const Spacer(),
                BlocBuilder<TranslationBloc, TranslationState>(
                  builder: (context, state) {
                    return WCheckBox(
                        borderRadius: 100,
                        value: (state.language == 'en') ? true : false,
                        onPressed: (newValue) {
                          Navigator.of(context).pop();
                          context.read<TranslationBloc>().add(
                              TranslationEvent.changeLanguage(
                                  languageName: 'en'));
                        });
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
