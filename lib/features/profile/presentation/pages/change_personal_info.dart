// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:go_router/go_router.dart';
// import 'package:top_top/assets/icons/icons.dart';
// import 'package:top_top/core/app_functions.dart';
// import 'package:top_top/features/account/presentation/pages/change_name_info.dart';
//
// import '../../../../assets/colors/colors.dart';
//
// class ChangePersonalInfo extends StatefulWidget {
//   const ChangePersonalInfo({Key? key}) : super(key: key);
//   static const String id = '/account/change_personal_info';
//   @override
//   State<ChangePersonalInfo> createState() => _ChangePersonalInfoState();
// }
//
// class _ChangePersonalInfoState extends State<ChangePersonalInfo> {
//   TextEditingController nameController = TextEditingController();
//
//   TextEditingController surnameController = TextEditingController();
//   DateTime selectedDate = DateTime.now();
//   Future<void> _showDatePicker(BuildContext context) async {
//     await showModalBottomSheet(
//       context: context,
//       builder: (BuildContext builder) {
//         return SizedBox(
//           height: 250.0,
//           child: CupertinoDatePicker(
//             mode: CupertinoDatePickerMode.date,
//             initialDateTime: selectedDate ?? DateTime.now(),
//             onDateTimeChanged: (DateTime newDate) {
//               setState(() {
//                 selectedDate = newDate;
//               });
//             },
//           ),
//         );
//       },
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(
//           "edit_personal_data".tr(),
//           style: Theme.of(context).textTheme.titleLarge,
//         ),
//       ),
//       body: Container(
//         margin: const EdgeInsets.all(20),
//         padding: EdgeInsets.symmetric(vertical: 24.h, horizontal: 20.w),
//         decoration: BoxDecoration(
//           color: white,
//           borderRadius: BorderRadius.circular(14),
//         ),
//         child: Column(
//           children: [
//             ListTile(
//               onTap: () {
//                 // Navigator.of(context).push(fade(
//                 //     page: ChangeName(
//                 //   title: 'name'.tr(),
//                 //   hintText: 'Islom',
//                 //   controller: nameController,
//                 //   onTap: () {
//                 //     Navigator.of(context).pop();
//                 //   },
//                 // )));
//
//                 context.push(
//                   ChangeName.id,
//                   extra: {
//                     'title': 'name'.tr(),
//                     'hintText': 'Islom',
//                     'controller': nameController,
//                     'onTap': () {
//                       Navigator.of(context).pop();
//                     },
//                   },
//                 );
//               },
//               title: Text(
//                 'name'.tr(),
//                 style: Theme.of(context).textTheme.titleMedium,
//               ),
//               subtitle: Text(
//                 'Islom',
//                 style: Theme.of(context).textTheme.bodyMedium,
//               ),
//               trailing: SvgPicture.asset(AppIcons.arrowRight),
//             ),
//             SizedBox(height: 22.h),
//             ListTile(
//               onTap: () {
//                 // Navigator.of(context).push(fade(
//                 //     page: ChangeName(
//                 //   title: 'surname'.tr(),
//                 //   hintText: 'Usmonov',
//                 //   controller: surnameController,
//                 //   onTap: () {
//                 //     Navigator.of(context).pop();
//                 //   },
//                 // )));
//
//                 context.push(
//                   ChangeName.id,
//                   extra: {
//                     'title': 'surname'.tr(),
//                     'hintText': 'Usmonov',
//                     'controller': surnameController,
//                     'onTap': () {
//                       Navigator.of(context).pop();
//                     },
//                   },
//                 );
//               },
//               title: Text('surname'.tr(),
//                   style: Theme.of(context).textTheme.titleMedium),
//               subtitle: Text('Usmonov',
//                   style: Theme.of(context).textTheme.bodyMedium),
//               trailing: SvgPicture.asset(AppIcons.arrowRight),
//             ),
//             SizedBox(height: 22.h),
//             ListTile(
//               onTap: () {
//                 _showDatePicker(context);
//               },
//               title: Text("data_birth".tr(),
//                   style: Theme.of(context).textTheme.titleMedium),
//               subtitle: Text(
//                   (selectedDate.year == DateTime.now().year &&
//                           selectedDate.month == DateTime.now().month &&
//                           selectedDate.day == DateTime.now().day)
//                       ? "data_birth".tr()
//                       : AppFunctions.formatDataOfBirth(selectedDate, context),
//                   // '13 December, 1999',
//                   style: Theme.of(context).textTheme.bodyMedium),
//               trailing: SvgPicture.asset(AppIcons.arrowRight),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
