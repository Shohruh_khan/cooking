// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:top_top/core/widgets/w_button.dart';
// import 'package:top_top/core/widgets/w_text_field/w_text_field.dart';
//
// import '../../../../assets/colors/colors.dart';
//
// class ChangeName extends StatelessWidget {
//   static const String id = '/account/change_car_or_name';
//   const ChangeName(
//       {Key? key,
//       required this.title,
//       required this.hintText,
//       required this.controller,
//       required this.onTap})
//       : super(key: key);
//   final String title;
//   final String hintText;
//   final TextEditingController controller;
//   final VoidCallback onTap;
//   // TextEditingController nameController = TextEditingController();
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(title, style: Theme.of(context).textTheme.titleLarge),
//       ),
//       body: WTextField(
//         height: 68.h,
//         controller: controller,
//         hintText: hintText,
//         hintTextStyle:
//             Theme.of(context).textTheme.bodyMedium!.copyWith(fontSize: 18.sp),
//         onChanged: (value) {},
//         borderRadius: 14,
//         disabledColor: white,
//         textStyle:
//             Theme.of(context).textTheme.titleMedium!.copyWith(fontSize: 18.sp),
//         margin: const EdgeInsets.all(20),
//       ),
//       bottomNavigationBar: WButton(
//         margin: EdgeInsets.symmetric(horizontal: 36.w, vertical: 36.h),
//         onTap: onTap,
//         height: 56.h,
//         color: primary,
//         text: 'save'.tr(),
//       ),
//     );
//   }
// }
