// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_multi_formatter/formatters/masked_input_formatter.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
// import 'package:go_router/go_router.dart';
// import 'package:top_top/core/widgets/w_button.dart';
//
// import '../../../../assets/colors/colors.dart';
// import '../../../../core/bloc/show_pop_up/show_pop_up_bloc.dart';
// import '../../../../core/models/formz/formz_status.dart';
// import '../../../../core/models/formz/formz_type.dart';
// import '../../../../core/models/formz/rules.dart';
// import '../../../../core/widgets/w_text_field/bloc/validation_bloc.dart';
// import '../../../../core/widgets/w_text_field/w_text_field.dart';
// import '../../../login/presentation/pages/verification_screen.dart';
//
// class ChangePhoneNumber extends StatelessWidget {
//   static const String id = '/account/change_number';
//   ChangePhoneNumber({Key? key}) : super(key: key);
//   TextEditingController numberController = TextEditingController();
//   final ValidationBloc validatorBloc = ValidationBloc();
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('phone_number'.tr()),
//       ),
//       body: WTextField(
//         height: 68.h,
//         validationBloc: validatorBloc,
//         controller: numberController,
//         prefix: Padding(
//           padding: EdgeInsets.fromLTRB(24.w, 22.h, 12.w, 20.h),
//           child: Text(
//             '+998',
//             style: Theme.of(context).textTheme.titleLarge,
//           ),
//         ),
//         validationType: FormzType.phoneNumber,
//         rules: const PhoneNumberRules(length: 12),
//         onChanged: (value) {},
//         borderRadius: 14,
//         hasBorderColor: false,
//         textInputFormatters: [
//           FilteringTextInputFormatter.deny(
//               RegExp('[-*.,+/\$!@#%^&()=[~`{}<>\';:_"?]')),
//           MaskedInputFormatter('## ### ## ##'),
//         ],
//         keyBoardType: TextInputType.phone,
//         textStyle: Theme.of(context).textTheme.titleLarge,
//         hintTextStyle: Theme.of(context)
//             .textTheme
//             .titleLarge!
//             .copyWith(fontWeight: FontWeight.w400),
//         hintText: 'phone_number'.tr(),
//         margin: const EdgeInsets.all(20),
//       ),
//       bottomNavigationBar: WButton(
//         width: 165.w,
//         margin: EdgeInsets.symmetric(horizontal: 36.w, vertical: 36.h),
//         onTap: () {
//           if (validatorBloc.state.status == FormzStatus.submissionSuccess) {
//             // Navigator.of(context).push(fade(page: const VerificationScreen()));
//
//             context.push(VerificationScreen.id);
//           } else
//           // if (validatorBloc1.state.status ==
//           //   FormzStatus.submissionFailure)
//           {
//             context
//                 .read<ShowPopUpBloc>()
//                 .add(ShowPopUpEvent.showFailure(text: 'pop_up_failure'.tr()));
//           }
//           // else {
//           //   print(validatorBloc1.state.status);
//           // }
//         },
//         height: 56.h,
//         color: primary,
//         text: 'send_msg'.tr(),
//       ),
//     );
//   }
// }
