import 'package:bloc/bloc.dart';
import 'package:cooking/core/models/formz/formz_status.dart';
import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../core/app_functions.dart';

part 'profile_bloc.freezed.dart';
part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(const ProfileState()) {
    on<_UploadPhoto>((event, emit) async {
      emit(state.copyWith(status: FormzStatus.submissionInProgress));
      final response = await AppFunctions.pickImage();

      response.either(
        (failureMessage) {
          emit(state.copyWith(status: FormzStatus.submissionFailure));

          event.onFailure(failureMessage);
        },
        (photo) {
          emit(
            state.copyWith(
              image: photo.path,
            ),
          );
        },
      );
    });

    on<_UploadFromCamera>(
      (event, emit) async {
        emit(state.copyWith(status: FormzStatus.submissionInProgress));
        final response =
            await AppFunctions.pickImage(source: ImageSource.camera);

        response.either(
          (failureMessage) {
            emit(state.copyWith(status: FormzStatus.submissionFailure));

            event.onFailure(failureMessage);
          },
          (photo) {
            emit(
              state.copyWith(
                image: photo.path,
              ),
            );
          },
        );
      },
    );

    on<_DeletePhoto>((event, emit) async {
      emit(
        state.copyWith(
          image: '',
        ),
      );
    });
  }
}
