// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'profile_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ProfileEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) uploadPhoto,
    required TResult Function(ValueChanged<String> onFailure) uploadFromCamera,
    required TResult Function() getUserInfo,
    required TResult Function() deletePhoto,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult? Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult? Function()? getUserInfo,
    TResult? Function()? deletePhoto,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult Function()? getUserInfo,
    TResult Function()? deletePhoto,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UploadPhoto value) uploadPhoto,
    required TResult Function(_UploadFromCamera value) uploadFromCamera,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_DeletePhoto value) deletePhoto,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UploadPhoto value)? uploadPhoto,
    TResult? Function(_UploadFromCamera value)? uploadFromCamera,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_DeletePhoto value)? deletePhoto,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UploadPhoto value)? uploadPhoto,
    TResult Function(_UploadFromCamera value)? uploadFromCamera,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_DeletePhoto value)? deletePhoto,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileEventCopyWith<$Res> {
  factory $ProfileEventCopyWith(
          ProfileEvent value, $Res Function(ProfileEvent) then) =
      _$ProfileEventCopyWithImpl<$Res, ProfileEvent>;
}

/// @nodoc
class _$ProfileEventCopyWithImpl<$Res, $Val extends ProfileEvent>
    implements $ProfileEventCopyWith<$Res> {
  _$ProfileEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$UploadPhotoImplCopyWith<$Res> {
  factory _$$UploadPhotoImplCopyWith(
          _$UploadPhotoImpl value, $Res Function(_$UploadPhotoImpl) then) =
      __$$UploadPhotoImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ValueChanged<String> onFailure});
}

/// @nodoc
class __$$UploadPhotoImplCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res, _$UploadPhotoImpl>
    implements _$$UploadPhotoImplCopyWith<$Res> {
  __$$UploadPhotoImplCopyWithImpl(
      _$UploadPhotoImpl _value, $Res Function(_$UploadPhotoImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? onFailure = null,
  }) {
    return _then(_$UploadPhotoImpl(
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
    ));
  }
}

/// @nodoc

class _$UploadPhotoImpl implements _UploadPhoto {
  const _$UploadPhotoImpl({required this.onFailure});

  @override
  final ValueChanged<String> onFailure;

  @override
  String toString() {
    return 'ProfileEvent.uploadPhoto(onFailure: $onFailure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UploadPhotoImpl &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, onFailure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UploadPhotoImplCopyWith<_$UploadPhotoImpl> get copyWith =>
      __$$UploadPhotoImplCopyWithImpl<_$UploadPhotoImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) uploadPhoto,
    required TResult Function(ValueChanged<String> onFailure) uploadFromCamera,
    required TResult Function() getUserInfo,
    required TResult Function() deletePhoto,
  }) {
    return uploadPhoto(onFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult? Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult? Function()? getUserInfo,
    TResult? Function()? deletePhoto,
  }) {
    return uploadPhoto?.call(onFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult Function()? getUserInfo,
    TResult Function()? deletePhoto,
    required TResult orElse(),
  }) {
    if (uploadPhoto != null) {
      return uploadPhoto(onFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UploadPhoto value) uploadPhoto,
    required TResult Function(_UploadFromCamera value) uploadFromCamera,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_DeletePhoto value) deletePhoto,
  }) {
    return uploadPhoto(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UploadPhoto value)? uploadPhoto,
    TResult? Function(_UploadFromCamera value)? uploadFromCamera,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_DeletePhoto value)? deletePhoto,
  }) {
    return uploadPhoto?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UploadPhoto value)? uploadPhoto,
    TResult Function(_UploadFromCamera value)? uploadFromCamera,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_DeletePhoto value)? deletePhoto,
    required TResult orElse(),
  }) {
    if (uploadPhoto != null) {
      return uploadPhoto(this);
    }
    return orElse();
  }
}

abstract class _UploadPhoto implements ProfileEvent {
  const factory _UploadPhoto({required final ValueChanged<String> onFailure}) =
      _$UploadPhotoImpl;

  ValueChanged<String> get onFailure;
  @JsonKey(ignore: true)
  _$$UploadPhotoImplCopyWith<_$UploadPhotoImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$UploadFromCameraImplCopyWith<$Res> {
  factory _$$UploadFromCameraImplCopyWith(_$UploadFromCameraImpl value,
          $Res Function(_$UploadFromCameraImpl) then) =
      __$$UploadFromCameraImplCopyWithImpl<$Res>;
  @useResult
  $Res call({ValueChanged<String> onFailure});
}

/// @nodoc
class __$$UploadFromCameraImplCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res, _$UploadFromCameraImpl>
    implements _$$UploadFromCameraImplCopyWith<$Res> {
  __$$UploadFromCameraImplCopyWithImpl(_$UploadFromCameraImpl _value,
      $Res Function(_$UploadFromCameraImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? onFailure = null,
  }) {
    return _then(_$UploadFromCameraImpl(
      onFailure: null == onFailure
          ? _value.onFailure
          : onFailure // ignore: cast_nullable_to_non_nullable
              as ValueChanged<String>,
    ));
  }
}

/// @nodoc

class _$UploadFromCameraImpl implements _UploadFromCamera {
  const _$UploadFromCameraImpl({required this.onFailure});

  @override
  final ValueChanged<String> onFailure;

  @override
  String toString() {
    return 'ProfileEvent.uploadFromCamera(onFailure: $onFailure)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UploadFromCameraImpl &&
            (identical(other.onFailure, onFailure) ||
                other.onFailure == onFailure));
  }

  @override
  int get hashCode => Object.hash(runtimeType, onFailure);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$UploadFromCameraImplCopyWith<_$UploadFromCameraImpl> get copyWith =>
      __$$UploadFromCameraImplCopyWithImpl<_$UploadFromCameraImpl>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) uploadPhoto,
    required TResult Function(ValueChanged<String> onFailure) uploadFromCamera,
    required TResult Function() getUserInfo,
    required TResult Function() deletePhoto,
  }) {
    return uploadFromCamera(onFailure);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult? Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult? Function()? getUserInfo,
    TResult? Function()? deletePhoto,
  }) {
    return uploadFromCamera?.call(onFailure);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult Function()? getUserInfo,
    TResult Function()? deletePhoto,
    required TResult orElse(),
  }) {
    if (uploadFromCamera != null) {
      return uploadFromCamera(onFailure);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UploadPhoto value) uploadPhoto,
    required TResult Function(_UploadFromCamera value) uploadFromCamera,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_DeletePhoto value) deletePhoto,
  }) {
    return uploadFromCamera(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UploadPhoto value)? uploadPhoto,
    TResult? Function(_UploadFromCamera value)? uploadFromCamera,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_DeletePhoto value)? deletePhoto,
  }) {
    return uploadFromCamera?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UploadPhoto value)? uploadPhoto,
    TResult Function(_UploadFromCamera value)? uploadFromCamera,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_DeletePhoto value)? deletePhoto,
    required TResult orElse(),
  }) {
    if (uploadFromCamera != null) {
      return uploadFromCamera(this);
    }
    return orElse();
  }
}

abstract class _UploadFromCamera implements ProfileEvent {
  const factory _UploadFromCamera(
      {required final ValueChanged<String> onFailure}) = _$UploadFromCameraImpl;

  ValueChanged<String> get onFailure;
  @JsonKey(ignore: true)
  _$$UploadFromCameraImplCopyWith<_$UploadFromCameraImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$GetUserInfoImplCopyWith<$Res> {
  factory _$$GetUserInfoImplCopyWith(
          _$GetUserInfoImpl value, $Res Function(_$GetUserInfoImpl) then) =
      __$$GetUserInfoImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetUserInfoImplCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res, _$GetUserInfoImpl>
    implements _$$GetUserInfoImplCopyWith<$Res> {
  __$$GetUserInfoImplCopyWithImpl(
      _$GetUserInfoImpl _value, $Res Function(_$GetUserInfoImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetUserInfoImpl implements _GetUserInfo {
  const _$GetUserInfoImpl();

  @override
  String toString() {
    return 'ProfileEvent.getUserInfo()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetUserInfoImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) uploadPhoto,
    required TResult Function(ValueChanged<String> onFailure) uploadFromCamera,
    required TResult Function() getUserInfo,
    required TResult Function() deletePhoto,
  }) {
    return getUserInfo();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult? Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult? Function()? getUserInfo,
    TResult? Function()? deletePhoto,
  }) {
    return getUserInfo?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult Function()? getUserInfo,
    TResult Function()? deletePhoto,
    required TResult orElse(),
  }) {
    if (getUserInfo != null) {
      return getUserInfo();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UploadPhoto value) uploadPhoto,
    required TResult Function(_UploadFromCamera value) uploadFromCamera,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_DeletePhoto value) deletePhoto,
  }) {
    return getUserInfo(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UploadPhoto value)? uploadPhoto,
    TResult? Function(_UploadFromCamera value)? uploadFromCamera,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_DeletePhoto value)? deletePhoto,
  }) {
    return getUserInfo?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UploadPhoto value)? uploadPhoto,
    TResult Function(_UploadFromCamera value)? uploadFromCamera,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_DeletePhoto value)? deletePhoto,
    required TResult orElse(),
  }) {
    if (getUserInfo != null) {
      return getUserInfo(this);
    }
    return orElse();
  }
}

abstract class _GetUserInfo implements ProfileEvent {
  const factory _GetUserInfo() = _$GetUserInfoImpl;
}

/// @nodoc
abstract class _$$DeletePhotoImplCopyWith<$Res> {
  factory _$$DeletePhotoImplCopyWith(
          _$DeletePhotoImpl value, $Res Function(_$DeletePhotoImpl) then) =
      __$$DeletePhotoImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$DeletePhotoImplCopyWithImpl<$Res>
    extends _$ProfileEventCopyWithImpl<$Res, _$DeletePhotoImpl>
    implements _$$DeletePhotoImplCopyWith<$Res> {
  __$$DeletePhotoImplCopyWithImpl(
      _$DeletePhotoImpl _value, $Res Function(_$DeletePhotoImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$DeletePhotoImpl implements _DeletePhoto {
  const _$DeletePhotoImpl();

  @override
  String toString() {
    return 'ProfileEvent.deletePhoto()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$DeletePhotoImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(ValueChanged<String> onFailure) uploadPhoto,
    required TResult Function(ValueChanged<String> onFailure) uploadFromCamera,
    required TResult Function() getUserInfo,
    required TResult Function() deletePhoto,
  }) {
    return deletePhoto();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult? Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult? Function()? getUserInfo,
    TResult? Function()? deletePhoto,
  }) {
    return deletePhoto?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(ValueChanged<String> onFailure)? uploadPhoto,
    TResult Function(ValueChanged<String> onFailure)? uploadFromCamera,
    TResult Function()? getUserInfo,
    TResult Function()? deletePhoto,
    required TResult orElse(),
  }) {
    if (deletePhoto != null) {
      return deletePhoto();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_UploadPhoto value) uploadPhoto,
    required TResult Function(_UploadFromCamera value) uploadFromCamera,
    required TResult Function(_GetUserInfo value) getUserInfo,
    required TResult Function(_DeletePhoto value) deletePhoto,
  }) {
    return deletePhoto(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_UploadPhoto value)? uploadPhoto,
    TResult? Function(_UploadFromCamera value)? uploadFromCamera,
    TResult? Function(_GetUserInfo value)? getUserInfo,
    TResult? Function(_DeletePhoto value)? deletePhoto,
  }) {
    return deletePhoto?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_UploadPhoto value)? uploadPhoto,
    TResult Function(_UploadFromCamera value)? uploadFromCamera,
    TResult Function(_GetUserInfo value)? getUserInfo,
    TResult Function(_DeletePhoto value)? deletePhoto,
    required TResult orElse(),
  }) {
    if (deletePhoto != null) {
      return deletePhoto(this);
    }
    return orElse();
  }
}

abstract class _DeletePhoto implements ProfileEvent {
  const factory _DeletePhoto() = _$DeletePhotoImpl;
}

/// @nodoc
mixin _$ProfileState {
  FormzStatus get status => throw _privateConstructorUsedError;
  String get image => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ProfileStateCopyWith<ProfileState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ProfileStateCopyWith<$Res> {
  factory $ProfileStateCopyWith(
          ProfileState value, $Res Function(ProfileState) then) =
      _$ProfileStateCopyWithImpl<$Res, ProfileState>;
  @useResult
  $Res call({FormzStatus status, String image});
}

/// @nodoc
class _$ProfileStateCopyWithImpl<$Res, $Val extends ProfileState>
    implements $ProfileStateCopyWith<$Res> {
  _$ProfileStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? image = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ProfileStateImplCopyWith<$Res>
    implements $ProfileStateCopyWith<$Res> {
  factory _$$ProfileStateImplCopyWith(
          _$ProfileStateImpl value, $Res Function(_$ProfileStateImpl) then) =
      __$$ProfileStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({FormzStatus status, String image});
}

/// @nodoc
class __$$ProfileStateImplCopyWithImpl<$Res>
    extends _$ProfileStateCopyWithImpl<$Res, _$ProfileStateImpl>
    implements _$$ProfileStateImplCopyWith<$Res> {
  __$$ProfileStateImplCopyWithImpl(
      _$ProfileStateImpl _value, $Res Function(_$ProfileStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? image = null,
  }) {
    return _then(_$ProfileStateImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ProfileStateImpl implements _ProfileState {
  const _$ProfileStateImpl({this.status = FormzStatus.pure, this.image = ''});

  @override
  @JsonKey()
  final FormzStatus status;
  @override
  @JsonKey()
  final String image;

  @override
  String toString() {
    return 'ProfileState(status: $status, image: $image)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ProfileStateImpl &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.image, image) || other.image == image));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status, image);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ProfileStateImplCopyWith<_$ProfileStateImpl> get copyWith =>
      __$$ProfileStateImplCopyWithImpl<_$ProfileStateImpl>(this, _$identity);
}

abstract class _ProfileState implements ProfileState {
  const factory _ProfileState({final FormzStatus status, final String image}) =
      _$ProfileStateImpl;

  @override
  FormzStatus get status;
  @override
  String get image;
  @override
  @JsonKey(ignore: true)
  _$$ProfileStateImplCopyWith<_$ProfileStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
