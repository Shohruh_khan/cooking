part of 'profile_bloc.dart';

@Freezed()
class ProfileState with _$ProfileState {
  const factory ProfileState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default('') String image,
  }) = _ProfileState;
}
