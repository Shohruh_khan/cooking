part of 'profile_bloc.dart';

@Freezed()
class ProfileEvent with _$ProfileEvent {
  const factory ProfileEvent.uploadPhoto({
    required ValueChanged<String> onFailure,
  }) = _UploadPhoto;

  const factory ProfileEvent.uploadFromCamera({
    required ValueChanged<String> onFailure,
  }) = _UploadFromCamera;
  const factory ProfileEvent.getUserInfo() = _GetUserInfo;
  const factory ProfileEvent.deletePhoto() = _DeletePhoto;
}
