import 'dart:io';

import 'package:cooking/core/models/formz/formz_status.dart';
import 'package:cooking/core/widgets/w_button.dart';
import 'package:cooking/core/widgets/w_scale.dart';
import 'package:cooking/features/home/presentation/widgets/navigator.dart';
import 'package:cooking/features/login/presentation/blocs/auth_bloc.dart';
import 'package:cooking/features/login/presentation/login_screen.dart';
import 'package:cooking/features/profile/presentation/widgets/avatar_photo_bottomsheet.dart';
import 'package:cooking/features/profile/presentation/widgets/translate_bottomsheet.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../assets/colors/colors.dart';
import '../../../assets/icons/icons.dart';
import '../../../core/bloc/show_pop_up/show_pop_up_bloc.dart';
import '../../../core/bloc/translation/translation_bloc.dart';
import '../../../core/data/boxes.dart';
import '../../../core/widgets/popus/popups.dart';
import '../../login/presentation/pages/register_screen.dart';
import 'blocs/profile_bloc.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  var tokenId = token.get(tokenKey);
  @override
  void initState() {
    // TODO: implement initState
    context.read<AuthBloc>().add(const AuthEvent.getUserInfo());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    context.read<TranslationBloc>().add(TranslationEvent.changeLanguage(
        languageName:
            EasyLocalization.of(context)?.currentLocale?.languageCode ?? 'en'));
    return BlocProvider(
      create: (context) => ProfileBloc(),
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: primary,
            title: Text('profile'.tr(),
                style: Theme.of(context).textTheme.headlineMedium),
            actions: [
              WScaleAnimation(
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: SvgPicture.asset(
                      AppIcons.edit,
                      height: 20,
                      width: 20,
                    ),
                  ),
                  onTap: () {
                    context.read<ShowPopUpBloc>().add(
                        ShowPopUpEvent.showWarning(text: "cant_change".tr()));
                  })
            ],
          ),
          body: Padding(
            padding: const EdgeInsets.all(20),
            child: BlocBuilder<AuthBloc, AuthState>(
              builder: (context, state) {
                if (state.status == FormzStatus.pure) {
                  context.read<AuthBloc>().add(const AuthEvent.getUserInfo());
                  return const SizedBox();
                } else if (state.status == FormzStatus.submissionSuccess) {
                  return ListView(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          BlocBuilder<ProfileBloc, ProfileState>(
                            builder: (context, state) {
                              return Align(
                                child: Stack(
                                  alignment: AlignmentDirectional.bottomEnd,
                                  children: [
                                    WScaleAnimation(
                                      onTap: () {
                                        showCustomBottomSheet(
                                          context,
                                          (_) => BlocProvider.value(
                                            value: context.read<ProfileBloc>(),
                                            child:
                                                const AvatarPhotoBottomsheet(),
                                          ),
                                        );
                                      },
                                      child: Container(
                                        padding: const EdgeInsets.all(4),
                                        height: 120.h,
                                        width: 120.h,
                                        decoration: BoxDecoration(
                                          image: state.image.isEmpty
                                              ? null
                                              : DecorationImage(
                                                  fit: BoxFit.cover,
                                                  image: FileImage(
                                                      File(state.image)),
                                                ),
                                          shape: BoxShape.circle,
                                        ),
                                        child: state.image.isEmpty
                                            ? Image.asset(
                                                AppIcons.avatarPhoto,
                                                fit: BoxFit.contain,
                                              )
                                            : null,
                                      ),
                                    ),
                                    BlocBuilder<AuthBloc, AuthState>(
                                      builder: (context, state) {
                                        return WScaleAnimation(
                                          onTap: () {
                                            showCustomBottomSheet(
                                              context,
                                              (_) => BlocProvider.value(
                                                value:
                                                    context.read<ProfileBloc>(),
                                                child:
                                                    const AvatarPhotoBottomsheet(),
                                              ),
                                            );
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.fromLTRB(
                                                8, 8, 8, 10),
                                            decoration: const BoxDecoration(
                                              color: primary,
                                              shape: BoxShape.circle,
                                            ),
                                            child: SvgPicture.asset(
                                                AppIcons.changePhoto),
                                          ),
                                        );
                                      },
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                          Column(
                            children: [
                              Text(
                                state.user!.firstName, //ToDO name
                                textAlign: TextAlign.center,
                                style: Theme.of(context)
                                    .textTheme
                                    .titleLarge!
                                    .copyWith(fontSize: 32.sp),
                              ),
                              SizedBox(height: 10.h),
                              Text(
                                state.user!.lastName,
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(fontSize: 30.sp),
                              ),
                            ],
                          ),
                          const SizedBox(),
                        ],
                      ),
                      SizedBox(height: 24.h),
                      Text(
                        '${'email'.tr()}:  ${state.user!.email}',
                        style: Theme.of(context)
                            .textTheme
                            .titleLarge!
                            .copyWith(fontSize: 18.sp),
                      ),
                      SizedBox(height: 10.h),
                      Text(
                        '${'gender'.tr()} :  ${state.user!.gender ? "male".tr() : "female".tr()}',
                        style: Theme.of(context)
                            .textTheme
                            .titleMedium!
                            .copyWith(fontSize: 18.sp),
                      ),
                      SizedBox(height: 10.h),
                      Text(
                        '${"age".tr()}:  ${state.user!.age}',
                        style: Theme.of(context).textTheme.titleLarge!.copyWith(
                              fontSize: 18.sp,
                            ),
                      ),
                      SizedBox(height: 24.h),
                      Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 24.h, horizontal: 20.w),
                        decoration: BoxDecoration(
                          color: white,
                          borderRadius: BorderRadius.circular(14),
                        ),
                        child: WScaleAnimation(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'language'.tr(),
                                    style:
                                        Theme.of(context).textTheme.titleLarge,
                                  ),
                                  BlocBuilder<TranslationBloc,
                                      TranslationState>(
                                    builder: (context, state) {
                                      return Text(
                                        state.language,
                                        style: Theme.of(context)
                                            .textTheme
                                            .labelMedium,
                                      );
                                    },
                                  ),
                                ],
                              ),
                              SvgPicture.asset(AppIcons.arrowRight),
                            ],
                          ),
                          onTap: () {
                            showCustomBottomSheet(
                                context,
                                (_) => BlocProvider.value(
                                    value: context.read<TranslationBloc>(),
                                    child: TranslateBottomsheet()));
                          },
                        ),
                      ),
                      SizedBox(height: 24.h),
                      WButton(
                        color: primary,
                        onTap: () {
                          if (tokenId == null || tokenId == '') {
                            Navigator.of(context, rootNavigator: true)
                                .pushReplacement(
                                    fade(page: const RegisterScreen()));
                          } else {
                            context
                                .read<AuthBloc>()
                                .add(AuthEvent.signOut(onFailure: (value) {
                                  debugPrint("sign out value:   $value");
                                }, onSuccess: (String value) {
                                  Navigator.of(context, rootNavigator: true)
                                      .pushReplacement(
                                          fade(page: const LoginScreen()));
                                }));
                          }
                        },
                        text: (tokenId == null || tokenId == '')
                            ? "sign_up".tr()
                            : "sign_out".tr(),
                        textStyle: Theme.of(context).textTheme.headlineMedium,
                      ),
                    ],
                  );
                } else if (state.status == FormzStatus.submissionInProgress) {
                  return const Center(child: CircularProgressIndicator());
                } else {
                  return Center(child: const Text("error").tr());
                }
              },
            ),
          )),
    );
  }
}
