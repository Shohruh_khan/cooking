import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

import 'ingredients.dart';

part 'recipe.g.dart';

@HiveType(typeId: 0)
class RecipeEntity extends Equatable {
  @HiveField(0)
  final int id;
  @HiveField(1)
  final int readyInMinutes;
  @HiveField(2)
  final String title;
  @HiveField(3)
  final String image;
  @HiveField(4)
  final String instructions;
  @HiveField(5)
  final int healthyScore;
  @HiveField(6)
  final String share;
  @HiveField(7)
  final List<IngredientEntity> ingredients;

  const RecipeEntity({
    required this.id,
    required this.readyInMinutes,
    required this.title,
    required this.image,
    required this.instructions,
    required this.healthyScore,
    required this.share,
    required this.ingredients,
  });
  @override
  List<Object?> get props => [
        id,
        readyInMinutes,
        title,
        image,
        instructions,
        healthyScore,
        share,
        ingredients
      ];
}
