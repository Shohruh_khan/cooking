import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

part 'ingredients.g.dart';

@HiveType(typeId: 2)
class IngredientEntity extends Equatable {
  @HiveField(0)
  final int id;
  @HiveField(1)
  final String aisle;
  @HiveField(2)
  final String image;
  @HiveField(3)
  final String consistency;
  @HiveField(4)
  final String name;
  @HiveField(5)
  final String nameClean;
  @HiveField(6)
  final String original;
  @HiveField(7)
  final String originalName;
  @HiveField(8)
  final double amount;
  @HiveField(9)
  final String unit;
  @HiveField(10)
  final List<String> meta;

  const IngredientEntity({
    required this.id,
    required this.aisle,
    required this.image,
    required this.consistency,
    required this.name,
    required this.nameClean,
    required this.original,
    required this.originalName,
    required this.amount,
    required this.unit,
    required this.meta,
  });

  @override
  List<Object?> get props => [
        id,
        aisle,
        image,
        consistency,
        name,
        nameClean,
        original,
        originalName,
        amount,
        unit,
        meta
      ];
}
