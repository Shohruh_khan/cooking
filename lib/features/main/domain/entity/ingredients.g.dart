// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ingredients.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class IngredientEntityAdapter extends TypeAdapter<IngredientEntity> {
  @override
  final int typeId = 2;

  @override
  IngredientEntity read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return IngredientEntity(
      id: fields[0] as int,
      aisle: fields[1] as String,
      image: fields[2] as String,
      consistency: fields[3] as String,
      name: fields[4] as String,
      nameClean: fields[5] as String,
      original: fields[6] as String,
      originalName: fields[7] as String,
      amount: fields[8] as double,
      unit: fields[9] as String,
      meta: (fields[10] as List).cast<String>(),
    );
  }

  @override
  void write(BinaryWriter writer, IngredientEntity obj) {
    writer
      ..writeByte(11)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.aisle)
      ..writeByte(2)
      ..write(obj.image)
      ..writeByte(3)
      ..write(obj.consistency)
      ..writeByte(4)
      ..write(obj.name)
      ..writeByte(5)
      ..write(obj.nameClean)
      ..writeByte(6)
      ..write(obj.original)
      ..writeByte(7)
      ..write(obj.originalName)
      ..writeByte(8)
      ..write(obj.amount)
      ..writeByte(9)
      ..write(obj.unit)
      ..writeByte(10)
      ..write(obj.meta);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is IngredientEntityAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
