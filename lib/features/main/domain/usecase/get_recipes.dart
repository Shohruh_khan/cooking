import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/main/data/repository/main_repo_impl.dart';
import 'package:cooking/features/main/domain/entity/recipe.dart';
import 'package:cooking/features/main/domain/repository/main_repo.dart';

class GetRecipeUseCase implements UseCase<List<RecipeEntity>, NoParams> {
  final MainRepository _repository = MainRepositoryImpl();

  @override
  Future<Either<Failure, List<RecipeEntity>>> call(NoParams params) async {
    return _repository.getRecipes();
  }
}
