import 'package:cooking/core/error/failure.dart';
import 'package:cooking/features/main/domain/entity/recipe.dart';

import '../../../../core/data/either.dart';

abstract class MainRepository {
  Future<Either<Failure, List<RecipeEntity>>> getRecipes();
}
