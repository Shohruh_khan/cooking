import 'package:cooking/core/error/exeptions.dart';
import 'package:cooking/features/main/data/models/recipe.dart';
import 'package:dio/dio.dart';

import '../../../../assets/constants/constants.dart';

abstract class MainRemoteDataSource {
  Future<List<RecipeModel>> getRecipes();
}

class MainRemoteDataSourceImpl extends MainRemoteDataSource {
  final Dio _dio = Dio();

  @override
  Future<List<RecipeModel>> getRecipes() async {
    final response = await _dio.get(
        "https://api.spoonacular.com/recipes/random?apiKey=$apiKey&number=10");
    if (response.statusCode! >= 200 && response.statusCode! <= 300) {
      return (response.data['recipes'] as List)
          .map((e) => RecipeModel.fromJson(e))
          .toList();
    } else {
      throw ServerException(
          statusMessage: 'Server Failure', statusCode: response.statusCode!);
    }
  }
  // @override
  // Future<List<RecipeModel>> getRecipes() async {
  //   return _getRecipesWithRetry(apiKey, numbers);
  // }
  //
  // Future<List<RecipeModel>> _getRecipesWithRetry(
  //     String currentApiKey, int retries) async {
  //   final response = await _dio.get(
  //       "https://api.spoonacular.com/recipes/random?apiKey=$currentApiKey&number=$numbers");
  //
  //   if (response.statusCode == 402) {
  //     print("inside 404:  $currentApiKey");
  //     print("inside 404:  $retries");
  //     // If 404 and retries left, try with the next API key
  //     if (retries > 0) {
  //       print("inside retries>0 :   $currentApiKey");
  //       print("inside retries>0 :  $retries");
  //
  //       final nextIndex = (keys.indexOf(currentApiKey) + 1) % keys.length;
  //       final nextApiKey = keys[nextIndex];
  //       return _getRecipesWithRetry(nextApiKey, retries - 1);
  //     } else {
  //       // If no retries left, throw an exception
  //       throw ServerException(
  //         statusMessage: 'Server Failure - All API keys failed',
  //         statusCode: response.statusCode!,
  //       );
  //     }
  //   } else if (response.statusCode! >= 200 && response.statusCode! <= 300) {
  //     print("inside 200 :   $currentApiKey");
  //     print("inside 200 :   $retries");
  //     return (response.data['recipes'] as List)
  //         .map((e) => RecipeModel.fromJson(e))
  //         .toList();
  //   } else {
  //     print("ServerException ");
  //     // If the status code is not 404, throw an exception
  //     throw ServerException(
  //       statusMessage: 'Server Failure',
  //       statusCode: response.statusCode!,
  //     );
  //   }
  // }
}
