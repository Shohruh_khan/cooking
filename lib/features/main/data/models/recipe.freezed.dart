// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'recipe.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RecipeModel _$RecipeModelFromJson(Map<String, dynamic> json) {
  return _RecipeModel.fromJson(json);
}

/// @nodoc
mixin _$RecipeModel {
  int get id => throw _privateConstructorUsedError;
  int get readyInMinutes => throw _privateConstructorUsedError;
  String? get title => throw _privateConstructorUsedError;
  String? get image => throw _privateConstructorUsedError;
  String? get instructions => throw _privateConstructorUsedError;
  int get healthScore => throw _privateConstructorUsedError;
  String? get spoonacularSourceUrl => throw _privateConstructorUsedError;
  List<IngredientModel>? get extendedIngredients =>
      throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RecipeModelCopyWith<RecipeModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RecipeModelCopyWith<$Res> {
  factory $RecipeModelCopyWith(
          RecipeModel value, $Res Function(RecipeModel) then) =
      _$RecipeModelCopyWithImpl<$Res, RecipeModel>;
  @useResult
  $Res call(
      {int id,
      int readyInMinutes,
      String? title,
      String? image,
      String? instructions,
      int healthScore,
      String? spoonacularSourceUrl,
      List<IngredientModel>? extendedIngredients});
}

/// @nodoc
class _$RecipeModelCopyWithImpl<$Res, $Val extends RecipeModel>
    implements $RecipeModelCopyWith<$Res> {
  _$RecipeModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? readyInMinutes = null,
    Object? title = freezed,
    Object? image = freezed,
    Object? instructions = freezed,
    Object? healthScore = null,
    Object? spoonacularSourceUrl = freezed,
    Object? extendedIngredients = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      readyInMinutes: null == readyInMinutes
          ? _value.readyInMinutes
          : readyInMinutes // ignore: cast_nullable_to_non_nullable
              as int,
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      instructions: freezed == instructions
          ? _value.instructions
          : instructions // ignore: cast_nullable_to_non_nullable
              as String?,
      healthScore: null == healthScore
          ? _value.healthScore
          : healthScore // ignore: cast_nullable_to_non_nullable
              as int,
      spoonacularSourceUrl: freezed == spoonacularSourceUrl
          ? _value.spoonacularSourceUrl
          : spoonacularSourceUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      extendedIngredients: freezed == extendedIngredients
          ? _value.extendedIngredients
          : extendedIngredients // ignore: cast_nullable_to_non_nullable
              as List<IngredientModel>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RecipeModelImplCopyWith<$Res>
    implements $RecipeModelCopyWith<$Res> {
  factory _$$RecipeModelImplCopyWith(
          _$RecipeModelImpl value, $Res Function(_$RecipeModelImpl) then) =
      __$$RecipeModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      int readyInMinutes,
      String? title,
      String? image,
      String? instructions,
      int healthScore,
      String? spoonacularSourceUrl,
      List<IngredientModel>? extendedIngredients});
}

/// @nodoc
class __$$RecipeModelImplCopyWithImpl<$Res>
    extends _$RecipeModelCopyWithImpl<$Res, _$RecipeModelImpl>
    implements _$$RecipeModelImplCopyWith<$Res> {
  __$$RecipeModelImplCopyWithImpl(
      _$RecipeModelImpl _value, $Res Function(_$RecipeModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? readyInMinutes = null,
    Object? title = freezed,
    Object? image = freezed,
    Object? instructions = freezed,
    Object? healthScore = null,
    Object? spoonacularSourceUrl = freezed,
    Object? extendedIngredients = freezed,
  }) {
    return _then(_$RecipeModelImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      readyInMinutes: null == readyInMinutes
          ? _value.readyInMinutes
          : readyInMinutes // ignore: cast_nullable_to_non_nullable
              as int,
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      image: freezed == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      instructions: freezed == instructions
          ? _value.instructions
          : instructions // ignore: cast_nullable_to_non_nullable
              as String?,
      healthScore: null == healthScore
          ? _value.healthScore
          : healthScore // ignore: cast_nullable_to_non_nullable
              as int,
      spoonacularSourceUrl: freezed == spoonacularSourceUrl
          ? _value.spoonacularSourceUrl
          : spoonacularSourceUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      extendedIngredients: freezed == extendedIngredients
          ? _value._extendedIngredients
          : extendedIngredients // ignore: cast_nullable_to_non_nullable
              as List<IngredientModel>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RecipeModelImpl implements _RecipeModel {
  const _$RecipeModelImpl(
      {required this.id,
      required this.readyInMinutes,
      required this.title,
      required this.image,
      required this.instructions,
      required this.healthScore,
      required this.spoonacularSourceUrl,
      required final List<IngredientModel>? extendedIngredients})
      : _extendedIngredients = extendedIngredients;

  factory _$RecipeModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$RecipeModelImplFromJson(json);

  @override
  final int id;
  @override
  final int readyInMinutes;
  @override
  final String? title;
  @override
  final String? image;
  @override
  final String? instructions;
  @override
  final int healthScore;
  @override
  final String? spoonacularSourceUrl;
  final List<IngredientModel>? _extendedIngredients;
  @override
  List<IngredientModel>? get extendedIngredients {
    final value = _extendedIngredients;
    if (value == null) return null;
    if (_extendedIngredients is EqualUnmodifiableListView)
      return _extendedIngredients;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'RecipeModel(id: $id, readyInMinutes: $readyInMinutes, title: $title, image: $image, instructions: $instructions, healthScore: $healthScore, spoonacularSourceUrl: $spoonacularSourceUrl, extendedIngredients: $extendedIngredients)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RecipeModelImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.readyInMinutes, readyInMinutes) ||
                other.readyInMinutes == readyInMinutes) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.image, image) || other.image == image) &&
            (identical(other.instructions, instructions) ||
                other.instructions == instructions) &&
            (identical(other.healthScore, healthScore) ||
                other.healthScore == healthScore) &&
            (identical(other.spoonacularSourceUrl, spoonacularSourceUrl) ||
                other.spoonacularSourceUrl == spoonacularSourceUrl) &&
            const DeepCollectionEquality()
                .equals(other._extendedIngredients, _extendedIngredients));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      readyInMinutes,
      title,
      image,
      instructions,
      healthScore,
      spoonacularSourceUrl,
      const DeepCollectionEquality().hash(_extendedIngredients));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RecipeModelImplCopyWith<_$RecipeModelImpl> get copyWith =>
      __$$RecipeModelImplCopyWithImpl<_$RecipeModelImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RecipeModelImplToJson(
      this,
    );
  }
}

abstract class _RecipeModel implements RecipeModel {
  const factory _RecipeModel(
          {required final int id,
          required final int readyInMinutes,
          required final String? title,
          required final String? image,
          required final String? instructions,
          required final int healthScore,
          required final String? spoonacularSourceUrl,
          required final List<IngredientModel>? extendedIngredients}) =
      _$RecipeModelImpl;

  factory _RecipeModel.fromJson(Map<String, dynamic> json) =
      _$RecipeModelImpl.fromJson;

  @override
  int get id;
  @override
  int get readyInMinutes;
  @override
  String? get title;
  @override
  String? get image;
  @override
  String? get instructions;
  @override
  int get healthScore;
  @override
  String? get spoonacularSourceUrl;
  @override
  List<IngredientModel>? get extendedIngredients;
  @override
  @JsonKey(ignore: true)
  _$$RecipeModelImplCopyWith<_$RecipeModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
