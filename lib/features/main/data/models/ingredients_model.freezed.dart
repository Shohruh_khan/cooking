// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'ingredients_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

IngredientModel _$IngredientModelFromJson(Map<String, dynamic> json) {
  return _IngredientModel.fromJson(json);
}

/// @nodoc
mixin _$IngredientModel {
  int get id => throw _privateConstructorUsedError;
  String get aisle => throw _privateConstructorUsedError;
  String get image => throw _privateConstructorUsedError;
  String get consistency => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get nameClean => throw _privateConstructorUsedError;
  String get original => throw _privateConstructorUsedError;
  String get originalName => throw _privateConstructorUsedError;
  double get amount => throw _privateConstructorUsedError;
  String get unit => throw _privateConstructorUsedError;
  List<String> get meta => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $IngredientModelCopyWith<IngredientModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $IngredientModelCopyWith<$Res> {
  factory $IngredientModelCopyWith(
          IngredientModel value, $Res Function(IngredientModel) then) =
      _$IngredientModelCopyWithImpl<$Res, IngredientModel>;
  @useResult
  $Res call(
      {int id,
      String aisle,
      String image,
      String consistency,
      String name,
      String nameClean,
      String original,
      String originalName,
      double amount,
      String unit,
      List<String> meta});
}

/// @nodoc
class _$IngredientModelCopyWithImpl<$Res, $Val extends IngredientModel>
    implements $IngredientModelCopyWith<$Res> {
  _$IngredientModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? aisle = null,
    Object? image = null,
    Object? consistency = null,
    Object? name = null,
    Object? nameClean = null,
    Object? original = null,
    Object? originalName = null,
    Object? amount = null,
    Object? unit = null,
    Object? meta = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      aisle: null == aisle
          ? _value.aisle
          : aisle // ignore: cast_nullable_to_non_nullable
              as String,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      consistency: null == consistency
          ? _value.consistency
          : consistency // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      nameClean: null == nameClean
          ? _value.nameClean
          : nameClean // ignore: cast_nullable_to_non_nullable
              as String,
      original: null == original
          ? _value.original
          : original // ignore: cast_nullable_to_non_nullable
              as String,
      originalName: null == originalName
          ? _value.originalName
          : originalName // ignore: cast_nullable_to_non_nullable
              as String,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as double,
      unit: null == unit
          ? _value.unit
          : unit // ignore: cast_nullable_to_non_nullable
              as String,
      meta: null == meta
          ? _value.meta
          : meta // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$IngredientModelImplCopyWith<$Res>
    implements $IngredientModelCopyWith<$Res> {
  factory _$$IngredientModelImplCopyWith(_$IngredientModelImpl value,
          $Res Function(_$IngredientModelImpl) then) =
      __$$IngredientModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String aisle,
      String image,
      String consistency,
      String name,
      String nameClean,
      String original,
      String originalName,
      double amount,
      String unit,
      List<String> meta});
}

/// @nodoc
class __$$IngredientModelImplCopyWithImpl<$Res>
    extends _$IngredientModelCopyWithImpl<$Res, _$IngredientModelImpl>
    implements _$$IngredientModelImplCopyWith<$Res> {
  __$$IngredientModelImplCopyWithImpl(
      _$IngredientModelImpl _value, $Res Function(_$IngredientModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? aisle = null,
    Object? image = null,
    Object? consistency = null,
    Object? name = null,
    Object? nameClean = null,
    Object? original = null,
    Object? originalName = null,
    Object? amount = null,
    Object? unit = null,
    Object? meta = null,
  }) {
    return _then(_$IngredientModelImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      aisle: null == aisle
          ? _value.aisle
          : aisle // ignore: cast_nullable_to_non_nullable
              as String,
      image: null == image
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String,
      consistency: null == consistency
          ? _value.consistency
          : consistency // ignore: cast_nullable_to_non_nullable
              as String,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      nameClean: null == nameClean
          ? _value.nameClean
          : nameClean // ignore: cast_nullable_to_non_nullable
              as String,
      original: null == original
          ? _value.original
          : original // ignore: cast_nullable_to_non_nullable
              as String,
      originalName: null == originalName
          ? _value.originalName
          : originalName // ignore: cast_nullable_to_non_nullable
              as String,
      amount: null == amount
          ? _value.amount
          : amount // ignore: cast_nullable_to_non_nullable
              as double,
      unit: null == unit
          ? _value.unit
          : unit // ignore: cast_nullable_to_non_nullable
              as String,
      meta: null == meta
          ? _value._meta
          : meta // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$IngredientModelImpl implements _IngredientModel {
  const _$IngredientModelImpl(
      {required this.id,
      required this.aisle,
      required this.image,
      required this.consistency,
      required this.name,
      required this.nameClean,
      required this.original,
      required this.originalName,
      required this.amount,
      required this.unit,
      required final List<String> meta})
      : _meta = meta;

  factory _$IngredientModelImpl.fromJson(Map<String, dynamic> json) =>
      _$$IngredientModelImplFromJson(json);

  @override
  final int id;
  @override
  final String aisle;
  @override
  final String image;
  @override
  final String consistency;
  @override
  final String name;
  @override
  final String nameClean;
  @override
  final String original;
  @override
  final String originalName;
  @override
  final double amount;
  @override
  final String unit;
  final List<String> _meta;
  @override
  List<String> get meta {
    if (_meta is EqualUnmodifiableListView) return _meta;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_meta);
  }

  @override
  String toString() {
    return 'IngredientModel(id: $id, aisle: $aisle, image: $image, consistency: $consistency, name: $name, nameClean: $nameClean, original: $original, originalName: $originalName, amount: $amount, unit: $unit, meta: $meta)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$IngredientModelImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.aisle, aisle) || other.aisle == aisle) &&
            (identical(other.image, image) || other.image == image) &&
            (identical(other.consistency, consistency) ||
                other.consistency == consistency) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.nameClean, nameClean) ||
                other.nameClean == nameClean) &&
            (identical(other.original, original) ||
                other.original == original) &&
            (identical(other.originalName, originalName) ||
                other.originalName == originalName) &&
            (identical(other.amount, amount) || other.amount == amount) &&
            (identical(other.unit, unit) || other.unit == unit) &&
            const DeepCollectionEquality().equals(other._meta, _meta));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      aisle,
      image,
      consistency,
      name,
      nameClean,
      original,
      originalName,
      amount,
      unit,
      const DeepCollectionEquality().hash(_meta));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$IngredientModelImplCopyWith<_$IngredientModelImpl> get copyWith =>
      __$$IngredientModelImplCopyWithImpl<_$IngredientModelImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$IngredientModelImplToJson(
      this,
    );
  }
}

abstract class _IngredientModel implements IngredientModel {
  const factory _IngredientModel(
      {required final int id,
      required final String aisle,
      required final String image,
      required final String consistency,
      required final String name,
      required final String nameClean,
      required final String original,
      required final String originalName,
      required final double amount,
      required final String unit,
      required final List<String> meta}) = _$IngredientModelImpl;

  factory _IngredientModel.fromJson(Map<String, dynamic> json) =
      _$IngredientModelImpl.fromJson;

  @override
  int get id;
  @override
  String get aisle;
  @override
  String get image;
  @override
  String get consistency;
  @override
  String get name;
  @override
  String get nameClean;
  @override
  String get original;
  @override
  String get originalName;
  @override
  double get amount;
  @override
  String get unit;
  @override
  List<String> get meta;
  @override
  @JsonKey(ignore: true)
  _$$IngredientModelImplCopyWith<_$IngredientModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
