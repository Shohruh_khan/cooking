// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'recipe.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RecipeModelImpl _$$RecipeModelImplFromJson(Map<String, dynamic> json) =>
    _$RecipeModelImpl(
      id: json['id'] as int,
      readyInMinutes: json['readyInMinutes'] as int,
      title: json['title'] as String?,
      image: json['image'] as String?,
      instructions: json['instructions'] as String?,
      healthScore: json['healthScore'] as int,
      spoonacularSourceUrl: json['spoonacularSourceUrl'] as String?,
      extendedIngredients: (json['extendedIngredients'] as List<dynamic>?)
          ?.map((e) => IngredientModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$RecipeModelImplToJson(_$RecipeModelImpl instance) =>
    <String, dynamic>{
      'id': instance.id,
      'readyInMinutes': instance.readyInMinutes,
      'title': instance.title,
      'image': instance.image,
      'instructions': instance.instructions,
      'healthScore': instance.healthScore,
      'spoonacularSourceUrl': instance.spoonacularSourceUrl,
      'extendedIngredients': instance.extendedIngredients,
    };
