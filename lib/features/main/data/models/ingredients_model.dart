import 'package:freezed_annotation/freezed_annotation.dart';

part 'ingredients_model.freezed.dart';
part 'ingredients_model.g.dart';

@freezed
class IngredientModel with _$IngredientModel {
  const factory IngredientModel({
    required int id,
    required String aisle,
    required String image,
    required String consistency,
    required String name,
    required String nameClean,
    required String original,
    required String originalName,
    required double amount,
    required String unit,
    required List<String> meta,
  }) = _IngredientModel;

  factory IngredientModel.fromJson(Map<String, dynamic> json) =>
      _$IngredientModelFromJson(json);
}
