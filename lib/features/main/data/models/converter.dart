import '../../domain/entity/ingredients.dart';
import '../../domain/entity/recipe.dart';
import 'ingredients_model.dart';
import 'recipe.dart';

abstract class MainConverter {
  static List<RecipeEntity> recipeModelsToEntities(List<RecipeModel> models) {
    return models
        .map((model) => RecipeEntity(
            id: model.id,
            readyInMinutes: model.readyInMinutes,
            title: model.title ?? '',
            image: model.image ?? '',
            instructions: model.instructions ?? '',
            healthyScore: model.healthScore,
            share: model.spoonacularSourceUrl ?? '',
            ingredients: ingredientModelsToEntities(model.extendedIngredients)))
        .toList();
  }

  static RecipeEntity recipeModelToEntity(RecipeModel model) {
    return RecipeEntity(
        id: model.id,
        readyInMinutes: model.readyInMinutes,
        title: model.title ?? '',
        image: model.image ?? '',
        instructions: model.instructions ?? '',
        healthyScore: model.healthScore,
        share: model.spoonacularSourceUrl ?? '',
        ingredients: ingredientModelsToEntities(model.extendedIngredients));
  }

  static List<IngredientEntity> ingredientModelsToEntities(
      List<IngredientModel>? models) {
    return models
            ?.map((model) => IngredientEntity(
                  id: model.id,
                  aisle: model.aisle ?? '',
                  image: model.image ?? '',
                  consistency: model.consistency ?? '',
                  name: model.name ?? '',
                  nameClean: model.nameClean ?? '',
                  original: model.original ?? '',
                  originalName: model.originalName ?? '',
                  amount: model.amount,
                  unit: model.unit ?? '',
                  meta: model.meta ?? [],
                ))
            ?.toList() ??
        [];
  }
}
