import 'package:cooking/core/data/either.dart';
import 'package:cooking/core/data/network_info.dart';
import 'package:cooking/core/error/failure.dart';
import 'package:cooking/features/main/data/datasource/remote_data_source.dart';
import 'package:cooking/features/main/domain/entity/recipe.dart';
import 'package:cooking/features/main/domain/repository/main_repo.dart';

import '../../../../core/error/exeptions.dart';
import '../models/converter.dart';

class MainRepositoryImpl extends MainRepository {
  final NetworkInfo _networkInfo = const NetworkInfoImpl();
  final MainRemoteDataSource _remoteDataSource = MainRemoteDataSourceImpl();
  final List<RecipeEntity> favoriteRecipes =
      []; // List to store favorite recipes

  @override
  Future<Either<Failure, List<RecipeEntity>>> getRecipes() async {
    if (await _networkInfo.connected) {
      try {
        final data = await _remoteDataSource.getRecipes();
        return Right(MainConverter.recipeModelsToEntities(data));
      } on ServerException {
        rethrow;
      } catch (e) {
        return Left(ServerFailure(errorMessage: e.toString()));
      }
    } else {
      return Left(const ServerFailure(errorMessage: "No Internet"));
    }
  }
}
