import 'package:cooking/assets/colors/colors.dart';
import 'package:cooking/core/widgets/pagination_loader.dart';
import 'package:cooking/features/main/presentation/pages/recipe_instruction.dart';
import 'package:cooking/features/main/presentation/widgets/recipe_menu.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:lottie/lottie.dart';

import '../../../assets/animations/animations.dart';
import '../../../core/models/formz/formz_status.dart';
import '../../home/presentation/widgets/navigator.dart';
import 'blocs/main_bloc.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({Key? key, this.scrollController}) : super(key: key);
  final ScrollController? scrollController;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MainBloc(),
      child: Builder(builder: (context) {
        return Scaffold(
          backgroundColor: Colors.white38,
          appBar: AppBar(
            backgroundColor: primary,
            title: Text(
              "main_screen".tr(),
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ),
          body: RefreshIndicator(
            color: green,
            onRefresh: () async {
              context.read<MainBloc>().add(const MainEvent.getRecipes());
            },
            child: BlocBuilder<MainBloc, MainState>(
              builder: (context, state) {
                if (state.status == FormzStatus.pure) {
                  context.read<MainBloc>().add(const MainEvent.getRecipes());
                  return const SizedBox();
                } else if (state.status == FormzStatus.submissionInProgress) {
                  return Center(
                      child: Lottie.asset(
                    AppAnimations.cook,
                    height: 200,
                  ));
                } else if (state.status == FormzStatus.submissionSuccess) {
                  return PaginationLoader(
                    padding:
                        EdgeInsets.symmetric(vertical: 16.h, horizontal: 12.w),
                    seperatorHeight: 16.h,
                    list: state.entities
                        .map(
                          (data) => GestureDetector(
                            onTap: () {
                              Navigator.of(context, rootNavigator: true)
                                  .push(fade(
                                      page: BlocProvider.value(
                                value: context.read<MainBloc>(),
                                child: RecipeInstruction(
                                  data: data,
                                ),
                              )));
                            },
                            child: RecipesMenu(
                              data: data,
                            ),
                          ),
                        )
                        .toList(),
                    onLoadMore: () {
                      context
                          .read<MainBloc>()
                          .add(const MainEvent.addRecipes());
                    },
                    isFailedToLoad: false,
                  );
                } else {
                  return Center(
                    child: SingleChildScrollView(
                      physics: const AlwaysScrollableScrollPhysics(),
                      child: SizedBox(
                        height: MediaQuery.of(context).size.height - 152,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Lottie.asset(
                              AppAnimations.error,
                              height: 250.h,
                            ),
                            Text(
                              "Error",
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }
              },
            ),
          ),
        );
      }),
    );
  }
}
