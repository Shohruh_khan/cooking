import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../assets/icons/icons.dart';

class PopUpButton extends StatelessWidget {
  const PopUpButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
        icon: SvgPicture.asset(AppIcons.verticalDots),
        onSelected: (value) {
          if (value == 'share') {
            // Handle share button pressed
            // Add your share functionality here
          } else if (value == 'edit') {
            // Handle edit button pressed
            // Add your edit functionality here
          } else if (value == 'like') {
            // Handle like button pressed
            // Add your like functionality here
          }
        },
        itemBuilder: (BuildContext context) => const [
              PopupMenuItem<String>(
                value: 'share',
                child: Text('Share'),
              ),
              PopupMenuItem<String>(
                value: 'edit',
                child: Text('Edit'),
              ),
              PopupMenuItem<String>(
                value: 'like',
                child: Text('Like'),
              ),
            ]);
  }
}
