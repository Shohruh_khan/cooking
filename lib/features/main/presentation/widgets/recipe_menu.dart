import 'package:cached_network_image/cached_network_image.dart';
import 'package:cooking/core/widgets/w_scale.dart';
import 'package:cooking/features/main/domain/entity/recipe.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../assets/icons/icons.dart';
import '../../../../core/bloc/show_pop_up/show_pop_up_bloc.dart';
import '../../../favorite/presentation/blocs/favorite_bloc.dart';

class RecipesMenu extends StatelessWidget {
  const RecipesMenu({
    super.key,
    required this.data,
  });

  final RecipeEntity data;
  final isFavorite = false;
  @override
  Widget build(BuildContext context) {
    return Slidable(
      // startActionPane: SlidableScrollActionPane(),
      endActionPane: ActionPane(
        extentRatio: 0.2,
        // closeThreshold: 0.1,
        motion: const ScrollMotion(),
        children: [
          BlocBuilder<FavoriteBloc, FavoriteState>(
            builder: (context, state) {
              return WScaleAnimation(
                child: Padding(
                  padding: EdgeInsets.only(left: 24.w),
                  child: SvgPicture.asset(
                    state.entities.contains(data)
                        ? AppIcons.trash
                        : AppIcons.plus,
                    height: 32.h,
                    width: 32.w,
                  ),
                ),
                onTap: () {
                  if (state.entities.contains(data)) {
                    context
                        .read<FavoriteBloc>()
                        .add(FavoriteEvent.removeRecipe(data));
                    context.read<ShowPopUpBloc>().add(
                        ShowPopUpEvent.showSuccess(
                            text: "removed_wishlist".tr()));
                  } else {
                    context
                        .read<FavoriteBloc>()
                        .add(FavoriteEvent.addRecipe(data));

                    context.read<ShowPopUpBloc>().add(
                        ShowPopUpEvent.showSuccess(
                            text: "added_wishlist".tr()));
                  }
                },
              );
            },
          ),
        ],
      ),
      child: Container(
        padding: const EdgeInsets.fromLTRB(0, 0, 12, 0),
        height: 130.h,
        decoration: BoxDecoration(
          color: Colors.white54,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Row(
          children: [
            SizedBox(
              height: double.infinity,
              width: 120.w,
              child: CachedNetworkImage(
                // color: black,
                // width: 50.w,
                // height: 75.h,
                imageUrl: data.image,
                fit: BoxFit.cover,
                errorWidget: (_, __, e) => Image.asset(
                  AppIcons.defaultRecipe,
                  // width: 50.w,
                  fit: BoxFit.fill,
                  // height: 75.h,
                ),
                placeholder: (context, url) => Image.asset(
                  AppIcons.defaultRecipe,
                  fit: BoxFit.fill,
                  // width: 50.w,
                  // height: 75.h,
                ),
              ),
            ),
            SizedBox(width: 6.w),
            Expanded(
              child: Column(
                children: [
                  SizedBox(height: 12.h),
                  Text(
                    data.title,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    maxLines: 3,
                    style: Theme.of(context).textTheme.labelMedium,
                  ),
                  const Spacer(),
                  Row(
                    children: [
                      SizedBox(width: 10.w),
                      SvgPicture.asset(
                        AppIcons.time,
                        height: 24.h,
                      ),
                      Text(
                        '${data.readyInMinutes} min',
                        style: Theme.of(context)
                            .textTheme
                            .labelSmall!
                            .copyWith(fontSize: 14.sp),
                      ),
                      const Spacer(),
                      Text(
                        data.healthyScore.toString(),
                        style: Theme.of(context)
                            .textTheme
                            .labelSmall!
                            .copyWith(fontSize: 14.sp),
                      ),
                      SvgPicture.asset(
                        AppIcons.health,
                        height: 18.h,
                      ),
                    ],
                  ),
                  SizedBox(height: 12.h),
                ],
              ),
            ),
            //   )
            // ],
          ],
        ),
      ),
    );
  }
}
