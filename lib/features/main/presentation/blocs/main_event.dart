part of 'main_bloc.dart';

@freezed
class MainEvent with _$MainEvent {
  const factory MainEvent.getRecipes() = _GetRecipes;
  const factory MainEvent.addRecipes() = _AddRecipes;
  const factory MainEvent.getGrid({required bool isGrid}) = _GetGrid;
}
