import 'package:bloc/bloc.dart';
import 'package:cooking/core/usecase/usecase.dart';
import 'package:cooking/features/main/domain/usecase/get_recipes.dart';
import 'package:flutter/cupertino.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../core/models/formz/formz_status.dart';
import '../../domain/entity/recipe.dart';

part 'main_bloc.freezed.dart';
part 'main_event.dart';
part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  MainBloc() : super(const _MainState()) {
    on<_GetRecipes>((event, emit) async {
      emit(state.copyWith(status: FormzStatus.submissionInProgress));
      final useCase = GetRecipeUseCase();
      final result = await useCase.call(NoParams());
      result.either((error) {
        emit(state.copyWith(
            status: FormzStatus.submissionFailure,
            errorMessage: error.errorMessage));
        debugPrint('failure error: ${error.errorMessage}');
      }, (data) {
        debugPrint("Success congrats");
        emit(state.copyWith(
            entities: data, status: FormzStatus.submissionSuccess));
      });
    });

    on<_AddRecipes>(
      (event, emit) async {
        final usecase = GetRecipeUseCase();
        final result = await usecase.call(NoParams());
        result.either((error) {
          emit(state.copyWith(
              status: FormzStatus.submissionFailure,
              errorMessage: error.errorMessage));
        }, (data) {
          emit(state.copyWith(entities: state.entities + data));
        });
      },
    );

    on<_GetGrid>((event, emit) {
      emit(state.copyWith(isGrid: event.isGrid));
    });
  }
}
