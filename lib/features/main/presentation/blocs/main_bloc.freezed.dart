// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'main_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MainEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getRecipes,
    required TResult Function() addRecipes,
    required TResult Function(bool isGrid) getGrid,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getRecipes,
    TResult? Function()? addRecipes,
    TResult? Function(bool isGrid)? getGrid,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getRecipes,
    TResult Function()? addRecipes,
    TResult Function(bool isGrid)? getGrid,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetRecipes value) getRecipes,
    required TResult Function(_AddRecipes value) addRecipes,
    required TResult Function(_GetGrid value) getGrid,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetRecipes value)? getRecipes,
    TResult? Function(_AddRecipes value)? addRecipes,
    TResult? Function(_GetGrid value)? getGrid,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetRecipes value)? getRecipes,
    TResult Function(_AddRecipes value)? addRecipes,
    TResult Function(_GetGrid value)? getGrid,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MainEventCopyWith<$Res> {
  factory $MainEventCopyWith(MainEvent value, $Res Function(MainEvent) then) =
      _$MainEventCopyWithImpl<$Res, MainEvent>;
}

/// @nodoc
class _$MainEventCopyWithImpl<$Res, $Val extends MainEvent>
    implements $MainEventCopyWith<$Res> {
  _$MainEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GetRecipesImplCopyWith<$Res> {
  factory _$$GetRecipesImplCopyWith(
          _$GetRecipesImpl value, $Res Function(_$GetRecipesImpl) then) =
      __$$GetRecipesImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetRecipesImplCopyWithImpl<$Res>
    extends _$MainEventCopyWithImpl<$Res, _$GetRecipesImpl>
    implements _$$GetRecipesImplCopyWith<$Res> {
  __$$GetRecipesImplCopyWithImpl(
      _$GetRecipesImpl _value, $Res Function(_$GetRecipesImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetRecipesImpl implements _GetRecipes {
  const _$GetRecipesImpl();

  @override
  String toString() {
    return 'MainEvent.getRecipes()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetRecipesImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getRecipes,
    required TResult Function() addRecipes,
    required TResult Function(bool isGrid) getGrid,
  }) {
    return getRecipes();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getRecipes,
    TResult? Function()? addRecipes,
    TResult? Function(bool isGrid)? getGrid,
  }) {
    return getRecipes?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getRecipes,
    TResult Function()? addRecipes,
    TResult Function(bool isGrid)? getGrid,
    required TResult orElse(),
  }) {
    if (getRecipes != null) {
      return getRecipes();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetRecipes value) getRecipes,
    required TResult Function(_AddRecipes value) addRecipes,
    required TResult Function(_GetGrid value) getGrid,
  }) {
    return getRecipes(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetRecipes value)? getRecipes,
    TResult? Function(_AddRecipes value)? addRecipes,
    TResult? Function(_GetGrid value)? getGrid,
  }) {
    return getRecipes?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetRecipes value)? getRecipes,
    TResult Function(_AddRecipes value)? addRecipes,
    TResult Function(_GetGrid value)? getGrid,
    required TResult orElse(),
  }) {
    if (getRecipes != null) {
      return getRecipes(this);
    }
    return orElse();
  }
}

abstract class _GetRecipes implements MainEvent {
  const factory _GetRecipes() = _$GetRecipesImpl;
}

/// @nodoc
abstract class _$$AddRecipesImplCopyWith<$Res> {
  factory _$$AddRecipesImplCopyWith(
          _$AddRecipesImpl value, $Res Function(_$AddRecipesImpl) then) =
      __$$AddRecipesImplCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AddRecipesImplCopyWithImpl<$Res>
    extends _$MainEventCopyWithImpl<$Res, _$AddRecipesImpl>
    implements _$$AddRecipesImplCopyWith<$Res> {
  __$$AddRecipesImplCopyWithImpl(
      _$AddRecipesImpl _value, $Res Function(_$AddRecipesImpl) _then)
      : super(_value, _then);
}

/// @nodoc

class _$AddRecipesImpl implements _AddRecipes {
  const _$AddRecipesImpl();

  @override
  String toString() {
    return 'MainEvent.addRecipes()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$AddRecipesImpl);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getRecipes,
    required TResult Function() addRecipes,
    required TResult Function(bool isGrid) getGrid,
  }) {
    return addRecipes();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getRecipes,
    TResult? Function()? addRecipes,
    TResult? Function(bool isGrid)? getGrid,
  }) {
    return addRecipes?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getRecipes,
    TResult Function()? addRecipes,
    TResult Function(bool isGrid)? getGrid,
    required TResult orElse(),
  }) {
    if (addRecipes != null) {
      return addRecipes();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetRecipes value) getRecipes,
    required TResult Function(_AddRecipes value) addRecipes,
    required TResult Function(_GetGrid value) getGrid,
  }) {
    return addRecipes(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetRecipes value)? getRecipes,
    TResult? Function(_AddRecipes value)? addRecipes,
    TResult? Function(_GetGrid value)? getGrid,
  }) {
    return addRecipes?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetRecipes value)? getRecipes,
    TResult Function(_AddRecipes value)? addRecipes,
    TResult Function(_GetGrid value)? getGrid,
    required TResult orElse(),
  }) {
    if (addRecipes != null) {
      return addRecipes(this);
    }
    return orElse();
  }
}

abstract class _AddRecipes implements MainEvent {
  const factory _AddRecipes() = _$AddRecipesImpl;
}

/// @nodoc
abstract class _$$GetGridImplCopyWith<$Res> {
  factory _$$GetGridImplCopyWith(
          _$GetGridImpl value, $Res Function(_$GetGridImpl) then) =
      __$$GetGridImplCopyWithImpl<$Res>;
  @useResult
  $Res call({bool isGrid});
}

/// @nodoc
class __$$GetGridImplCopyWithImpl<$Res>
    extends _$MainEventCopyWithImpl<$Res, _$GetGridImpl>
    implements _$$GetGridImplCopyWith<$Res> {
  __$$GetGridImplCopyWithImpl(
      _$GetGridImpl _value, $Res Function(_$GetGridImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? isGrid = null,
  }) {
    return _then(_$GetGridImpl(
      isGrid: null == isGrid
          ? _value.isGrid
          : isGrid // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$GetGridImpl implements _GetGrid {
  const _$GetGridImpl({required this.isGrid});

  @override
  final bool isGrid;

  @override
  String toString() {
    return 'MainEvent.getGrid(isGrid: $isGrid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GetGridImpl &&
            (identical(other.isGrid, isGrid) || other.isGrid == isGrid));
  }

  @override
  int get hashCode => Object.hash(runtimeType, isGrid);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GetGridImplCopyWith<_$GetGridImpl> get copyWith =>
      __$$GetGridImplCopyWithImpl<_$GetGridImpl>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getRecipes,
    required TResult Function() addRecipes,
    required TResult Function(bool isGrid) getGrid,
  }) {
    return getGrid(isGrid);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getRecipes,
    TResult? Function()? addRecipes,
    TResult? Function(bool isGrid)? getGrid,
  }) {
    return getGrid?.call(isGrid);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getRecipes,
    TResult Function()? addRecipes,
    TResult Function(bool isGrid)? getGrid,
    required TResult orElse(),
  }) {
    if (getGrid != null) {
      return getGrid(isGrid);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_GetRecipes value) getRecipes,
    required TResult Function(_AddRecipes value) addRecipes,
    required TResult Function(_GetGrid value) getGrid,
  }) {
    return getGrid(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_GetRecipes value)? getRecipes,
    TResult? Function(_AddRecipes value)? addRecipes,
    TResult? Function(_GetGrid value)? getGrid,
  }) {
    return getGrid?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_GetRecipes value)? getRecipes,
    TResult Function(_AddRecipes value)? addRecipes,
    TResult Function(_GetGrid value)? getGrid,
    required TResult orElse(),
  }) {
    if (getGrid != null) {
      return getGrid(this);
    }
    return orElse();
  }
}

abstract class _GetGrid implements MainEvent {
  const factory _GetGrid({required final bool isGrid}) = _$GetGridImpl;

  bool get isGrid;
  @JsonKey(ignore: true)
  _$$GetGridImplCopyWith<_$GetGridImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$MainState {
  FormzStatus get status => throw _privateConstructorUsedError;
  List<RecipeEntity> get entities => throw _privateConstructorUsedError;
  String get errorMessage => throw _privateConstructorUsedError;
  bool get isGrid => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MainStateCopyWith<MainState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MainStateCopyWith<$Res> {
  factory $MainStateCopyWith(MainState value, $Res Function(MainState) then) =
      _$MainStateCopyWithImpl<$Res, MainState>;
  @useResult
  $Res call(
      {FormzStatus status,
      List<RecipeEntity> entities,
      String errorMessage,
      bool isGrid});
}

/// @nodoc
class _$MainStateCopyWithImpl<$Res, $Val extends MainState>
    implements $MainStateCopyWith<$Res> {
  _$MainStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? entities = null,
    Object? errorMessage = null,
    Object? isGrid = null,
  }) {
    return _then(_value.copyWith(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      entities: null == entities
          ? _value.entities
          : entities // ignore: cast_nullable_to_non_nullable
              as List<RecipeEntity>,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
      isGrid: null == isGrid
          ? _value.isGrid
          : isGrid // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$MainStateImplCopyWith<$Res>
    implements $MainStateCopyWith<$Res> {
  factory _$$MainStateImplCopyWith(
          _$MainStateImpl value, $Res Function(_$MainStateImpl) then) =
      __$$MainStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {FormzStatus status,
      List<RecipeEntity> entities,
      String errorMessage,
      bool isGrid});
}

/// @nodoc
class __$$MainStateImplCopyWithImpl<$Res>
    extends _$MainStateCopyWithImpl<$Res, _$MainStateImpl>
    implements _$$MainStateImplCopyWith<$Res> {
  __$$MainStateImplCopyWithImpl(
      _$MainStateImpl _value, $Res Function(_$MainStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = null,
    Object? entities = null,
    Object? errorMessage = null,
    Object? isGrid = null,
  }) {
    return _then(_$MainStateImpl(
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as FormzStatus,
      entities: null == entities
          ? _value._entities
          : entities // ignore: cast_nullable_to_non_nullable
              as List<RecipeEntity>,
      errorMessage: null == errorMessage
          ? _value.errorMessage
          : errorMessage // ignore: cast_nullable_to_non_nullable
              as String,
      isGrid: null == isGrid
          ? _value.isGrid
          : isGrid // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$MainStateImpl implements _MainState {
  const _$MainStateImpl(
      {this.status = FormzStatus.pure,
      final List<RecipeEntity> entities = const [],
      this.errorMessage = '',
      this.isGrid = false})
      : _entities = entities;

  @override
  @JsonKey()
  final FormzStatus status;
  final List<RecipeEntity> _entities;
  @override
  @JsonKey()
  List<RecipeEntity> get entities {
    if (_entities is EqualUnmodifiableListView) return _entities;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_entities);
  }

  @override
  @JsonKey()
  final String errorMessage;
  @override
  @JsonKey()
  final bool isGrid;

  @override
  String toString() {
    return 'MainState(status: $status, entities: $entities, errorMessage: $errorMessage, isGrid: $isGrid)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MainStateImpl &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality().equals(other._entities, _entities) &&
            (identical(other.errorMessage, errorMessage) ||
                other.errorMessage == errorMessage) &&
            (identical(other.isGrid, isGrid) || other.isGrid == isGrid));
  }

  @override
  int get hashCode => Object.hash(runtimeType, status,
      const DeepCollectionEquality().hash(_entities), errorMessage, isGrid);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MainStateImplCopyWith<_$MainStateImpl> get copyWith =>
      __$$MainStateImplCopyWithImpl<_$MainStateImpl>(this, _$identity);
}

abstract class _MainState implements MainState {
  const factory _MainState(
      {final FormzStatus status,
      final List<RecipeEntity> entities,
      final String errorMessage,
      final bool isGrid}) = _$MainStateImpl;

  @override
  FormzStatus get status;
  @override
  List<RecipeEntity> get entities;
  @override
  String get errorMessage;
  @override
  bool get isGrid;
  @override
  @JsonKey(ignore: true)
  _$$MainStateImplCopyWith<_$MainStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
