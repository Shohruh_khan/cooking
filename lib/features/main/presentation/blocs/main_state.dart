part of 'main_bloc.dart';

@freezed
class MainState with _$MainState {
  const factory MainState({
    @Default(FormzStatus.pure) FormzStatus status,
    @Default([]) List<RecipeEntity> entities,
    @Default('') String errorMessage,
    @Default(false) bool isGrid,
  }) = _MainState;
}
