import 'package:cached_network_image/cached_network_image.dart';
import 'package:cooking/core/bloc/show_pop_up/show_pop_up_bloc.dart';
import 'package:cooking/core/widgets/w_scale.dart';
import 'package:cooking/features/favorite/presentation/blocs/favorite_bloc.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:share_plus/share_plus.dart';

import '../../../../assets/colors/colors.dart';
import '../../../../assets/icons/icons.dart';
import '../../../../core/app_functions.dart';
import '../../domain/entity/recipe.dart';
import '../blocs/main_bloc.dart';

class RecipeInstruction extends StatefulWidget {
  const RecipeInstruction({
    super.key,
    required this.data,
  });
  // late TabController _tabController;
  final RecipeEntity data;

  @override
  State<RecipeInstruction> createState() => _RecipeInstructionState();
}

class _RecipeInstructionState extends State<RecipeInstruction>
    with SingleTickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 80.h,
        leading: WScaleAnimation(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: SvgPicture.asset(AppIcons.back),
          ),
        ),
        backgroundColor: primary,
        title: Text(
          widget.data.title,
          maxLines: 3,
          textAlign: TextAlign.center,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context)
              .textTheme
              .headlineMedium!
              .copyWith(fontSize: 16),
        ),
        actions: [
          WScaleAnimation(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 8.h, horizontal: 16.w),
                child: SvgPicture.asset(AppIcons.share),
              ),
              onTap: () {
                Share.share(
                  widget.data.share,
                  subject: 'Open from Website',
                );
              }),
        ],
      ),
      body: Column(
        children: [
          // SizedBox(height: 8.h),
          CachedNetworkImage(
            width: double.infinity,
            height: 200.h,
            imageUrl: widget.data.image,
            fit: BoxFit.cover,
            errorWidget: (_, __, e) => Image.asset(
              AppIcons.defaultRecipe,
              width: double.infinity,
              fit: BoxFit.fill,
              height: 150.h,
            ),
            placeholder: (context, url) => Image.asset(
              AppIcons.defaultRecipe,
              fit: BoxFit.fill,
              width: double.infinity,
              height: 150.h,
            ),
          ),
          SizedBox(height: 4.h),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 6.w),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  AppIcons.time,
                  height: 22.h,
                ),
                Text(
                  '${widget.data.readyInMinutes} min',
                  style: Theme.of(context)
                      .textTheme
                      .labelMedium!
                      .copyWith(fontSize: 12),
                ),
                const Spacer(),
                Text(
                  widget.data.healthyScore.toString(),
                  style: Theme.of(context)
                      .textTheme
                      .labelMedium!
                      .copyWith(fontSize: 12),
                ),
                SvgPicture.asset(
                  AppIcons.health,
                  height: 18.h,
                ),
              ],
            ),
          ),

          TabBar(controller: tabController, tabs: const [
            Text(
              'Ingredients',
              style: TextStyle(fontSize: 16),
            ),
            Text(
              'Instructions',
              style: TextStyle(fontSize: 16),
            ),
          ]),
          Expanded(
            child: TabBarView(
              controller: tabController,
              children: [
                // Content of Tab 1
                //bool?
                Ingredients(data: widget.data),

                Padding(
                  padding: EdgeInsets.symmetric(vertical: 12.h, horizontal: 18),
                  child: SelectableText(
                    AppFunctions.removeHtmlTags(widget.data.instructions),
                    style: Theme.of(context).textTheme.labelMedium,
                    showCursor: true,
                    cursorWidth: 3,
                    cursorRadius: const Radius.circular(5),
                  ),
                ),
              ],
            ),
          ),
          // Container(
          //   padding: const EdgeInsets.all(10),
          //   child: Column(
          //     crossAxisAlignment: CrossAxisAlignment.start,
          //     children: [
          //       Text(
          //         data.title,
          //         overflow: TextOverflow.ellipsis,
          //         maxLines: 3,
          //         textAlign: TextAlign.center,
          //         style: Theme.of(context)
          //             .textTheme
          //             .labelMedium!
          //             .copyWith(fontSize: 22),
          //       ),
          //       SizedBox(height: 12.h),
          //       Row(
          //         crossAxisAlignment: CrossAxisAlignment.center,
          //         mainAxisAlignment: MainAxisAlignment.center,
          //         children: [
          //           SvgPicture.asset(
          //             AppIcons.time,
          //             height: 32.h,
          //           ),
          //           Text(
          //             '${data.readyInMinutes} min',
          //             style: Theme.of(context)
          //                 .textTheme
          //                 .labelMedium!
          //                 .copyWith(fontSize: 20),
          //           ),
          //           const Spacer(),
          //           Text(
          //             data.healthyScore.toString(),
          //             style: Theme.of(context)
          //                 .textTheme
          //                 .labelMedium!
          //                 .copyWith(fontSize: 20),
          //           ),
          //           SvgPicture.asset(
          //             AppIcons.health,
          //             height: 32.h,
          //           ),
          //         ],
          //       ),
          //       SizedBox(height: 25.h),
          //       SelectableText(
          //         AppFunctions.removeHtmlTags(data.instructions),
          //         style: Theme.of(context).textTheme.labelMedium,
          //         showCursor: true,
          //         cursorWidth: 3,
          //         cursorRadius: const Radius.circular(5),
          //       ),
          //     ],
          //   ),
          // )
        ],
      ),
      floatingActionButton: BlocBuilder<FavoriteBloc, FavoriteState>(
        builder: (context, state) {
          return FloatingActionButton(
              onPressed: () {
                if (!state.entities.contains(widget.data)) {
                  context
                      .read<FavoriteBloc>()
                      .add(FavoriteEvent.addRecipe(widget.data));

                  context.read<ShowPopUpBloc>().add(
                      ShowPopUpEvent.showSuccess(text: "added_wishlist".tr()));
                } else {
                  context
                      .read<FavoriteBloc>()
                      .add(FavoriteEvent.removeRecipe(widget.data));
                  context.read<ShowPopUpBloc>().add(ShowPopUpEvent.showSuccess(
                      text: "removed_wishlist".tr()));
                }
              },
              child: Icon(
                state.entities.contains(widget.data) ? Icons.remove : Icons.add,
                size: 32,
              ));
        },
      ),
    );
  }
}

class Ingredients extends StatelessWidget {
  const Ingredients({
    super.key,
    required this.data,
  });

  final RecipeEntity data;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: SingleChildScrollView(
        child: BlocBuilder<MainBloc, MainState>(
          builder: (context, state) {
            return Column(
              children: [
                SizedBox(height: 6.h),
                Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                  GestureDetector(
                    onTap: () {
                      context
                          .read<MainBloc>()
                          .add(const MainEvent.getGrid(isGrid: true));
                    },
                    child: AnimatedContainer(
                      duration: const Duration(milliseconds: 300),
                      width: 64, // Set a specific width
                      height: 20.h,
                      margin: EdgeInsets.symmetric(horizontal: 4.w),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2),
                        shape: BoxShape.rectangle,
                        color: state.isGrid ? blue : lightGrey,
                      ),
                      child: Text(
                        "grid",
                        style: Theme.of(context)
                            .textTheme
                            .headlineSmall!
                            .copyWith(fontSize: 12),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      context
                          .read<MainBloc>()
                          .add(const MainEvent.getGrid(isGrid: false));
                    },
                    child: AnimatedContainer(
                      duration: const Duration(milliseconds: 300),
                      width: 64.w, // Set a specific width
                      height: 20.h,
                      margin: EdgeInsets.symmetric(horizontal: 4.w),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2),
                        shape: BoxShape.rectangle,
                        color: state.isGrid ? lightGrey : blue,
                      ),
                      child: Text(
                        'list',
                        style: Theme.of(context)
                            .textTheme
                            .headlineSmall!
                            .copyWith(fontSize: 12),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ]),
                SizedBox(height: 6.h),
                state.isGrid
                    ? GridView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3),
                        itemCount: data.ingredients.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            children: [
                              Text(
                                '${data.ingredients[index].amount.toString()} ${data.ingredients[index].unit}',
                                style: Theme.of(context).textTheme.titleMedium,
                                maxLines: 2,
                              ),
                              CachedNetworkImage(
                                width: 45.w,
                                height: 45.h,
                                imageUrl:
                                    "https://spoonacular.com/cdn/ingredients_100x100/${data.ingredients[index].image}",
                                fit: BoxFit.cover,
                                errorWidget: (_, __, e) => Image.asset(
                                  AppIcons.defaultRecipe,
                                  width: 24,
                                  fit: BoxFit.fill,
                                  height: 24,
                                ),
                                placeholder: (context, url) => Image.asset(
                                  AppIcons.defaultRecipe,
                                  fit: BoxFit.fill,
                                  width: 24,
                                  height: 24,
                                ),
                              ),
                              Text(
                                data.ingredients[index].originalName,
                                style: Theme.of(context).textTheme.titleMedium,
                                maxLines: 2,
                                textAlign: TextAlign.center,
                              ),
                            ],
                          );
                        },
                      )
                    : ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        padding: EdgeInsets.symmetric(
                            vertical: 8.h, horizontal: 12.w),
                        scrollDirection: Axis.vertical,
                        itemBuilder: (BuildContext context, int index) {
                          // final data = widget.data.ingredients[index];
                          return Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              CachedNetworkImage(
                                width: 75.w,
                                height: 75.h,
                                imageUrl:
                                    "https://spoonacular.com/cdn/ingredients_100x100/${data.ingredients[index].image}",
                                fit: BoxFit.cover,
                                errorWidget: (_, __, e) => Image.asset(
                                  AppIcons.defaultRecipe,
                                  width: 24,
                                  fit: BoxFit.fill,
                                  height: 24,
                                ),
                                placeholder: (context, url) => Image.asset(
                                  AppIcons.defaultRecipe,
                                  fit: BoxFit.fill,
                                  width: 24,
                                  height: 24,
                                ),
                              ),
                              SizedBox(width: 6.w),
                              Expanded(
                                child: Text(
                                  "${data.ingredients[index].amount.toString()} ${data.ingredients[index].unit} ${data.ingredients[index].originalName}",
                                  style:
                                      Theme.of(context).textTheme.titleMedium,
                                  maxLines: 2,
                                ),
                              ),
                              // SizedBox(width: 4.w),
                              // Text(widget.data.ingredients[index].unit),
                              // SizedBox(width: 6.w),
                              // Text(widget.data.ingredients[index].originalName)
                            ],
                          );
                          return null;
                        },
                        separatorBuilder: (BuildContext context, int index) =>
                            SizedBox(height: 8.h),
                        itemCount: data.ingredients.length),
              ],
            );
          },
        ),
      ),
    );
  }
}
