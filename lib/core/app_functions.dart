import 'dart:io';

import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import 'data/either.dart';

abstract class AppFunctions {
  static String removeHtmlTags(String data) {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);
    return data.replaceAll(exp, '');
  }

  static Future<Either<String, File>> pickImage({
    ImageSource source = ImageSource.gallery,
  }) async {
    try {
      // PermissionStatus cameraStatus = await Permission.camera.request();
      final ImagePicker picker = ImagePicker();
      final image = await picker.pickImage(source: source);

      if (image != null) {
        final croppedFile = await ImageCropper().cropImage(
          sourcePath: image.path,
          aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
          aspectRatioPresets: [
            CropAspectRatioPreset.square,
            CropAspectRatioPreset.ratio3x2,
            CropAspectRatioPreset.original,
            CropAspectRatioPreset.ratio4x3,
            CropAspectRatioPreset.ratio16x9
          ],
          // androidUiSettings: AndroidUiSettings(
          //   toolbarTitle: 'Crop Image',
          //   // toolbarWidgetColor: Colors.white,
          //   initAspectRatio: CropAspectRatioPreset.square,
          //   lockAspectRatio: true,
          // ),
          uiSettings: [
            AndroidUiSettings(
              toolbarTitle: 'Crop Image',
              // toolbarWidgetColor: Colors.white,
              initAspectRatio: CropAspectRatioPreset.square,
              lockAspectRatio: true,
            ),
            IOSUiSettings(title: 'Crop Image'),
          ],
          // iosUiSettings: const IOSUiSettings(title: 'Crop Image'),
        );

        if (croppedFile != null) {
          return Right(File(croppedFile.path));
        } else {
          return Right(File(image.path));
        }
      } else {
        return Left('No image is selected');
      }
    } catch (_) {
      return Left('Something went wrong');
    }
  }
}
