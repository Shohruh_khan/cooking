import 'package:equatable/equatable.dart';

import '../data/either.dart';
import '../error/failure.dart';

abstract class UseCase<Type, Params> {
  const UseCase();
  Future<Either<Failure, Type>> call(Params params);
}

class NoParams extends Equatable {
  @override
  List<Object?> get props => [];
}

class SignUpParams extends Equatable {
  final String firstName;
  final String lastName;
  final String email;
  final String password;
  final int age;
  final bool gender;
  final String image;

  const SignUpParams({
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.password,
    required this.age,
    required this.gender,
    required this.image,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        firstName,
        lastName,
        email,
        password,
        age,
        gender,
        image,
      ];
}

class SignInParams extends Equatable {
  final String email;
  final String password;

  const SignInParams({
    required this.email,
    required this.password,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        email,
        password,
      ];
}
