import 'package:hive/hive.dart';

late Box recipeBox;
late Box searchBox;
late Box token;
late Box searchHistoryBox;

const recipesKey = "recipes";
const tokenKey = "token";
const searchHistoryKey = "historyKey";
