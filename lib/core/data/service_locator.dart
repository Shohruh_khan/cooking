import 'package:cooking/core/data/boxes.dart';
import 'package:cooking/features/main/data/repository/main_repo_impl.dart';
import 'package:cooking/features/main/domain/repository/main_repo.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:hive/hive.dart';

import '../../features/main/domain/entity/recipe.dart';
import 'dio_settings.dart';

final serviceLocator = GetIt.instance;
Dio dio = Dio();
void setupLocator() {
  serviceLocator.registerLazySingleton(DioSettings.new);
  serviceLocator
      .registerLazySingleton<MainRepository>(() => MainRepositoryImpl());
  // serviceLocator.registerLazySingleton(() => FavoriteBloc());
  serviceLocator
      .registerLazySingleton(() async => Hive.box<RecipeEntity>(recipesKey));

  // serviceLocator.registerLazySingleton<FavouritesRepository>(
  //     () => FavouritesRepositoryImpl());
}
