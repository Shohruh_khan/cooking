part of 'translation_bloc.dart';

@Freezed()
class TranslationEvent with _$TranslationEvent {
  factory TranslationEvent.changeLanguage({required String languageName}) =
      _ChangeLanguage;
}
