import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:meta/meta.dart';

part 'translation_bloc.freezed.dart';
part 'translation_event.dart';
part 'translation_state.dart';

class TranslationBloc extends Bloc<TranslationEvent, TranslationState> {
  TranslationBloc() : super(TranslationState()) {
    // on<TranslationEvent>((event, emit) {
    //   // TODO: implement event handler
    // });

    on<_ChangeLanguage>((event, emit) async {
      emit(state.copyWith(language: event.languageName));
    });
  }
}
