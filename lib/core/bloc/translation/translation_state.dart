part of 'translation_bloc.dart';

@Freezed()
class TranslationState with _$TranslationState {
  factory TranslationState({
    @Default('') String language,
  }) = _TranslationState;
}
