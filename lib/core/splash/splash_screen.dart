import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../assets/icons/icons.dart';
import '../../features/home/presentation/home.dart';
import '../../features/home/presentation/widgets/navigator.dart';
import '../../features/login/presentation/login_screen.dart';
import '../data/boxes.dart';

class SplashScreen extends StatefulWidget {
  static Route route() => MaterialPageRoute(
        builder: (context) => const SplashScreen(),
      );
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    _init();
  }

  Future<void> _init() async {
    var tokenID = token.get(tokenKey);
    Timer(const Duration(seconds: 3), () {
      if (tokenID == null || tokenID == '') {
        Navigator.of(context).pushReplacement(fade(page: const LoginScreen()));
      } else {
        Navigator.of(context).pushReplacement(fade(page: const HomeScreen()));
      }
    });

    // Future.delayed(
    //   const Duration(seconds: 5),
    //   () async => await Navigator.pushReplacement(
    //     context,
    //     fade(page: const HomeScreen()),
    //   ),
    // );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      alignment: AlignmentDirectional.topCenter,
      children: [
        Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(AppIcons.splashBackground),
              fit: BoxFit.cover,
            ),
          ),
          child: null,
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: [
                  Image.asset(AppIcons.cookAvatar),
                  SizedBox(
                    height: 14.h,
                  ),
                  Text(
                    '100K+ Premium Recipe',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                ],
              ),
              Column(
                children: [
                  Text(
                    'Get Cooking',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(fontSize: 50.sp),
                  ),
                  SizedBox(height: 20.h),
                  Text(
                    'Simple way to find Tasty Recipe',
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium!
                        .copyWith(fontSize: 16.sp, fontWeight: FontWeight.w400),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    ));
  }
}
