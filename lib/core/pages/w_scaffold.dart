import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:overlay_support/overlay_support.dart';

import '../../assets/colors/colors.dart';
import '../../assets/icons/icons.dart';
import '../bloc/show_pop_up/show_pop_up_bloc.dart';
import '../models/popup_types.dart';
import '../widgets/popups.dart';

class WScaffold extends StatefulWidget {
  final Widget body;
  final PreferredSizeWidget? appBar;
  final Color? backgroundColor;
  final Widget? bottomNavigationBar;
  final Widget? floatingButton;
  final Widget? drawer;
  const WScaffold({
    required this.body,
    this.drawer,
    this.appBar,
    this.backgroundColor,
    this.bottomNavigationBar,
    this.floatingButton,
    Key? key,
  }) : super(key: key);

  @override
  State<WScaffold> createState() => _WScaffoldState();
}

class _WScaffoldState extends State<WScaffold> {
  final _warningKey = GlobalKey();
  final _successKey = GlobalKey();
  final _failureKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShowPopUpBloc, ShowPopUpState>(
      listener: (context, state) {
        if (state.showPopUp && state.popUpType == PopUpType.error) {
          showSimpleNotification(
            WPopUp(color: red, icon: AppIcons.error, text: state.errorText),
            elevation: 0,
            background: Colors.transparent,
            autoDismiss: true,
            slideDismissDirection: DismissDirection.horizontal,
          );
        } else if (state.showPopUp && state.popUpType == PopUpType.warning) {
          showSimpleNotification(
            WPopUp(
              color: Colors.orangeAccent,
              icon: AppIcons.error,
              text: state.warningText,
            ),
            elevation: 0,
            background: Colors.transparent,
            autoDismiss: true,
            slideDismissDirection: DismissDirection.horizontal,
          );
        } else if (state.showPopUp && state.popUpType == PopUpType.success) {
          showSimpleNotification(
            WPopUp(
              color: Colors.green,
              icon: AppIcons.success,
              text: state.successText,
            ),
            elevation: 0,
            background: Colors.transparent,
            autoDismiss: true,
            slideDismissDirection: DismissDirection.horizontal,
          );
        }
      },
      builder: (context, state) => KeyboardDismisser(
        child: Stack(
          children: [
            Scaffold(
              drawer: widget.drawer,
              floatingActionButton: widget.floatingButton,
              appBar: widget.appBar,
              backgroundColor: widget.backgroundColor,
              bottomNavigationBar: widget.bottomNavigationBar,
              body: widget.body,
            ),
            // PopUp.warning(
            //   showPopUp:
            //       state.showPopUp && state.popUpType == PopUpType.warning,
            //   globalKey: _warningKey,
            //   warningMessage: state.warningText,
            // ),
            // PopUp.success(
            //   globalKey: _successKey,
            //   successMessage: state.successText,
            //   showPopUp:
            //       state.showPopUp && state.popUpType == PopUpType.success,
            // ),
            // PopUp.error(
            //   globalKey: _failureKey,
            //   errorMessage: state.errorText,
            //   showPopUp: state.showPopUp && state.popUpType == PopUpType.error,
            // ),
          ],
        ),
      ),
    );
  }
}
