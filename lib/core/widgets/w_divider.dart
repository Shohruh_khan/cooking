import 'package:cooking/assets/colors/colors.dart';
import 'package:flutter/material.dart';

class WDivider extends StatelessWidget {
  final double height;
  final Color color;
  final EdgeInsets margin;
  const WDivider(
      {super.key,
      this.height = 1,
      this.color = dividerColor,
      this.margin = EdgeInsets.zero});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: margin,
      height: height,
      color: color,
    );
  }
}
