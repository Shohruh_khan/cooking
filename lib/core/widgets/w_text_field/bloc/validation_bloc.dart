// ignore: depend_on_referenced_packages
import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../models/formz/formz.dart';
import '../../../models/formz/formz_status.dart';
import '../../../models/formz/formz_type.dart';
import '../../../models/formz/rules.dart';

part 'validation_bloc.freezed.dart';
part 'validation_event.dart';
part 'validation_state.dart';

class ValidationBloc extends Bloc<ValidationEvent, ValidationState> {
  ValidationBloc() : super(const ValidationState()) {
    on<_FieldValidated>((event, emit) {
      emit(state.copyWith(status: FormzStatus.submissionInProgress));

      switch (event.type) {
        case FormzType.email:
          final formz = Formz.validateMail(mail: event.value);
          emit(state.copyWith(
            status: formz.status,
            validationMessage: formz.message,
          ));
          break;
        case FormzType.password:
          final formz = Formz.validatePassword(
            value: event.value,
            rules: event.rules as PasswordRules,
          );
          emit(state.copyWith(
            status: formz.status,
            validationMessage: formz.message,
          ));
          break;
        case FormzType.custom:
          final formz = Formz.validateCustom(
            value: event.value,
            rules: event.rules as CustomRules,
          );
          emit(state.copyWith(
            status: formz.status,
            validationMessage: formz.message,
          ));
          break;
        default:
          final formz = Formz.success();
          emit(state.copyWith(
            status: formz.status,
            validationMessage: formz.message,
          ));
      }
    });
  }
}
