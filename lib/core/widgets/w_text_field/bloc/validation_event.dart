part of 'validation_bloc.dart';

@freezed
class ValidationEvent with _$ValidationEvent {

  const factory ValidationEvent.validate(
    String value, {
    required VoidCallback onSuccess,
    required ValueChanged<String> onFailure,
    required FormzType type,
    Rules? rules,
  }) = _FieldValidated;
}
