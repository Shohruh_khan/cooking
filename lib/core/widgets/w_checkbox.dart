import 'package:cooking/core/widgets/w_scale.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../../assets/icons/icons.dart';

class WCheckBox extends StatelessWidget {
  const WCheckBox({
    Key? key,
    required this.value,
    this.borderRadius = 6,
    required this.onPressed,
    this.activeColor = Colors.green,
  }) : super(key: key);

  final bool value;
  final double borderRadius;
  final ValueChanged<bool> onPressed;
  final Color activeColor;

  @override
  Widget build(BuildContext context) {
    return WScaleAnimation(
      onTap: () {
        onPressed(!value);
      },
      child: AnimatedContainer(
        padding: const EdgeInsets.only(
          top: 2,
          bottom: 1,
        ),
        decoration: BoxDecoration(
          // border: Border.all(color: value ? activeColor : disabled),
          borderRadius: BorderRadius.circular(borderRadius),
        ),
        height: 20.h,
        width: 20.w,
        duration: const Duration(milliseconds: 250),
        child: value ? SvgPicture.asset(AppIcons.check) : null,
      ),
    );
  }
}
