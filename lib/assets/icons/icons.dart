abstract class AppIcons {
  const AppIcons._();

  /// ------------    Global Icons    -------------------- ///
  static const home = 'assets/icons/global/home_active.svg';
  static const profile = 'assets/icons/global/profile_active.svg';
  static const wishlist = 'assets/icons/global/wishlist_active.svg';
  static const search = 'assets/icons/global/search_active.svg';
  static const homeInactive = 'assets/icons/global/home_inactive.svg';
  static const profileInactive = 'assets/icons/global/profile_inactive.svg';
  static const wishlistInactive = 'assets/icons/global/wishlist_inactive.svg';
  static const searchInactive = 'assets/icons/global/search_inactive.svg';
  static const eye = 'assets/icons/global/eye.svg';
  static const success = 'assets/icons/global/success.svg';
  static const error = 'assets/icons/global/error.svg';
  static const health = 'assets/icons/global/health.svg';
  static const time = 'assets/icons/global/time.svg';
  static const edit = 'assets/icons/global/edit.svg';
  static const arrowBottom = 'assets/icons/global/arrow_bottom.svg';

  static const cookAvatar = 'assets/icons/png/cook_avatar.png';
  static const avatarPhoto = 'assets/icons/png/photo_avatar.png';
  static const splashBackground = 'assets/icons/png/splash.png';
  static const defaultRecipe = 'assets/icons/png/default_recipe_image.jpg';
  static const flagUzb = 'assets/icons/global/flag_uzb.svg';
  static const flagRu = 'assets/icons/global/flag_ru.svg';
  static const flagEn = 'assets/icons/global/flag_en.svg';
  static const verticalDots = 'assets/icons/global/vertical_dots.svg';
  static const back = 'assets/icons/global/back.svg';
  static const changePhoto = 'assets/icons/global/avatar_camera.svg';
  static const share = 'assets/icons/global/share.svg';
  static const check = 'assets/icons/global/check.svg';
  static const plus = 'assets/icons/global/plus.svg';
  static const minus = 'assets/icons/global/minus.svg';

  static const gallery = 'assets/icons/global/gallery.svg';
  static const camera = 'assets/icons/global/camera.svg';
  static const trash = 'assets/icons/global/trash.svg';
  static const cancel = 'assets/icons/global/cancel.svg';
  static const arrowRight = 'assets/icons/global/arrow_more.svg';

  /// ------------    Global Icons End   -------------------- ///
}
