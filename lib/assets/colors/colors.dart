import 'package:flutter/material.dart';

const white = _white;
const crprus = _cyprus;
const black = _black;
const blue = _blue;
const red = _redVelvet;
const grey = _c494646;
const primary = _c129575;
const onSecondary = _cFF9C00;
const neutral = _c121212;

const lightGrey = _c5C5C5C;
const borderColor = _c9199AA;
const green = _green;
const hintText = _cD9D9D9;

const dividerColor = _cBDBDBD;

/// colors palatee: http://www.color-blindness.com/color-name-hue/
const _white = Color(0XFFFFFFFF);
const _cyprus = Color(0XFF0C1A30);
const _black = Color(0XFF000000);
const _blue = Color(0XFF3669C9);
const _green = Color(0XFF00FF00);
const _cD9D9D9 = Color(0XFFD9D9D9);

const _redVelvet = Color(0XFFFE3A30);

/// Unnamed colors
const _c129575 = Color(0XFF129575);
const _cFF9C00 = Color(0XFFFF9C00);
const _c121212 = Color(0XFF121212);

const _c494646 = Color(0XFF494646);
const _c5C5C5C = Color(0XFF5C5C5C);
const _c9199AA = Color(0XFF9199AA);
const _cBDBDBD = Color(0XFFBDBDBD);
