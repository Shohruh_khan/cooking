import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../colors/colors.dart';

abstract class AppTheme {
  static ThemeData lightTheme() => ThemeData(
        scaffoldBackgroundColor: white,
        useMaterial3: true,
        fontFamily: 'DMSans',
        brightness: Brightness.light,
        textTheme: TextTheme(
          titleLarge: titleLarge, //20 black
          titleMedium: titleMedium, //16 black
          titleSmall: titleSmall, //12 black
          displayMedium: displayMedium, //16 OnSecondary(FF9C00)
          displaySmall: displaySmall, //12 OnSecondary(FF9C00)
          bodyLarge: bodyLarge, //(22) Neutral(FF121212)
          bodyMedium: bodyMedium, //(16) Neutral(FF121212)
          bodySmall: bodySmall, //(12)  Neutral(FF121212)

          labelMedium: labelMedium, //16 Primary(FF129575)
          labelSmall: labelSmall, //12 Primary(FF129575)
          headlineLarge: headlineLarge, //(12) red textStyle
          headlineMedium: headlineMedium, //18 white
          headlineSmall: headlineSmall, //12 white
        ),
        colorScheme: const ColorScheme(
          brightness: Brightness.light,
          primary: primary,
          onPrimary: white,
          secondary: blue,
          onSecondary: onSecondary,
          error: red,
          onError: grey,
          background: white,
          onBackground: white,
          surface: white,
          onSurface: black,
        ),
      );

  static ThemeData darkTheme() => ThemeData(
        useMaterial3: true,
        brightness: Brightness.dark,
        fontFamily: 'DMSans',
      );

  static TextStyle titleLarge = TextStyle(
    fontSize: 20.sp,
    fontWeight: FontWeight.w600,
    color: black, //000000
  ); //000000
  static TextStyle titleMedium = TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w600,
    color: black, //000000
  ); //000000
  static TextStyle titleSmall = TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
    color: black, //000000
  ); //000000

  static TextStyle bodyLarge = TextStyle(
    fontSize: 22.sp,
    fontWeight: FontWeight.w600,
    color: neutral, //121212
  ); // 121212
  static TextStyle bodyMedium = TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w600,
    color: neutral, //121212
  ); // 121212
  static TextStyle bodySmall = TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
    color: neutral, // 121212
  ); // 121212

  static TextStyle displayMedium = TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w600,
    color: onSecondary, // FF9C00
  ); // FF9C00
  static TextStyle displaySmall = TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
    color: onSecondary, // FF9C00
  ); // FF9C00

  static TextStyle labelMedium = TextStyle(
    fontSize: 16.sp,
    fontWeight: FontWeight.w600,
    color: primary, //129575
  ); //129575
  static TextStyle labelSmall = TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
    color: primary, //129575
  ); //129575

  static TextStyle headlineLarge = TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w600,
    color: red, //red
  ); //red
  static TextStyle headlineMedium = TextStyle(
    fontSize: 18.sp,
    fontWeight: FontWeight.w600,
    color: white, //FFFFFF
  ); //FFFFFF
  static TextStyle headlineSmall = TextStyle(
    fontSize: 12.sp,
    fontWeight: FontWeight.w400,
    color: white, //FFFFFF
  ); //FFFFFF

  // static const blackText = TextStyle(color: black);
  // static const redText = TextStyle(color: red);
  // static const blueText = TextStyle(color: blue);
}
