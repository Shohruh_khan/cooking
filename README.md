# COOKING RECIPE APP

## Table of contents
* [General info](#general-info)
* [Features](#features)
* [Installation](#installation)
* [Usage](#usage)
* [Acknowledgments](#acknowledgments)
* [Technologies](#technologies)
* [Contributing](#contributing)
* [Screenshots](#screenshots)
* [Contact](#contact)

## General info
Welcome to the Cooking Recipe App! This app allows you to discover, search, and save your favorite recipes. With a user-friendly interface, seamless navigation, and integration with the Spoonacular API, cooking delicious meals has never been easier.

## Features
* Authentication:
  Login and register securely using Firebase.

* Main Page:
  View all recipes with pagination (10 recipes per page).
  Swipe left on recipe items to add or remove them from favorites.

* Search Page
  Search by food name
  Search history is stored locally with search names and timestamps.

* Favorite Page
  The application stores favorite recipes locally, ensuring that they are accessible even when there is no internet connection.

## Installation 
* Install Flutter&Dart locally
* Clone the repository (https://gitlab.com/Shohruh_khan/cooking.git)
* Install dependencies (write to terminal: "flutter pub get")

## Usage
* Launch the app on an emulator or physical device. (write to terminal: "flutter run")

## Acknowledgments
 Special Thanks to [Sponacular](https://spoonacular.com/)

## Technologies
Project is created with:
* Flutter: The cross-platform framework
* Clean Architecture with Test-Driven Development(TDD): Emphasizing modularity, maintainability, and the principles of TDD.
* Hive: A lightweight and efficient local storage solution used for storing favorite recipes and search history.
* Get_it: Service Locator.
* Firebase: Firebase Authentication for secure user login and registration.
* Firestore: Firestore database for storing users' personal information.

## Contributing
* Fork the repository.
* Create a new branch: git checkout -b <branchName>.
* Commit your changes: git commit -m 'feat: New feature added'.
* Push to the branch: git push --set-upstream origin <branchName>.
* Submit a pull request.

## Screenshots:
![main_screen](flutter_01.png)
![main_screen](flutter_1.png)
![ingredients](flutter_1_1.png)
![instructions](flutter_1_2.png)
![search](flutter_2_1.png)
![search_history](flutter_2_2.png)
![profile](flutter_4.png)

## Contact
For support or inquiries, contact [shohruh.asatov@mail.ru].


  
  
